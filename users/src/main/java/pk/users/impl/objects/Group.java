package pk.users.impl.objects;

// Umport used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.users.api.objects.IGroupPermission;

// Import implemented API
import pk.users.api.objects.IGroup;

/**
 * Class representing Group table with idGroup as identity column. New instances
 * of this class are not distinguishable.
 */
public class Group extends ObjectBase implements IGroup {

	/**
	 * Identity column (idGroup).
	 */
	protected int idGroup;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Collection of GroupPermission object related with this group.
	 */
	protected Collection<IGroupPermission> groupPermissions;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Group(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize gropu permissions collection
		this.groupPermissions = Sets.newHashSet();
	}

	/**
	 * Get query builder to insert this group.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getGroupInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("name", this.name);

		// Return query builder
		return this.queries.getInsert("pklibrary.Group", insertMap);
	}

	/**
	 * Get query builder to update this group.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getGroupUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Group");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("name", this.name);
		Collection<String> updateWhere = Sets.newHashSet(
			"idGroup=" + this.idGroup);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this client.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idGroup == 0) {
			// This is a new group, do an insert
			IQueryInsert query = this.getGroupInsert();
			this.idGroup = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idGroup > 0;
		} else {
			// This group already exists, do an update
			IQueryUpdate query = this.getGroupUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idGroup=" + this.idGroup);
		IQuerySelect query = this.queries.getSelect("pklibrary.Group");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet group = this.database.select(query);
			group.first();

			// Fill fields with row data
			this.name = group.getString("name");
			this.fillGroupPermissions();
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during group object creation!";
			Logger.getLogger(Group.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	/**
	 * Automatically set all group permissions for this object using group
	 * permission id numbers obtained from direct query.
	 */
	protected void fillGroupPermissions() {
		// Prepare SELECT query for all group permissions
		Collection<String> whereSet = Sets.newHashSet(
			"idGroup=" + this.idGroup);
		IQuerySelect query = this.queries.getSelect(
			"pklibrary.GroupPermission");
		query.where(whereSet);

		try {
			// Execute query and iterate over results
			ResultSet permissions = this.database.select(query);
			while (permissions.next()) {
				// Intiialize new group permission object
				IGroupPermission permission = new GroupPermission(database,
					queries);
				permission.setIdGroup(this.idGroup);
				permission.setId(permissions.getInt("idGroupPermission"));

				// Save new group permission to collection
				this.groupPermissions.add(permission);
			}
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during group permissions " +
				"creation in group object!";
			Logger.getLogger(Group.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idGroup;
	}

	@Override
	public void setId(int id) {
		this.idGroup = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Collection<IGroupPermission> getGroupPermissions() {
		return this.groupPermissions;
	}

	@Override
	public boolean save() {
		// Assume success
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idGroup == 0) {
			result &= this.doSave();
		}

		// Save relational permission objects for this group
		for (IGroupPermission groupPermission : this.groupPermissions) {
			groupPermission.setIdGroup(this.idGroup);
			groupPermission.save();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idGroup != 0) {
			// Delete all relational permissions
			for (IGroupPermission groupPermission : this.groupPermissions) {
				groupPermission.delete();
			}

			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.Group"));
			Collection<String> where = Sets.newHashSet(
				"idGroup=" + this.idGroup);
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash code from fields
		int hash = 5;
		hash = 97 * hash + this.idGroup;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Group) {
			result = this.idGroup ==
				((Group) toCompare).getId();
		}

		return result;
	}

}
