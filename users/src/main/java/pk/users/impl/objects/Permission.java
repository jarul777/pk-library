package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.users.api.objects.IPermission;

/**
 * Class representing Permission table with idPermission as identity column. New
 * instances of this class are not distinguishable. This object shouldn't be
 * deleted and invoking delete method will have no effect.
 */
public class Permission extends ObjectBase implements IPermission {

	/**
	 * Identity column (idPermission).
	 */
	protected int idPermission;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Permission(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Get query builder to insert this permission.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getPermissionInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("name", this.name);

		// Return query builder
		return this.queries.getInsert("pklibrary.Permission", insertMap);
	}

	/**
	 * Get query builder to update this permission.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getPermissionUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.Permission");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("name", this.name);
		Collection<String> updateWhere = Sets.newHashSet(
			"idPermission=" + this.idPermission);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this media type.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idPermission == 0) {
			// This is a new group permission, do an insert
			IQueryInsert query = this.getPermissionInsert();
			this.idPermission = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idPermission > 0;
		} else {
			// This group permission already exists, do an update
			IQueryUpdate query = this.getPermissionUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idPermission=" + this.idPermission);
		IQuerySelect query = this.queries.getSelect("Permission");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet permission = this.database.select(query);
			permission.first();

			// Fill fields with row data
			this.name = permission.getString("name");
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during permission object creation!";
			Logger.getLogger(Permission.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idPermission;
	}

	@Override
	public void setId(int id) {
		this.idPermission = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idPermission == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Permission records should never be deleted
		String msg = "Permission should not be deleted!";
		Logger.getLogger(Permission.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash using fields
		int hash = 5;
		hash = 97 * hash + this.idPermission;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Permission) {
			result = this.idPermission ==
				((Permission) toCompare).getId();
		}

		return result;
	}

}
