package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;

// Import used java time libraries
import java.time.Instant;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryInsert;
import pk.database.api.queries.IQueryUpdate;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.users.api.objects.IPerson;
import pk.users.api.objects.IClient;
import pk.users.api.objects.IEmployee;
import pk.users.api.objects.IGroup;

// Import implemented API
import pk.users.api.objects.IUser;

/**
 * Class representing User table, id field represents idUser identity column.
 * New instances of this class are not distinguishable. This object shouldn't be
 * deleted and invoking delete method will have no effect.
 */
public class User extends ObjectBase implements IUser {

	/**
	 * Identity column (idUser).
	 */
	protected int idUser;

	/**
	 * Representation of login column.
	 */
	protected String login;

	/**
	 * Representation of password column. This should be stored as hash.
	 */
	protected String password;

	/**
	 * Representation of registrationTime column, this field is read only and
	 * has no setter.
	 */
	protected Timestamp registrationTime;

	/**
	 * Representation of idPerson column. This relational object is permanent
	 * and can't be changed to point to a different object, but the underlying
	 * object can be modified and will be saved.
	 */
	protected IPerson person;

	/**
	 * Representation of idClient column. This is an optional field.
	 */
	protected IClient client;

	/**
	 * Representation of idEmployee column. This is an optional field.
	 */
	protected IEmployee employee;

	/**
	 * Representation of idGroup column. This field is read only in this context
	 * and underlying object won't be saved. The id itself can be changed to
	 * represent different object.
	 */
	protected IGroup group;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public User(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.person = new Person(database, queries);
		this.client = new Client(database, queries);
		this.employee = new Employee(database, queries);
		this.group = new Group(database, queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @param insert True if this list is built for insert query, required for
	 * excluding permanent fields during update.
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns(boolean insert) {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("login", this.login);
		result.put("password", this.password);
		result.put("idGroup", this.group.getId());

		// Save client id if it was set
		if (this.client.getId() != 0) {
			result.put("idClient", this.client.getId());
		}

		// Save employee id if it was set
		if (this.employee.getId() != 0) {
			result.put("idEmployee", this.employee.getId());
		}

		// Handle permanent fields
		if (insert) {
			result.put("idPerson", this.person.getId());
		}

		return result;
	}

	/**
	 * Get query builder to update this user in database. Relational objects
	 * won't be updated with this query, only the base data.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getUserUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.User");
		Map<String, Object> updateColumns = this.getColumns(false);
		Collection<String> updateWhere = Sets.newHashSet(
			"idUser=" + this.idUser);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this user. This won't save the state of relational objects,
	 * these should be handled in save method.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idUser == 0) {
			// This is a new user, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.User", this.getColumns(true));
			this.idUser = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idUser > 0;
		} else {
			// This user already exists, do an update
			IQueryUpdate query = this.getUserUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet("idUser=" + this.idUser);
		IQuerySelect query = this.queries.getSelect("pklibrary.User");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet user = this.database.select(query);
			user.first();

			// Fill fields with row data
			this.login = user.getString("login");
			this.password = user.getString("password");
			this.registrationTime = user.getTimestamp("registrationTime");

			// Fill relational objects
			this.person.setId(user.getInt("idPerson"));
			this.group.setId(user.getInt("idGroup"));
			this.fillClient(user.getInt("idClient"));
			this.fillEmployee(user.getInt("idEmployee"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during user object creation!";
			Logger.getLogger(User.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	/**
	 * Create client object if client identity is not zero.
	 *
	 * @param idClient Client identity, zero if no such relation exists.
	 */
	protected void fillClient(int idClient) {
		if (idClient != 0) {
			this.client.setId(idClient);
		}
	}

	/**
	 * Create employee object if employee identity is not zero.
	 *
	 * @param idEmployee Employee identity, zero if no such identity exists.
	 */
	protected void fillEmployee(int idEmployee) {
		if (idEmployee != 0) {
			this.employee.setId(idEmployee);
		}
	}

	@Override
	public int getId() {
		return this.idUser;
	}

	@Override
	public void setId(int id) {
		this.idUser = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getLogin() {
		return this.login;
	}

	@Override
	public void setLogin(String login) {
		this.login = login;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Instant getRegistrationTime() {
		Instant result = null;
		if (this.registrationTime != null) {
			// Convert registration time to point in time
			result = this.registrationTime.toInstant();
		}

		return result;
	}

	@Override
	public IPerson getPerson() {
		return this.person;
	}

	@Override
	public IClient getClient() {
		return this.client;
	}

	@Override
	public IEmployee getEmployee() {
		return this.employee;
	}

	@Override
	public IGroup getGroup() {
		return this.group;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save relational objects that are not read only
		result &= this.person.save();

		// Save client object if it was changed
		if (this.client.isChanged()) {
			result &= this.client.save();
		}

		// Save employee object if it was changed
		if (this.employee.isChanged()) {
			result &= this.employee.save();
		}

		// Save self if changed or new
		if (this.isChanged() || this.idUser == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// User records should never be deleted
		String msg = "Users should not be deleted!";
		Logger.getLogger(User.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return this.login;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 5;
		hash = 59 * hash + this.idUser;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof User) {
			result = this.idUser ==
				((User) toCompare).getId();
		}

		return result;
	}

}
