package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.users.api.objects.IClient;

/**
 * Class representing client table, id field represents idClient identity
 * column. New instances of this class are not distinguishable. This object
 * shouldn't be deleted and invoking delete method will have no effect.
 */
public class Client extends ObjectBase implements IClient {

	/**
	 * Identity column (idClient).
	 */
	protected int idClient;

	/**
	 * Representation of cardNumber column.
	 */
	protected String cardNumber;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Client(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Get query builder to insert this client.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getClientInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("cardNumber", this.cardNumber);

		// Return query builder
		return this.queries.getInsert("pklibrary.Client", insertMap);
	}

	/**
	 * Get query builder to update this client.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getClientUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.Client");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("cardNumber", this.cardNumber);
		Collection<String> updateWhere = Sets.newHashSet(
			"idClient=" + this.idClient);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this client.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idClient == 0) {
			// This is a new user, do an insert
			IQueryInsert query = this.getClientInsert();
			this.idClient = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idClient > 0;
		} else {
			// This client already exists, do an update
			IQueryUpdate query = this.getClientUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idClient = " + this.idClient);
		IQuerySelect query = this.queries.getSelect("pklibrary.Client");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet client = this.database.select(query);
			client.first();

			// Fill fields with row data
			this.cardNumber = client.getString("cardNumber");
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during client object creation!";
			Logger.getLogger(Client.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idClient;
	}

	@Override
	public void setId(int id) {
		this.idClient = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getCardNumber() {
		return this.cardNumber;
	}

	@Override
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean save() {
		// Assume success
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idClient == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Client records should never be deleted
		String msg = "Client should not be deleted!";
		Logger.getLogger(Client.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return "Client card no. " + this.cardNumber;
	}

	@Override
	public int hashCode() {
		// Build hash from fields
		int hash = 7;
		hash = 59 * hash + this.idClient;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Client) {
			result = this.idClient ==
				((Client) toCompare).getId();
		}

		return result;
	}

}
