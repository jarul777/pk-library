package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.users.api.objects.IPermission;

// Import implemented API
import pk.users.api.objects.IGroupPermission;

/**
 * Class representing GroupPermission table with idGroupPermission as identity
 * column. New instances of this class are distinguishable by permission.
 */
public class GroupPermission extends ObjectBase implements IGroupPermission {

	/**
	 * Identity column (idGroupPermission).
	 */
	protected int idGroupPermission;

	/**
	 * Reference to a group table by identity column, no group representation is
	 * necessary for this class. This field is permanent, so it won't be
	 * updated.
	 */
	protected int idGroup;

	/**
	 * Representation of see column (determines whether group member can view
	 * content with this permission).
	 */
	protected boolean see;

	/**
	 * Representation of edit column (determines whether group member can edit
	 * content with this permission).
	 */
	protected boolean edit;

	/**
	 * Representation of idPermission column. This field is read only in this
	 * context and won't be saved. The id itself can be changed to represent
	 * different object.
	 */
	protected IPermission permission;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public GroupPermission(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.permission = new Permission(this.database, this.queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @param insert True if this list is built for insert query, required for
	 * excluding permanent fields during update.
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns(boolean insert) {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("see", this.see ? 1 : 0);
		result.put("edit", this.edit ? 1 : 0);
		result.put("idPermission", this.permission.getId());

		// Handle permanent fields
		if (insert) {
			result.put("idGroup", this.idGroup);
		}

		return result;
	}

	/**
	 * Get query builder to update this group permission.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getGroupPermissionUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.GroupPermission");
		Map<String, Object> updateColumns = this.getColumns(false);
		Collection<String> updateWhere = Sets.newHashSet(
			"idGroupPermission=" + this.idGroupPermission,
			"idGroup=" + this.idGroup);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this client.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idGroupPermission == 0) {
			// This is a new group permission, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.GroupPermission", this.getColumns(true));
			this.idGroupPermission = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idGroupPermission > 0;
		} else {
			// This group permission already exists, do an update
			IQueryUpdate query = this.getGroupPermissionUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idGroupPermission=" + this.idGroupPermission);
		IQuerySelect query = this.queries.getSelect(
			"pklibrary.GroupPermission");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet groupPermission = this.database.select(query);
			groupPermission.first();

			// Fill fields with row data
			this.idGroup = groupPermission.getInt("idGroup");
			this.see = groupPermission.getBoolean("see");
			this.edit = groupPermission.getBoolean("edit");

			// Fill relational objects
			this.permission.setId(groupPermission.getInt("idPermission"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during group permission " +
				"object creation!";
			Logger.getLogger(GroupPermission.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idGroupPermission;
	}

	@Override
	public void setId(int id) {
		this.idGroupPermission = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public int getIdGroup() {
		return this.idGroup;
	}

	@Override
	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}

	@Override
	public boolean getSee() {
		return this.see;
	}

	@Override
	public void setSee(boolean see) {
		this.see = see;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean getEdit() {
		return this.edit;
	}

	@Override
	public void setEdit(boolean edit) {
		this.edit = edit;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public IPermission getPermission() {
		return this.permission;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idGroupPermission == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idGroupPermission != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.GroupPermission"));
			Collection<String> where = Sets.newHashSet(
				"idGroupPermission=" + this.idGroupPermission,
				"idGroup=" + this.idGroup);
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		String canSee = this.see ? "see" : "no see";
		String canEdit = this.edit ? "edit" : "no edit";

		// Build string representation from fields
		return this.permission.getName() + " (" + canSee + canEdit + ")";
	}

	@Override
	public int hashCode() {
		// Build hash code from fields
		int hash = 3;
		hash = 19 * hash + this.idGroupPermission;
		hash = 19 * hash + this.idGroup;

		// Add permission hash to distinguish newly created objects
		hash = 19 * hash + Objects.hashCode(this.permission);

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof GroupPermission) {
			result = this.idGroupPermission ==
				((GroupPermission) toCompare).getId();
			result &= this.idGroup ==
				((GroupPermission) toCompare).getIdGroup();

			// Add permission id to distinguish newly created objects
			result &= this.permission.equals(
				((GroupPermission) toCompare).getPermission());
		}

		return result;
	}

}
