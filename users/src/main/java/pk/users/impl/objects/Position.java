package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.users.api.objects.IPosition;

/**
 * Class representing Position table with idPosition as identity column. New
 * instances of this class are not distinguishable.
 */
public class Position extends ObjectBase implements IPosition {

	/**
	 * Identity column (idPosition).
	 */
	protected int idPosition;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Representation of defaultSalary column.
	 */
	protected int defaultSalary;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Position(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Get query builder to insert this group position.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getPositionInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("name", this.name);
		insertMap.put("defaultSalary", this.defaultSalary);

		// Return query builder
		return this.queries.getInsert("pklibrary.Position", insertMap);
	}

	/**
	 * Get query builder to update this group position.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getPositionUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Position");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("name", this.name);
		updateColumns.put("defaultSalary", this.defaultSalary);
		Collection<String> updateWhere = Sets.newHashSet(
			"idPosition=" + this.idPosition);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this client.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idPosition == 0) {
			// This is a new group position, do an insert
			IQueryInsert query = this.getPositionInsert();
			this.idPosition = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idPosition > 0;
		} else {
			// This group position already exists, do an update
			IQueryUpdate query = this.getPositionUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idPosition=" + this.idPosition);
		IQuerySelect query = this.queries.getSelect("pklibrary.Position");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet position = this.database.select(query);
			position.first();

			// Fill fields with row data
			this.name = position.getString("name");
			this.defaultSalary = position.getInt("defaultSalary");
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during position object creation!";
			Logger.getLogger(Position.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idPosition;
	}

	@Override
	public void setId(int id) {
		this.idPosition = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public int getDefaultSalary() {
		return this.defaultSalary;
	}

	@Override
	public void setDefaultSalary(int defaultSalary) {
		this.defaultSalary = defaultSalary;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idPosition == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idPosition != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.Position"));
			Collection<String> where = Sets.newHashSet(
				"idPosition=" + this.idPosition);
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash using fields
		int hash = 7;
		hash = 59 * hash + this.idPosition;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Position) {
			result = this.idPosition ==
				((Position) toCompare).getId();
		}

		return result;
	}

}
