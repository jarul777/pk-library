package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;
import java.util.Date;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.text.SimpleDateFormat;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used common objects API
import pk.core.api.objects.IAddress;
import pk.core.api.objects.ILibrary;

// Import used common objects implementation
import pk.core.impl.objects.Address;
import pk.core.impl.objects.Library;

// Import implemented API
import pk.users.api.objects.IPerson;

/**
 * Class representing Person table, id field represents idPerson identity
 * column. New instances of this class are not distinguishable. This object
 * shouldn't be deleted and invoking delete method will have no effect.
 */
public class Person extends ObjectBase implements IPerson {

	/**
	 * Identity column (idPerson).
	 */
	protected int idPerson;

	/**
	 * Representation of surname column.
	 */
	protected String name;

	/**
	 * Representation of name column.
	 */
	protected String surname;

	/**
	 * Representation of dob column. This is an optional field.
	 */
	protected Date dob;

	/**
	 * Representation of phone column. This is an optional field.
	 */
	protected String phone;

	/**
	 * Representation of email column. This is an optional field.
	 */
	protected String email;

	/**
	 * Representation of address column. This is an optional field.
	 */
	protected IAddress address;

	/**
	 * Representation of idLibrary column. This field is read only in this
	 * context and won't be saved. The id itself can be changed to represent
	 * different object.
	 */
	protected ILibrary library;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Person(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.address = new Address(database, queries);
		this.library = new Library(database, queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns() {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("name", this.name);
		result.put("surname", this.surname);
		result.put("idLibrary", this.library.getId());

		// Save optional date of birth if it was set
		if (this.dob != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
			result.put("dob", formatter.format(this.dob));
		}

		// Save optional phone if it was set
		if (this.phone != null) {
			result.put("phone", this.phone);
		}

		// Save optional email if it was set
		if (this.email != null) {
			result.put("email", this.email);
		}

		// Save optional address id if it was set
		if (this.address.getId() != 0) {
			result.put("idAddress", this.address.getId());
		}

		return result;
	}

	/**
	 * Get query builder to update this person in database. Relational objects
	 * won't be updated with this query, only the base data.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getPersonUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Person");
		Map<String, Object> updateColumns = this.getColumns();
		Collection<String> updateWhere = Sets.newHashSet(
			"idPerson=" + this.idPerson);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this person. This won't save the state of relational
	 * objects, these should be handled in save method.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idPerson == 0) {
			// This is a new person, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.Person", this.getColumns());
			this.idPerson = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idPerson > 0;
		} else {
			// This person already exists, do an update
			IQueryUpdate query = this.getPersonUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idPerson=" + this.idPerson);
		IQuerySelect query = this.queries.getSelect("pklibrary.Person");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet person = this.database.select(query);
			person.first();

			// Fill fields with row data
			this.name = person.getString("name");
			this.surname = person.getString("surname");
			this.dob = person.getDate("dob");
			this.phone = person.getString("phone");
			this.email = person.getString("email");

			// Fill relational objects
			this.library.setId(person.getInt("idLibrary"));
			this.fillAddress(person.getInt("idAddress"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during person object creation!";
			Logger.getLogger(Person.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	/**
	 * Create address object if address identity is not zero.
	 *
	 * @param idAddress Address identity, zero if no such relation exists.
	 */
	protected void fillAddress(int idAddress) {
		if (idAddress != 0) {
			this.address.setId(idAddress);
		}
	}

	@Override
	public int getId() {
		return this.idPerson;
	}

	@Override
	public void setId(int id) {
		this.idPerson = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getSurname() {
		return this.surname;
	}

	@Override
	public void setSurname(String surname) {
		this.surname = surname;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Date getDateOfBirth() {
		return this.dob;
	}

	@Override
	public void setDateOfBirth(Date dob) {
		this.dob = dob;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getPhone() {
		return this.phone;
	}

	@Override
	public void setPhone(String phone) {
		this.phone = phone;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public IAddress getAddress() {
		return this.address;
	}

	@Override
	public ILibrary getLibrary() {
		return this.library;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save relational objects that are not read only
		result &= this.address.save();

		// Save self if changed or new
		if (this.isChanged() || this.idPerson == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Person records should never be deleted
		String msg = "Person should not be deleted!";
		Logger.getLogger(Person.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return this.name + " " + this.surname;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 7;
		hash = 53 * hash + this.idPerson;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Person) {
			result = this.idPerson ==
				((Person) toCompare).getId();
		}

		return result;
	}

}
