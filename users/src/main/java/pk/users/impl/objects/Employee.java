package pk.users.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.users.api.objects.IPosition;

// Import implemented API
import pk.users.api.objects.IEmployee;

/**
 * Class representing employee table, id field represents idEmployee identity
 * column. New instances of this class are not distinguishable. This object
 * shouldn't be deleted and invoking delete method will have no effect.
 */
public class Employee extends ObjectBase implements IEmployee {

	/**
	 * Identity column (idEmployee).
	 */
	protected int idEmployee;

	/**
	 * Representation of salary column. This is an optional field.
	 */
	protected int salary;

	/**
	 * Representation of idPosition column. This field is read only in this
	 * context and won't be saved. The id itself can be changed to represent
	 * different object.
	 */
	protected IPosition position;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Employee(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational table objects
		this.position = new Position(database, queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns() {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("idPosition", this.position.getId());

		// Save optional salary if it was set
		if (this.salary != 0) {
			result.put("salary", this.salary);
		}

		return result;
	}

	/**
	 * Get query builder to update this employee.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getEmployeeUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Employee");
		Map<String, Object> updateColumns = this.getColumns();
		Collection<String> updateWhere = Sets.newHashSet(
			"idEmployee=" + this.idEmployee);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this employee.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idEmployee == 0) {
			// This is a new employee, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.Employee", this.getColumns());
			this.idEmployee = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idEmployee > 0;
		} else {
			// This employee already exists, do an update
			IQueryUpdate query = this.getEmployeeUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idEmployee=" + this.idEmployee);
		IQuerySelect query = this.queries.getSelect("pklibrary.Employee");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet employee = this.database.select(query);
			employee.first();

			// Fill fields with row data
			this.salary = employee.getInt("salary");

			// Fill relational objects
			this.position.setId(employee.getInt("idPosition"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during employee object creation!";
			Logger.getLogger(Employee.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idEmployee;
	}

	@Override
	public void setId(int id) {
		this.idEmployee = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public int getSalary() {
		return this.salary;
	}

	@Override
	public void setSalary(int salary) {
		this.salary = salary;

		// Object base value has changed
		this.changed = true;
	}

	@Override
	public IPosition getPosition() {
		return this.position;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idEmployee == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Employee records should never be deleted
		String msg = "Employee should not be deleted!";
		Logger.getLogger(Employee.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		// Use underlying position object
		return "Employee id no. " + this.idEmployee;
	}

	@Override
	public int hashCode() {
		// Build hash from fields
		int hash = 7;
		hash = 97 * hash + this.idEmployee;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Employee) {
			result = this.idEmployee ==
				((Employee) toCompare).getId();
		}

		return result;
	}

}
