package pk.users.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;

// Import used external libraries
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ServiceBase;

// Import used utility classes
import pk.users.internal.Security;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.users.api.IAuthentication;

/**
 * Authentication mechanism using database stored user data. Mails are used to
 * implement password reset mechanism.
 */
public class Authentication extends ServiceBase implements IAuthentication {

	/**
	 * Login of the user to authenticate.
	 */
	protected String login;

	/**
	 * Password of user to authenticate.
	 */
	protected String password;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Authentication(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Set login to authenticate (invoked only via login method).
	 *
	 * @param login Login to authenticate.
	 */
	protected void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Set password to authenticate (invoked only via login method).This method
	 * will automatically hash the password it receives.
	 *
	 * @param password Password corresponding to set login.
	 */
	protected void setPassword(String password) {
		this.password = Security.getStringHash(password);
	}

	/**
	 * Get select query that will confirm, that given login - password pair
	 * exists in the database.
	 *
	 * @return Prepared select query.
	 */
	protected IQuerySelect getLoginSelect() {
		// Prepare clauses to get user with given login
		Collection<String> columnSet = Sets.newHashSet(
			"idUser", "login", "password");
		Collection<String> whereSet = Sets.newHashSet(
			"login='" + this.login + "'",
			"password='" + this.password + "'");

		// Build query
		IQuerySelect query = this.queries.getSelect("User");
		query.select(columnSet);
		query.where(whereSet);

		// Return query builder
		return query;
	}

	@Override
	public int login(String login, String password) {
		// Default state is user is not authorized
		int result = 0;

		// Save login and password variables and get query
		this.setLogin(login);
		this.setPassword(password);
		IQuerySelect query = this.getLoginSelect();

		try {
			// Execute query and check results count
			ResultSet users = this.database.select(query);
			while (users.next()) {
				// User exists, get its id
				result = users.getInt("idUser");
			}
		} catch (SQLException exception) {
			String msg = "Error occured during user authentication!";
			Logger.getLogger(Authentication.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public boolean sendResetPassword(String login) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean confirmResetPassword(String token) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
