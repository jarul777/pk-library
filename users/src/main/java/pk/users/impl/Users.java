package pk.users.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Random;

// Import used external libraries
import com.google.common.collect.Sets;

// Import common libraries
import pk.core.api.base.ServiceBase;

// Import used utility classes
import pk.users.internal.Security;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import used objects API
import pk.users.api.objects.IUser;
import pk.users.api.objects.IPosition;

// Import used objects implementation
import pk.users.impl.objects.User;
import pk.users.impl.objects.Position;

// Import implemented API
import pk.users.api.IUsers;

/**
 * User factory for managing single users by identity or getting empty users to
 * be filled with data and saved. Can also list all users registered in the
 * system.
 */
public class Users extends ServiceBase implements IUsers {

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Users(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	@Override
	public Collection<IUser> listUsers() {
		// Assume empty collection as default state
		Collection<IUser> result = Sets.newHashSet();

		// Create users list
		IQuerySelect query = this.queries.getSelect("pklibrary.User");
		query.select(Sets.newHashSet("idUser"));

		try {
			// Execute query to get all users
			ResultSet users = this.database.select(query);
			while (users.next()) {
				// Create user with identity from database
				IUser user = new User(database, queries);
				user.setId(users.getInt("idUser"));

				// Save it to the collection
				result.add(user);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during users list creation!";
			Logger.getLogger(Users.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of users
		return result;
	}

	@Override
	public IUser getUser(int idUser) {
		// Create empty user object and fill its data if identity is not zero
		IUser result = new User(this.database, this.queries);
		if (idUser != 0) {
			result.setId(idUser);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public Collection<IPosition> listPositions() {
		// Assume empty collection as default state
		Collection<IPosition> result = Sets.newHashSet();

		// Create positions list
		IQuerySelect query = this.queries.getSelect("pklibrary.Position");
		query.select(Sets.newHashSet("idPosition"));

		try {
			// Execute query to get all positions
			ResultSet positions = this.database.select(query);
			while (positions.next()) {
				// Create position with identity from database
				IPosition position = new Position(database, queries);
				position.setId(positions.getInt("idPosition"));

				// Save it to the collection
				result.add(position);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during positions list creation!";
			Logger.getLogger(Users.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of users
		return result;
	}

	@Override
	public IPosition getPosition(int idPosition) {
		// Create empty position object and fill its data if id is not zero
		IPosition result = new Position(this.database, this.queries);
		if (idPosition != 0) {
			result.setId(idPosition);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public String generateCardNumber() {
		// Get bounded random generated number
		Random random = new Random();
		int result = random.nextInt(999999);

		// Return it converted to string
		return String.valueOf(result);
	}

	@Override
	public String generatePasswordHash(String password) {
		return Security.getStringHash(password);
	}

}
