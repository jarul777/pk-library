package pk.users.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

// Import used external libraries
import com.google.common.collect.Sets;

// Import common libraries
import pk.core.api.base.ServiceBase;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import used objects APi
import pk.users.api.objects.IGroup;
import pk.users.api.objects.IPermission;
import pk.users.api.objects.IGroupPermission;

// Import used objects implementation
import pk.users.impl.objects.Group;
import pk.users.impl.objects.Permission;
import pk.users.impl.objects.GroupPermission;

// Import implemented API
import pk.users.api.IGroups;

/**
 * Groups factory for managing all groups and permissions.
 */
public class Groups extends ServiceBase implements IGroups {

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Groups(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	@Override
	public Collection<IGroup> listGroups() {
		// Assume empty collection as default state
		Collection<IGroup> result = Sets.newHashSet();

		// Create groups list
		IQuerySelect query = this.queries.getSelect("pklibrary.Group");
		query.select(Sets.newHashSet("idGroup"));

		try {
			// Execute query to get all groups
			ResultSet groups = this.database.select(query);
			while (groups.next()) {
				// Create group with identity from database
				IGroup group = new Group(database, queries);
				group.setId(groups.getInt("idGroup"));

				// Save it to the collection
				result.add(group);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during groups list creation!";
			Logger.getLogger(Groups.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of groups
		return result;
	}

	@Override
	public IGroup getGroup(int idGroup) {
		// Create empty group object and fill its data
		IGroup result = new Group(this.database, this.queries);
		if (idGroup != 0) {
			result.setId(idGroup);
		}

		// Return prepared group object
		return result;
	}

	@Override
	public Collection<IPermission> listPermissions() {
		// Assume empty collection as default state
		Collection<IPermission> result = Sets.newHashSet();

		// Create permissions list
		IQuerySelect query = this.queries.getSelect("pklibrary.Permission");
		query.select(Sets.newHashSet("idPermission"));

		try {
			// Execute query to get all groups
			ResultSet pemissions = this.database.select(query);
			while (pemissions.next()) {
				// Create permission with identity from database
				IPermission permission = new Permission(database, queries);
				permission.setId(pemissions.getInt("idPermission"));

				// Save it to the collection
				result.add(permission);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during permissions list creation!";
			Logger.getLogger(Groups.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of permissions
		return result;
	}

	@Override
	public IGroupPermission getGroupPermission(int idGroupPermission) {
		// Create empty group permission object and fill its data
		IGroupPermission result =
			new GroupPermission(this.database, this.queries);
		if (idGroupPermission != 0) {
			result.setId(idGroupPermission);
		}

		// Return prepared group permission object
		return result;
	}

}
