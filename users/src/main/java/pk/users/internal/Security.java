package pk.users.internal;

// Import used java I/O libraries
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.InputStream;

// Import used java security libraries
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

// Import used external libraries
import org.apache.commons.codec.binary.Hex;

/**
 *
 *
 */
public class Security {

	/**
	 * Empty constructor (prevents class instantiation).
	 */
	private Security() {
	}

	/**
	 * Get string hash representation using algorithm specified in security
	 * properties file.
	 *
	 * @param toHash String to get hash for.
	 * @return String hash.
	 */
	public static String getStringHash(String toHash) {
		String result = null;

		try {
			// Get security properties
			Properties security = Security.getProperties();

			// Get hashed string representation
			MessageDigest hasher = MessageDigest.getInstance(
				security.getProperty("algorithm"));
			byte[] hashedHex = hasher.digest(toHash.getBytes(
				security.getProperty("charset")));

			// Get string from its hexadecimal representation
			result = Hex.encodeHexString(hashedHex);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException exception) {
			String msg = "Error occured during hash generation!";
			Logger.getLogger(Security.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return string hash
		return result;
	}

	/**
	 * Get security properties, read from security properties file.
	 *
	 * @return Configured properties object.
	 */
	public static Properties getProperties() {
		// Default state assumes no properties are set
		Properties properties = new Properties();
		InputStream input = null;

		try {
			// Load a properties file
			input = Security.class.getResourceAsStream("security.properties");
			properties.load(input);
		} catch (IOException exception) {
			// Loading resource failed, log error using native java logger
			String msg = "Error during properties file reading!";
			Logger.getLogger(Security.class.getName()).
				log(Level.SEVERE, msg, exception);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException exception) {
					// Unable to close, log error using native java logger
					String msg = "Error during properties file closing!";
					Logger.getLogger(Security.class.getName()).
						log(Level.SEVERE, msg, exception);
				}
			}
		}

		// Return resulting properties
		return properties;
	}

}
