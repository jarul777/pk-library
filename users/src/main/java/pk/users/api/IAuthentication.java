package pk.users.api;

/**
 * Interface for user authentication with login and password, also provides
 * functionality for password recovery.
 */
public interface IAuthentication {

	/**
	 * Authenticate user using given login and password.
	 *
	 * @param login User login to authenticate.
	 * @param password User password to authenticate.
	 * @return Id of logged user if user passed authentication, zero otherwise.
	 */
	public int login(String login, String password);

	/**
	 * Send request for password reset, this should be confirmed in
	 * confirmResetPassword method.
	 *
	 * @param login Login for which to reset the password.
	 * @return Whether request was sent correctly or not.
	 */
	public boolean sendResetPassword(String login);

	/**
	 * This method should be invoked to confirm password reset with generated
	 * token sent to users by implementation specific way.
	 *
	 * @param token Token sent to the user to confirm password reset.
	 * @return Whether password reset is confirmed or not.
	 */
	public boolean confirmResetPassword(String token);

}
