package pk.users.api;

// Import used java utilities
import java.util.Collection;

// Import used objects API
import pk.users.api.objects.IGroup;
import pk.users.api.objects.IGroupPermission;
import pk.users.api.objects.IPermission;

/**
 * Interface for groups management. Provides methods for listing groups and
 * available permissions. Also serves as an factory for group and group
 * permission objects.
 */
public interface IGroups {

	/**
	 * Get list of all groups registered in the system.
	 *
	 * @return All registered groups.
	 */
	public Collection<IGroup> listGroups();

	/**
	 * Build group with given identity. if identity is zero builds empty group
	 * object.
	 *
	 * @param idGroup Identity of group to build.
	 * @return Group with given identity or empty group object if identity was
	 * zero.
	 */
	public IGroup getGroup(int idGroup);

	/**
	 * Get list of all permissions registered in the system. This list is
	 * constant - permissions can't be removed or added.
	 *
	 * @return All registered permissions.
	 */
	public Collection<IPermission> listPermissions();

	/**
	 * Build group permission with given identity. if identity is zero builds
	 * empty group permission object.
	 *
	 * @param idGroupPermission Identity of group permission to build.
	 * @return Group permission with given identity or empty group permission
	 * object if identity was zero.
	 */
	public IGroupPermission getGroupPermission(int idGroupPermission);

}
