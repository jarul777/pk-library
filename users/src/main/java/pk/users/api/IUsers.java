package pk.users.api;

// Import used java utilities
import java.util.Collection;

// Import used objects API
import pk.users.api.objects.IPosition;
import pk.users.api.objects.IUser;

/**
 * Interface for users management. Provides methods for listing users and
 * functions specific for user groups like generating card number for a client
 * or listing all positions available for an employee. Also serves as an user
 * related objects factory.
 */
public interface IUsers {

	/**
	 * Get list of all users registered in the system.
	 *
	 * @return All registered users.
	 */
	public Collection<IUser> listUsers();

	/**
	 * Build user with given identity. if identity is zero builds empty user
	 * object.
	 *
	 * @param idUser Identity of user to build.
	 * @return User with given identity or empty user object if identity was
	 * zero.
	 */
	public IUser getUser(int idUser);

	/**
	 * List all positions available to employee users.
	 *
	 * @return All available positions.
	 */
	public Collection<IPosition> listPositions();

	/**
	 * Build position with given identity. if identity is zero builds empty
	 * position object.
	 *
	 * @param idPosition Identity of position to build.
	 * @return Position with given identity or empty position object if identity
	 * was zero.
	 */
	public IPosition getPosition(int idPosition);

	/**
	 * Generates card number for client user.
	 *
	 * @return Generated card number
	 */
	public String generateCardNumber();

	/**
	 * Generates hash for passed string object using external security
	 * properties file.
	 *
	 * @param password Password to get hash for.
	 * @return Password hash generated using algorithm specified in security
	 * properties.
	 */
	public String generatePasswordHash(String password);

}
