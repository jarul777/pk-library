package pk.users.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for client table representation. Fields:
 * <ul>
 * <li>Card number</li>
 * </ul>
 */
public interface IClient extends IObject {

	/**
	 * Card number column value getter.
	 *
	 * @return Client card number null if client identity has not been set.
	 */
	public String getCardNumber();

	/**
	 * Card number column value setter. This should be an unique value for each
	 * client.
	 *
	 * @param cardNumber New client card number.
	 */
	public void setCardNumber(String cardNumber);

}
