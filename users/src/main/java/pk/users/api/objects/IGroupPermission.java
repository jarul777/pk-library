package pk.users.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for GroupPermission table representation. Fields:
 * <ul>
 * <li>Group (identity only, permanent)</li>
 * <li>See</li>
 * <li>Edit</li>
 * <li>Permission (read only)</li>
 * </ul>
 */
public interface IGroupPermission extends IObject {

	/**
	 * Group id column value getter. No representation is needed since
	 * permissions belong to group. This field is permanent, so it won't be
	 * updated.
	 *
	 * @return Group id, zero if group permission identity has not been set.
	 */
	public int getIdGroup();

	/**
	 * Group id column value setter. Since this field is permanent, setting this
	 * on an existing object will have no effect.
	 *
	 * @param idGroup New group identity.
	 */
	public void setIdGroup(int idGroup);

	/**
	 * See column value getter.
	 *
	 * @return See permission, false if group permission identity has not been
	 * set.
	 */
	public boolean getSee();

	/**
	 * See column value setter.
	 *
	 * @param see New see permission.
	 */
	public void setSee(boolean see);

	/**
	 * Edit column value getter.
	 *
	 * @return Edit permission, false if group permission identity has not been
	 * set.
	 */
	public boolean getEdit();

	/**
	 * Edit column value setter.
	 *
	 * @param edit New edit permission.
	 */
	public void setEdit(boolean edit);

	/**
	 * Permission column value getter. This relational object is read only and
	 * won't be saved in this context, but it's id can be changed to point to a
	 * different object.
	 *
	 * @return Permission object, empty if group permission identity has not
	 * been set.
	 */
	public IPermission getPermission();

}
