package pk.users.api.objects;

// Import used java utilities
import java.util.Collection;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for group table representation. Fields
 * <ul>
 * <li>Name</li>
 * <li>Group permissions (collection)</li>
 * </ul>
 */
public interface IGroup extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Group name, null if group identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New group name.
	 */
	public void setName(String name);

	/**
	 * Getter for group permissions for this group. This is a collection of all
	 * relational objects that reference this group.
	 *
	 * @return Collection of group permissions.
	 */
	public Collection<IGroupPermission> getGroupPermissions();

}
