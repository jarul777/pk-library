package pk.users.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Permission table representation. Fields:
 * <ul>
 * <li>Name</li>
 * </ul>
 */
public interface IPermission extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Permission name, null if permission identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New permission name.
	 */
	public void setName(String name);

}
