package pk.users.api.objects;

// Import used java time libraries
import java.time.Instant;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for user table representation. Fields:
 * <ul>
 * <li>Login</li>
 * <li>Password (hash representation)</li>
 * <li>Registration time (read only)</li>
 * <li>Person (permanent)</li>
 * <li>Client (optional)</li>
 * <li>Employee (optional)</li>
 * <li>Group (read only)</li>
 * </ul>
 */
public interface IUser extends IObject {

	/**
	 * Login column value getter.
	 *
	 * @return User login, null if user identity has not been set.
	 */
	public String getLogin();

	/**
	 * Login column value setter.
	 *
	 * @param login New user login.
	 */
	public void setLogin(String login);

	/**
	 * Password column value getter. Password should be stored in the database
	 * only in hash form.
	 *
	 * @return User hashed password, null if identity has not been set.
	 */
	public String getPassword();

	/**
	 * Password column value setter. Password should be stored in the database
	 * only in hash form.
	 *
	 * @param password New user password as hash.
	 */
	public void setPassword(String password);

	/**
	 * Registration time column value getter. This field is read only and has no
	 * setter.
	 *
	 * @return Registration time, null if identity has not been set.
	 */
	public Instant getRegistrationTime();

	/**
	 * Person object getter. This field is permanent, so it won't be updated.
	 *
	 * @return Person object created for this user, empty person object if user
	 * identity has not been set.
	 */
	public IPerson getPerson();

	/**
	 * Client object getter. This field is optional and can return empty object
	 * even if user identity has been set.
	 *
	 * @return Client object created for this user, empty client object if user
	 * has no client or no identity set.
	 */
	public IClient getClient();

	/**
	 * Employee object getter. This field is optional and can return empty
	 * object even if user identity has been set.
	 *
	 * @return Employee object created for this user, empty employee if user has
	 * no employee or no identity set.
	 */
	public IEmployee getEmployee();

	/**
	 * Group permissions object getter. This relational object is read only and
	 * won't be saved in this context, but it's id can be changed to point to a
	 * different object.
	 *
	 * @return Group permissions object created for this user, empty permission
	 * if user identity has not been set.
	 */
	public IGroup getGroup();

}
