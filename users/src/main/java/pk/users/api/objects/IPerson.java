package pk.users.api.objects;

// Import used java utiliites
import java.util.Date;

// Import used common objects API
import pk.core.api.objects.IAddress;
import pk.core.api.objects.ILibrary;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Person table representation. Fields:
 * <ul>
 * <li>Name</li>
 * <li>Surname</li>
 * <li>Date of birth (optional)</li>
 * <li>Phone (optional)</li>
 * <li>Email (optional)</li>
 * <li>Library (read only)</li>
 * <li>Address (optional)</li>
 * </ul>
 */
public interface IPerson extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Person name, null if person identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New person name.
	 */
	public void setName(String name);

	/**
	 * Surname column value getter.
	 *
	 * @return Person surname, null if person identity has not been set.
	 */
	public String getSurname();

	/**
	 * Surname column value setter.
	 *
	 * @param surname New person surname.
	 */
	public void setSurname(String surname);

	/**
	 * Date of birth column value getter. This field is optional and can return
	 * null even if inventory identity has been set.
	 *
	 * @return Person date of birth name, null if person identity has not been
	 * set or this inventory item doesn't have publish date field set.
	 */
	public Date getDateOfBirth();

	/**
	 * Date of birth column value setter. This field is optional, this object
	 * can be saved without this being set.
	 *
	 * @param dob New person date of birth.
	 */
	public void setDateOfBirth(Date dob);

	/**
	 * Phone column value getter. This field is optional and can return null
	 * even if inventory identity has been set.
	 *
	 * @return Person phone number, null if person identity has not been set or
	 * this inventory item doesn't have publish date field set.
	 */
	public String getPhone();

	/**
	 * Phone column value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param phone New person phone number.
	 */
	public void setPhone(String phone);

	/**
	 * Email column value getter. This field is optional and can return null
	 * even if inventory identity has been set.
	 *
	 * @return Person email address, null if person identity has not been set or
	 * this inventory item doesn't have publish date field set.
	 */
	public String getEmail();

	/**
	 * Email column value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param email New person email.
	 */
	public void setEmail(String email);

	/**
	 * Library object getter. This relational object is read only and won't be
	 * saved in this context, but it's id can be changed to point to a different
	 * object.
	 *
	 * @return Library object created for this person, empty library object if
	 * person identity has not been set.
	 */
	public ILibrary getLibrary();

	/**
	 * Address object getter. This field is optional and can return empty object
	 * even if inventory identity has been set.
	 *
	 * @return Address object created for this person, empty address object if
	 * person identity has not been set.
	 */
	public IAddress getAddress();

}
