package pk.users.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Inventory table representation. Fields:
 * <ul>
 * <li>Name</li>
 * <li>Default salary</li>
 * </ul>
 */
public interface IPosition extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Position name, null if position identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New position name.
	 */
	public void setName(String name);

	/**
	 * Default salary column value getter.
	 *
	 * @return Position default salary, zero if position identity has not been
	 * set.
	 */
	public int getDefaultSalary();

	/**
	 * Default salary column value setter.
	 *
	 * @param defaultSalary New position default salary.
	 */
	public void setDefaultSalary(int defaultSalary);

}
