package pk.users.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for employee table representation. Fields:
 * <ul>
 * <li>Salary (optional)</li>
 * <li>Position (read only)</li>
 * </ul>
 */
public interface IEmployee extends IObject {

	/**
	 * Salary column value getter. This field is optional and can return zero
	 * even if employee identity has been set.
	 *
	 * @return Employee salary, zero if inventory identity has not been set or
	 * this inventory item doesn't have publisher field set.
	 */
	public int getSalary();

	/**
	 * Salary column value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param salary New employee salary.
	 */
	public void setSalary(int salary);

	/**
	 * Position object getter. This relational object is read only and won't be
	 * saved in this context, but it's id can be changed to point to a different
	 * object.
	 *
	 * @return Position object created for this employee, empty person object if
	 * employee identity has not been set.
	 */
	public IPosition getPosition();

}
