package pk.users;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;

// Import OSGi framework libraries
import org.osgi.framework.*;

// Import used services API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import registered API
import pk.users.api.IAuthentication;
import pk.users.api.IGroups;
import pk.users.api.IUsers;

// Import registered API implementation
import pk.users.impl.Authentication;
import pk.users.impl.Groups;
import pk.users.impl.Users;

/**
 * UsersActivator for library users bundle, registers users service (for users
 * management), groups service (for groups and permissions management) and
 * authentication service, requires database service (for executing queries) and
 * queries service (for building queries).
 */
public class UsersActivator implements BundleActivator, ServiceListener {

	/**
	 * Reference to context in which this bundle was started.
	 */
	private BundleContext context;

	/**
	 * Database service reference, if not set this bundle will be in waiting
	 * mode.
	 */
	private ServiceReference databaseService;

	/**
	 * Queries service reference, if not set this bundle will be in waiting
	 * mode.
	 */
	private ServiceReference queriesService;

	/**
	 * Authentication service registration object.
	 */
	private ServiceRegistration authenticationService;

	/**
	 * Users service registration object.
	 */
	private ServiceRegistration usersService;

	/**
	 * Groups service registration object.
	 */
	private ServiceRegistration groupsService;

	/**
	 * Tries to register services of this bundle. If required services are not
	 * registered, bundle will wait until serviceChanged is invoked.
	 *
	 * @throws Exception Thrown when OSGi framework encounters an error.
	 */
	private void registerServices() throws Exception {
		// Both database and queries service must be set
		if (this.databaseService == null || this.queriesService == null) {
			return;
		}

		// Services provided must be unregistered before new ones can be set
		this.unregisterServices();

		// Get services from its references
		IDatabase database = (IDatabase) this.context.getService(
			this.databaseService);
		IQueries queries = (IQueries) this.context.getService(
			this.queriesService);

		// Register authentication service
		this.authenticationService = this.context.registerService(
			IAuthentication.class.getName(),
			new Authentication(database, queries),
			null);

		// Register users service
		this.usersService = this.context.registerService(
			IUsers.class.getName(),
			new Users(database, queries),
			null);

		// Register groups service
		this.groupsService = this.context.registerService(
			IGroups.class.getName(),
			new Groups(database, queries),
			null);
	}

	/**
	 * Unregisters services registered by this bundle in registerServices
	 * method, invoked when bundle is stopped.
	 */
	private void unregisterServices() {
		// Unregister authentication service
		if (this.authenticationService != null) {
			this.authenticationService.unregister();
		}

		// Unregister users service
		if (this.usersService != null) {
			this.usersService.unregister();
		}

		// Unregister groups service
		if (this.groupsService != null) {
			this.groupsService.unregister();
		}
	}

	/**
	 * Start bundle in given context. If services it depends on are not
	 * registered, enter waiting state (wait for serviceChanged).
	 *
	 * @param context Bundle context.
	 * @throws Exception Thrown when OSGi framework encounters an error.
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		// Save context of this bundle and add it as service listener
		this.context = context;
		this.context.addServiceListener((ServiceListener) this);

		// Try to get database service reference
		this.databaseService = context.getServiceReference(
			IDatabase.class.getName());

		// Try to get users service reference
		this.queriesService = context.getServiceReference(
			IQueries.class.getName());

		// Try to register services of this bundle
		this.registerServices();
	}

	/**
	 * Stop bundle in given context and unregister servces provided by this
	 * bundle (users and authentication).
	 *
	 * @param context Bundle context.
	 * @throws Exception Thrown when OSGi framework encounters an error.
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		// Unregister registered services
		this.unregisterServices();
	}

	/**
	 * Invoked when service is changed, checks if change occurs in one of
	 * services used by this bundle and applies changes if it does.
	 *
	 * @param event Change event with service information.
	 */
	@Override
	public void serviceChanged(ServiceEvent event) {
		// Indicates whether the change is needed or not
		boolean change = false;

		// Get service referenced by change event
		ServiceReference changingServiceRef = event.getServiceReference();
		Object service = this.context.getService(changingServiceRef);

		// Determine service type and if change is needed
		if (service instanceof IDatabase) {
			this.databaseService = changingServiceRef;
			change = true;
		} else if (service instanceof IQueries) {
			this.queriesService = changingServiceRef;
			change = true;
		}

		try {
			// Try registering services after the change
			if (change) {
				this.registerServices();
			}
		} catch (Exception exception) {
			String msg = "Error occured during service change!";
			Logger.getLogger(UsersActivator.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

}
