### System Zarządzania Biblioteką ###

# Uruchomienie #
* W systemie Linux - plik run.sh
* W systemie Windows - plik run.bat
* Z poziomu NetBeans (wymaga pluginu Maven) - Clean & Build na głównym projekcie (PK Library), Run na projekcie Library GUI

Do uruchomienia projektu wymagana jest Java w wersji 8!

# Konta #
* login MAT, hasło qwerty (admin)
* login Client, hasło qwerty (klient)
* login Cashier hasło qwerty (pracownik)

Można dodawać nowe w panelu users, można tworzyć grupy ze zdefiniowanymi uprawnieniami w panelu groups.

# Używanie #
## Panel Notifications ##
Wyświetla wypożyczone pozycje (dla konta klienckiego) oraz pozycje wydane klientom (dla konta pracowniczego), jedno konto może łączyć dwie funkcjonalności (patrz accounts w panelu users).
## Panel Library Inventory ##
Pozwala na przegląd detali, rezerwację oraz wypożyczanie pozycji dostępnych w wybranej bibliotece, pokazuje ilość dostępnych pozycji danego typu.
## Panel Global Inventory ##
Pozwala na dodawanie pozycji do globalnego inwentarza oraz przypisywanie ich do konkretnych bibliotek wraz z ich ilością.
## Panel Users ##
Edycja i dodawanie kont użytkowników.
## Panel Groups ##
Edycja i dodawanie grup użytkowników.
## Menu User ##
Pozwala się przelogować, Edit user nie ma interfejsu!
## Menu Library ##
Pozwala zmienić bibliotekę. Create new nie ma interfejsu!


# Link do dokumentacji #
https://docs.google.com/document/d/1zKVC69mwaN08QHOkqZbFiQ2MRI01HPa5zsStshJZv08/edit?usp=sharing