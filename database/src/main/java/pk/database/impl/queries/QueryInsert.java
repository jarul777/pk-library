package pk.database.impl.queries;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map;

// Import string utilities library
import org.apache.commons.lang3.StringUtils;

// Import extended base class
import pk.database.api.base.QueryBase;

// Import impemented query API
import pk.database.api.queries.IQueryInsert;

/**
 * MySQL INSERT query builder. Table name and values to set to each column are
 * required parameters to build this query.
 *
 */
public class QueryInsert extends QueryBase implements IQueryInsert {

	private String into;

	private Map<String, Object> insertValues;

	public QueryInsert(String into, Map<String, Object> values)
		throws Exception {
		if (into.isEmpty() || values.isEmpty()) {
			// Illegal empty parameters, throw exception
			String msg = "Trying to create INSERT without specifying " +
				"all required parameters!";
			throw new Exception(msg);
		} else {
			// Assign into table and insert values
			this.into = into;
			this.insertValues = values;
		}
	}

	@Override
	protected void buildQuery() {
		// Initialize string builder
		StringBuilder result = new StringBuilder("INSERT INTO " + this.into);
		result = this.ensureTrailingSpace(result);

		// Parse and append insert values
		result.append(this.parseValues());
		result = this.ensureTrailingSpace(result);

		// Save final query
		this.query = result;
		this.queryChanged = false;
	}

	private String parseValues() {
		// Reformat order columns map
		int index = 0;
		String[] formattedValues = new String[this.insertValues.size()];
		for (Map.Entry<String, Object> value
			: this.insertValues.entrySet()) {
			// Glue column with its value
			formattedValues[index] = value.getKey() + "='" +
				value.getValue() + "'";
			index++;
		}

		// Build query part
		StringBuilder result = new StringBuilder();
		result.append("SET ").
			append(StringUtils.join(formattedValues, ", "));

		// Return query part string version
		return result.toString();
	}

	@Override
	public void table(String table) {
		if (!table.isEmpty()) {
			// Set new into table and mark query changed
			this.into = table;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty table passed to INSERT query!";
			Logger.getLogger(QueryInsert.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void values(Map<String, Object> values) {
		if (!values.isEmpty()) {
			// Set new insert values and mark query changed
			this.insertValues = values;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty values list passed to INSERT query!";
			Logger.getLogger(QueryInsert.class.getName()).
				log(Level.WARNING, msg);
		}
	}

}
