package pk.database.impl.queries;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

// Import string utilities library
import org.apache.commons.lang3.StringUtils;

// Import extended base class
import pk.database.api.base.QueryBase;

// Import implemented query API
import pk.database.api.queries.IQuerySelect;

/**
 * Select query builder. Base from table name is required to build this query,
 * columns list has a default value if not user specified.
 */
public class QuerySelect extends QueryBase implements IQuerySelect {

	private static final String DEFAULT_COLUMN = "*";

	private Collection<String> columns;

	private String baseFrom;

	private Collection<Join> joinClauses;

	private Collection<String> whereClauses;

	private Collection<String> groupColumns;

	private Map<String, String> orderColumns;

	private int limit;

	public QuerySelect(String baseFrom) throws Exception {
		if (baseFrom.isEmpty()) {
			// Illegal empty table name, throw exception
			String msg = "Trying to create SELECT without base FROM table!";
			throw new Exception(msg);
		} else {
			// Assign base from table
			this.baseFrom = baseFrom;

			// Set default values
			this.columns = new HashSet<>(1);
			this.columns.add(QuerySelect.DEFAULT_COLUMN);
		}
	}

	@Override
	protected void buildQuery() {
		// Initialize string builder
		StringBuilder result = new StringBuilder();

		// Parse and append select query part
		result.append(this.parseSelectColumns());
		result = this.ensureTrailingSpace(result);

		// Append from query part
		result.append(" FROM ").append(this.baseFrom);
		result = this.ensureTrailingSpace(result);

		// Parse and append joins query part
		result.append(this.parseJoinClauses());
		result = this.ensureTrailingSpace(result);

		// Parse and append where query part
		result.append(this.parseWhereClauses());
		result = this.ensureTrailingSpace(result);

		// Parse and append group query part
		result.append(this.parseGroupColumns());
		result = this.ensureTrailingSpace(result);

		// Parse and append order query part
		result.append(this.parseOrderColumns());
		result = this.ensureTrailingSpace(result);

		// Append limit query part
		if (this.limit >= 1) {
			result.append("LIMIT ").append(this.limit);
		}

		// Save final query
		this.query = result;
		this.queryChanged = false;
	}

	private String parseSelectColumns() {
		StringBuilder result = new StringBuilder();
		result.append("SELECT ").append(StringUtils.join(this.columns, ", "));

		return result.toString();
	}

	private String parseJoinClauses() {
		if (this.joinClauses == null || this.joinClauses.isEmpty()) {
			return "";
		}

		StringBuilder result = new StringBuilder();
		for (Join joinClause : this.joinClauses) {
			result.append(joinClause.toString());
			result = this.ensureTrailingSpace(result);
		}

		return result.toString();
	}

	private String parseWhereClauses() {
		if (this.whereClauses == null || this.whereClauses.isEmpty()) {
			return "";
		}

		StringBuilder result = new StringBuilder();
		result.append("WHERE ").
			append(StringUtils.join(this.whereClauses, " AND "));

		return result.toString();
	}

	private String parseGroupColumns() {
		if (this.groupColumns == null || this.groupColumns.isEmpty()) {
			return "";
		}

		StringBuilder result = new StringBuilder();
		result.append("GROUP BY ").
			append(StringUtils.join(this.groupColumns, ", "));

		return result.toString();
	}

	private String parseOrderColumns() {
		if (this.orderColumns == null || this.orderColumns.isEmpty()) {
			return "";
		}

		// Reformat order columns map
		int index = 0;
		String[] formattedOrder = new String[this.orderColumns.size()];
		for (Map.Entry<String, String> orderColumn
			: this.orderColumns.entrySet()) {
			// Glue order column with order directions
			formattedOrder[index] = orderColumn.getKey() + " " +
				orderColumn.getValue();
			index++;
		}

		// Build query part
		StringBuilder result = new StringBuilder();
		result.append("ORDER BY ").
			append(StringUtils.join(formattedOrder, ","));

		// Return query part string version
		return result.toString();
	}

	@Override
	public void select(Collection<String> columns) {
		if (!columns.isEmpty()) {
			// Assign columns list
			this.columns = columns;
		} else {
			// Illegal empty columns list, use default
			this.columns = new HashSet<>(1);
			this.columns.add(QuerySelect.DEFAULT_COLUMN);
		}

		// Mark query as changed
		this.queryChanged = true;
	}

	@Override
	public void from(String table) {
		if (!table.isEmpty()) {
			// Set new table and mark query changed
			this.baseFrom = table;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty table passed as base FROM table!";
			Logger.getLogger(QuerySelect.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void join(String table, String type, Collection<String> on) {
		if (!table.isEmpty() && !type.isEmpty() && !on.isEmpty()) {
			// Add new join and mark query changed
			this.joinClauses.add(new Join(table, type, on));
			this.queryChanged = true;
		} else {
			// Empty values here are suspicious, log event
			String msg = "Empty JOIN parameter passed!";
			Logger.getLogger(QuerySelect.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void where(Collection<String> clauses) {
		// Empty collection here is acceptable, no logging
		this.whereClauses = clauses;
		this.queryChanged = true;
	}

	@Override
	public void group(Collection<String> columns) {
		// Empty collection here is acceptable, no logging
		this.groupColumns = columns;
		this.queryChanged = true;
	}

	@Override
	public void order(Map<String, String> columns) {
		// Empty map here is acceptable, no logging
		this.orderColumns = columns;
		this.queryChanged = true;
	}

	@Override
	public void limit(int limit) {
		if (limit >= 1) {
			this.limit = limit;
			this.queryChanged = true;
		} else {
			// Negative values here are illegal, log event
			String msg = "Illegal LIMIT parameter passed!";
			Logger.getLogger(QuerySelect.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	private class Join {

		private final String table;

		private final String type;

		private final Collection<String> on;

		private Join(String table, String type, Collection<String> on) {
			this.table = table;
			this.type = type;
			this.on = on;
		}

		@Override
		public String toString() {
			StringBuilder join = new StringBuilder(this.type);
			join.append(" JOIN ").append(this.table).append(" ON (").
				append(StringUtils.join(this.on, " AND ")).append(")");

			return join.toString();
		}

	}

}
