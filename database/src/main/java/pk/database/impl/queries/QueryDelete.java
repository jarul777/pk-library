package pk.database.impl.queries;

import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import pk.database.api.queries.IQueryDelete;
import pk.database.api.base.QueryBase;

/**
 *
 *
 */
public class QueryDelete extends QueryBase implements IQueryDelete {

	private boolean queryChanged;

	private Collection<String> deleteTables;

	private Collection<String> whereClauses;

	private Map<String, String> orderColumns;

	private int limit;

	public QueryDelete(Collection<String> tables) throws Exception {
		if (tables.isEmpty()) {
			// Illegal empty table names list, throw exception
			String msg = "Trying to create delete query without specifying " +
				"all required parameters!";
			throw new Exception(msg);
		} else {
			// Assign delete tables
			this.deleteTables = tables;
		}
	}

	@Override
	protected void buildQuery() {
		// Initialize string builder
		StringBuilder result = new StringBuilder("DELETE FROM ");
		result.append(StringUtils.join(this.deleteTables, ","));
		result = this.ensureTrailingSpace(result);

		// Parse and append where query part
		result.append(this.parseWhereClauses());
		result = this.ensureTrailingSpace(result);

		// Parse and append order query part
		result.append(this.parseOrderColumns());
		result = this.ensureTrailingSpace(result);

		// Append limit query part
		if (this.limit >= 1) {
			result.append("LIMIT ").append(this.limit);
		}

		// Save final query
		this.query = result;
		this.queryChanged = false;
	}

	private String parseWhereClauses() {
		if (this.whereClauses == null || this.whereClauses.isEmpty()) {
			return "";
		}

		StringBuilder result = new StringBuilder();
		result.append("WHERE ").
			append(StringUtils.join(this.whereClauses, " AND "));

		return result.toString();
	}

	private String parseOrderColumns() {
		if (this.orderColumns == null || this.orderColumns.isEmpty()) {
			return "";
		}

		// Reformat order columns map
		int index = 0;
		String[] formattedOrder = new String[this.orderColumns.size()];
		for (Map.Entry<String, String> orderColumn
			: this.orderColumns.entrySet()) {
			// Glue order column with order directions
			formattedOrder[index] = orderColumn.getKey() + " " +
				orderColumn.getValue();
			index++;
		}

		// Build query part
		StringBuilder result = new StringBuilder();
		result.append("ORDER BY ").
			append(StringUtils.join(formattedOrder, ","));

		// Return query part string version
		return result.toString();
	}

	@Override
	public void tables(Collection<String> tables) {
		if (!tables.isEmpty()) {
			// Set new update tables and mark query changed
			this.deleteTables = tables;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty table collection passed to delete query!";
			Logger.getLogger(QueryDelete.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void where(Collection<String> clauses) {
		// Empty collection here is acceptable, no logging
		this.whereClauses = clauses;
		this.queryChanged = true;
	}

	@Override
	public void order(Map<String, String> columns) {
		// Empty map here is acceptable, no logging
		this.orderColumns = columns;
		this.queryChanged = true;
	}

	@Override
	public void limit(int limit) {
		if (limit >= 1) {
			this.limit = limit;
			this.queryChanged = true;
		} else {
			// Negative values here are illegal, log event
			String msg = "Illegal LIMIT parameter passed!";
			Logger.getLogger(QueryDelete.class.getName()).
				log(Level.WARNING, msg);
		}
	}

}
