package pk.database.impl.queries;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import string utilities library
import org.apache.commons.lang3.StringUtils;

// Import extended base class
import pk.database.api.base.QueryBase;

// Import implemented API
import pk.database.api.queries.IQueryUpdate;

/**
 * MySQL UPDATE query builder. Names of updated tables and values to set to each
 * column are required parameters to build this query.
 *
 */
public class QueryUpdate extends QueryBase implements IQueryUpdate {

	private Collection<String> updateTables;

	private Map<String, Object> updateValues;

	private Collection<String> whereClauses;

	private Map<String, String> orderColumns;

	private int limit;

	public QueryUpdate(Collection<String> tables, Map<String, Object> values)
		throws Exception {
		if (tables.isEmpty() || values.isEmpty()) {
			// Illegal empty table name, throw exception
			String msg = "Trying to create UPDATE without specifying " +
				"all required parameters!";
			throw new Exception(msg);
		} else {
			// Assign update tables and values
			this.updateTables = tables;
			this.updateValues = values;
		}
	}

	@Override
	protected void buildQuery() {
		// Initialize string builder
		StringBuilder result = new StringBuilder("UPDATE ");
		result.append(StringUtils.join(this.updateTables, ","));
		result = this.ensureTrailingSpace(result);

		// Parse and append update values
		result.append(this.parseValues());
		result = this.ensureTrailingSpace(result);

		// Parse and append where query part
		result.append(this.parseWhereClauses());
		result = this.ensureTrailingSpace(result);

		// Parse and append order query part
		result.append(this.parseOrderColumns());
		result = this.ensureTrailingSpace(result);

		// Append limit query part
		if (this.limit >= 1) {
			result.append("LIMIT ").append(this.limit);
		}

		// Save final query
		this.query = result;
		this.queryChanged = false;
	}

	private String parseValues() {
		// Reformat order columns map
		int index = 0;
		String[] formattedValues = new String[this.updateValues.size()];
		for (Map.Entry<String, Object> value
			: this.updateValues.entrySet()) {
			// Glue column with its value
			formattedValues[index] = value.getKey() + "='" +
				value.getValue() + "'";
			index++;
		}

		// Build query part
		StringBuilder result = new StringBuilder();
		result.append("SET ").
			append(StringUtils.join(formattedValues, ","));

		// Return query part string version
		return result.toString();
	}

	private String parseWhereClauses() {
		if (this.whereClauses == null || this.whereClauses.isEmpty()) {
			return "";
		}

		StringBuilder result = new StringBuilder();
		result.append("WHERE ").
			append(StringUtils.join(this.whereClauses, " AND "));

		return result.toString();
	}

	private String parseOrderColumns() {
		if (this.orderColumns == null || this.orderColumns.isEmpty()) {
			return "";
		}

		// Reformat order columns map
		int index = 0;
		String[] formattedOrder = new String[this.orderColumns.size()];
		for (Map.Entry<String, String> orderColumn
			: this.orderColumns.entrySet()) {
			// Glue order column with order directions
			formattedOrder[index] = orderColumn.getKey() + " " +
				orderColumn.getValue();
			index++;
		}

		// Build query part
		StringBuilder result = new StringBuilder();
		result.append("ORDER BY ").
			append(StringUtils.join(formattedOrder, ","));

		// Return query part string version
		return result.toString();
	}

	@Override
	public void tables(Collection<String> tables) {
		if (!tables.isEmpty()) {
			// Set new update tables and mark query changed
			this.updateTables = tables;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty table collection passed to UPDATE query!";
			Logger.getLogger(QueryUpdate.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void values(Map<String, Object> values) {
		if (!values.isEmpty()) {
			// Set new update values and mark query changed
			this.updateValues = values;
			this.queryChanged = true;
		} else {
			// Empty string here is suspicious, log event
			String msg = "Empty values list passed to UPDATE query!";
			Logger.getLogger(QueryUpdate.class.getName()).
				log(Level.WARNING, msg);
		}
	}

	@Override
	public void where(Collection<String> clauses) {
		// Empty collection here is acceptable, no logging
		this.whereClauses = clauses;
		this.queryChanged = true;
	}

	@Override
	public void order(Map<String, String> columns) {
		// Empty map here is acceptable, no logging
		this.orderColumns = columns;
		this.queryChanged = true;
	}

	@Override
	public void limit(int limit) {
		if (limit >= 1) {
			this.limit = limit;
			this.queryChanged = true;
		} else {
			// Negative values here are illegal, log event
			String msg = "Illegal LIMIT parameter passed!";
			Logger.getLogger(QueryUpdate.class.getName()).
				log(Level.WARNING, msg);
		}
	}

}
