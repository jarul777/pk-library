package pk.database.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;

// Import query objects API
import pk.database.api.queries.IQueryDelete;
import pk.database.api.queries.IQueryInsert;
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryUpdate;

// Import implemented API
import pk.database.api.IDatabase;

/**
 * Database using MySQL database driver. Uses connection created with properties
 * taken from database properties file.
 *
 */
public class Database implements IDatabase {

	/**
	 * SQL connection object created in connection factory.
	 */
	private final Connection connection;

	/**
	 * Constructor, sets connection to this database.
	 *
	 * @param connection Connection to be used.
	 */
	public Database(Connection connection) {
		this.connection = connection;
	}

	@Override
	public ResultSet select(IQuerySelect select) {
		// Default state assumes not existing result set
		ResultSet result = null;

		try {
			// Create statement from given query object
			Statement statement = this.connection.createStatement();
			String query = select.getQuery();

			// Execute query to get result set
			result = statement.executeQuery(query);
		} catch (SQLException exception) {
			// Query failed, log error using native java logger
			String msg = "Error occured in SELECT query!";
			Logger.getLogger(Database.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public int update(IQueryUpdate update) {
		// Default state assumes no update
		int result = 0;

		try {
			// Create statement from given query object
			Statement statement = this.connection.createStatement();
			String query = update.getQuery();

			// Execute update query to get updated records count
			result = statement.executeUpdate(query);
		} catch (SQLException exception) {
			// Query failed, log error using native java logger
			String msg = "Error occured in UPDATE query!";
			Logger.getLogger(Database.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public int delete(IQueryDelete delete) {
		// Default state assumes no deletion
		int result = 0;

		try {
			// Create statement from given query object
			Statement statement = this.connection.createStatement();
			String query = delete.getQuery();

			// Execute delete query to get deleted records count
			result = statement.executeUpdate(query);
		} catch (SQLException exception) {
			// Query failed, log error using native java logger
			String msg = "Error occured in DELETE query!";
			Logger.getLogger(Database.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public int insert(IQueryInsert insert) {
		// Default state assumes not existing result set
		int result = 0;

		try {
			// Create statement from given query object
			Statement statement = this.connection.createStatement();
			String query = insert.getQuery();

			// Execute insert query to get inserted row id
			statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			// Use first key, this corresponds to the main primary key
			ResultSet mainKey = statement.getGeneratedKeys();
			mainKey.first();

			// Get generated key
			result = mainKey.getInt("GENERATED_KEY");
		} catch (SQLException exception) {
			// Query failed, log error using native java logger
			String msg = "Error occured in INSERT query!";
			Logger.getLogger(Database.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

}
