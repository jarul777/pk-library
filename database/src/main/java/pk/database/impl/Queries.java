package pk.database.impl;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import queries API
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryUpdate;
import pk.database.api.queries.IQueryDelete;
import pk.database.api.queries.IQueryInsert;

// Import used query objects implementation
import pk.database.impl.queries.*;

/**
 * Database query builders factory. This class controls the process of creating
 * query builders that support basic database operations and enforces that
 * required values are always set.
 *
 */
public class Queries implements IQueries {

	@Override
	public IQuerySelect getSelect(String baseFrom) {
		// Assume error during query creation
		IQuerySelect result = null;

		try {
			// Try to create select query
			result = new QuerySelect(baseFrom);
		} catch (Exception exception) {
			// Unable to receive query, log error using native java logger
			String msg = "Error occured during empty select query building!";
			Logger.getLogger(Queries.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public IQueryUpdate getUpdate(Collection<String> tables,
		Map<String, Object> values) {
		// Assume error during query creation
		IQueryUpdate result = null;

		try {
			// Try to create update query
			result = new QueryUpdate(tables, values);
		} catch (Exception exception) {
			// Unable to receive query, log error using native java logger
			String msg = "Error occured during empty update query building!";
			Logger.getLogger(Queries.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public IQueryDelete getDelete(Collection<String> tables) {
		// Assume error during query creation
		IQueryDelete result = null;

		try {
			// Try to create update query
			result = new QueryDelete(tables);
		} catch (Exception exception) {
			// Unable to receive query, log error using native java logger
			String msg = "Error occured during empty delete query building!";
			Logger.getLogger(Queries.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

	@Override
	public IQueryInsert getInsert(String table, Map<String, Object> values) {
		// Assume error during query creation
		IQueryInsert result = null;

		try {
			// Try to create insert query
			result = new QueryInsert(table, values);
		} catch (Exception exception) {
			// Unable to receive query, log error using native java logger
			String msg = "Error occured during empty INSERT query building!";
			Logger.getLogger(Queries.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		return result;
	}

}
