package pk.database.internal;

// Import used java I/O libraries
import java.io.IOException;
import java.io.InputStream;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

/**
 * Utility class for building database connection.
 *
 */
public class ConnectionFactory {

	/**
	 * Empty constructor (prevents class instantiation).
	 */
	private ConnectionFactory() {
	}

	/**
	 * Get connection to the database specified in database properties file.
	 *
	 * @return Connection instance, null if connection failed.
	 */
	public static Connection getConnection() {
		// Default state assumes empty connection
		Connection connection = null;

		try {
			// Load properties file and driver class
			Properties properties = ConnectionFactory.getProperties();
			Class.forName(properties.getProperty("driver"));

			// Setup the connection with database using properties file
			connection = DriverManager.getConnection(
				properties.getProperty("url"),
				properties.getProperty("user"),
				properties.getProperty("password"));
		} catch (ClassNotFoundException | SQLException exception) {
			// Connection failed, log error using native java logger
			String msg = "Error during connection creation!";
			Logger.getLogger(ConnectionFactory.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return resulting connection object
		return connection;
	}

	/**
	 * Get database properties, read from database properties file.
	 *
	 * @return Configured properties object.
	 */
	public static Properties getProperties() {
		// Default state assumes no properties are set
		Properties properties = new Properties();
		InputStream input = null;

		try {
			// Load a properties file
			input = ConnectionFactory.class.getResourceAsStream(
				"database.properties");
			properties.load(input);
		} catch (IOException exception) {
			// Loading resource failed, log error using native java logger
			String msg = "Error during properties file reading!";
			Logger.getLogger(ConnectionFactory.class.getName()).
				log(Level.SEVERE, msg, exception);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException exception) {
					// Unable to close, log error using native java logger
					String msg = "Error during properties file closing!";
					Logger.getLogger(ConnectionFactory.class.getName()).
						log(Level.SEVERE, msg, exception);
				}
			}
		}

		// Return resulting properties
		return properties;
	}

}
