package pk.database;

// Import OSGi framework libraries
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

// Import registered API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import registered API implementation
import pk.database.impl.Database;
import pk.database.impl.Queries;

// Import utility classes
import pk.database.internal.ConnectionFactory;

/**
 * DatabaseActivator for library database bundle, registers database service
 * (for executing queries) and queries service (for building queries).
 */
public class DatabaseActivator implements BundleActivator {

	/**
	 * Database service registration object.
	 */
	private ServiceRegistration databaseService;

	/**
	 * Queries service registration object.
	 */
	private ServiceRegistration queriesService;

	/**
	 * Start bundle in given context and register services provided by this
	 * bundle (database and query service).
	 *
	 * @param context Bundle context.
	 * @throws Exception Thrown when OSGi framework encounters an error.
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		// Register database service
		this.databaseService = context.
			registerService(IDatabase.class.getName(),
				new Database(ConnectionFactory.getConnection()), null);

		// Register queries service
		this.queriesService = context.registerService(
			IQueries.class.getName(), new Queries(), null);
	}

	/**
	 * Stop bundle in given context and unregister any previously registered
	 * services (database and query service).
	 *
	 * @param context Bundle context.
	 * @throws Exception Thrown when OSGi framework encounters an error.
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		// Unregister database service
		if (this.databaseService != null) {
			this.databaseService.unregister();
		}

		// Unregister queries service
		if (this.queriesService != null) {
			this.queriesService.unregister();
		}
	}

}
