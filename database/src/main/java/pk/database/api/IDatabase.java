package pk.database.api;

// Import used java libraries
import java.sql.ResultSet;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryInsert;
import pk.database.api.queries.IQueryDelete;
import pk.database.api.queries.IQueryUpdate;

/**
 * Interface for library database, it supports all standard database operations.
 * To ensure possibility of multiple implementations additional layer of
 * abstraction is used for query building.
 *
 */
public interface IDatabase {

	/**
	 * Select records using built select query.
	 *
	 * @param select Built query to execute on database.
	 * @return Set of records corresponding to issued query.
	 */
	public ResultSet select(IQuerySelect select);

	/**
	 * Update records using built update query.
	 *
	 * @param update Built update query to execute on database.
	 * @return Number of updated records.
	 */
	public int update(IQueryUpdate update);

	/**
	 * Delete records using built delete query.
	 *
	 * @param delete Built select query to execute on database.
	 * @return Number of deleted records.
	 */
	public int delete(IQueryDelete delete);

	/**
	 * Insert record into table.
	 *
	 * @param insert Built insert query to execute on database.
	 * @return Key generated for inserted row.
	 */
	public int insert(IQueryInsert insert);

}
