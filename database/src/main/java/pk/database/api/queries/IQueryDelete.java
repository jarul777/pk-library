package pk.database.api.queries;

// Import used java utilities
import java.util.Collection;
import java.util.Map;

/**
 * Interface for delete query builder.
 *
 */
public interface IQueryDelete extends IQuery {

	/**
	 * In which tables to delete records.
	 *
	 * @param tables Names of the tables on which to perform this delete.
	 */
	public void tables(Collection<String> tables);

	/**
	 * Conditions which must be met by the record to be deleted.
	 *
	 * @param clauses Textual representations of clauses to be fulfilled (ex.
	 * columnName = 1).
	 */
	public void where(Collection<String> clauses);

	/**
	 * Orders records by values from given column in specified direction.
	 *
	 * @param columns Column names mapped to sorting direction (ascending or
	 * descending).
	 */
	public void order(Map<String, String> columns);

	/**
	 * Limits selected records to given number.
	 *
	 * @param limit Limit number.
	 */
	public void limit(int limit);

}
