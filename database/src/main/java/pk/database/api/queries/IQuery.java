package pk.database.api.queries;

/**
 * Interface used for building queries that can be later used by database. It's
 * independent of driver used by database implementation and serves as a base
 * for all operations, can also be implemented as custom query builder.
 *
 */
public interface IQuery {

	/**
	 * Returns string representation of the query.
	 *
	 * @return String Textual representation of built query.
	 */
	public String getQuery();

	/**
	 * Set custom query, used if query cannot be built with supplied mechanisms.
	 *
	 * @param query Textual representation of query to be set.
	 */
	public void setQuery(String query);

}
