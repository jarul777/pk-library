package pk.database.api.queries;

// Import used java utilities
import java.util.Map;

/**
 * Interface for insert query builder.
 *
 */
public interface IQueryInsert extends IQuery {

	/**
	 * Into which table to insert records.
	 *
	 * @param table Name of the table on which to perform this insert.
	 */
	public void table(String table);

	/**
	 * What values to set in which columns, use explicitly defined values.
	 *
	 * @param values Values mapped to corresponding column keys.
	 */
	public void values(Map<String, Object> values);

}
