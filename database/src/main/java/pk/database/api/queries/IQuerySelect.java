package pk.database.api.queries;

// Import used java utilities
import java.util.Collection;
import java.util.Map;

/**
 * Interface for select query builder.
 *
 */
public interface IQuerySelect extends IQuery {

	/**
	 * Which columns or expressions should be selected.
	 *
	 * @param columns Names of columns to be selected by this query, can also
	 * contain explicit expressions, ex. COUNT(column).
	 */
	public void select(Collection<String> columns);

	/**
	 * Main table from which to select records, tables added using join method
	 * will be joined with this one.
	 *
	 * @param table Name of table to join.
	 */
	public void from(String table);

	/**
	 * Additional tables from which to select records and how to join them. This
	 * method must be invoked separately for each table to join.
	 *
	 * @param table Name of the table to join to this select.
	 * @param type Join type for this table.
	 * @param on Textual representations of clauses on which to join tables (ex.
	 * a.columnName = b.columnName)
	 */
	public void join(String table, String type, Collection<String> on);

	/**
	 * Conditions which must be met by the record to be included in the
	 * resulting record set.
	 *
	 * @param clauses Textual representations of clauses to be fulfilled (ex.
	 * columnName = 1).
	 */
	public void where(Collection<String> clauses);

	/**
	 * Group records using given column.
	 *
	 * @param columns Column names by which to group records.
	 */
	public void group(Collection<String> columns);

	/**
	 * Orders records by values from given column in specified direction.
	 *
	 * @param columns Column names mapped to sorting direction (ascending or
	 * descending).
	 */
	public void order(Map<String, String> columns);

	/**
	 * Limits selected records to given number.
	 *
	 * @param limit Limit number.
	 */
	public void limit(int limit);

}
