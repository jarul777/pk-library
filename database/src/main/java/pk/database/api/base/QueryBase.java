package pk.database.api.base;

// Import implemented API
import pk.database.api.queries.IQuery;

/**
 * Implements some core code for all query builders.
 *
 */
public abstract class QueryBase implements IQuery {

	/**
	 * Indicates whether query needs rebuilding or not. True by default.
	 */
	protected boolean queryChanged = true;

	/**
	 * Indicates whether query was set by setQuery method or not. False by
	 * default.
	 */
	protected boolean queryCustom = false;

	/**
	 * Final query builder.
	 */
	protected StringBuilder query;

	/**
	 * Creates final query object from all the pieces set separately. Will
	 * override currently set query.
	 */
	protected abstract void buildQuery();

	/**
	 * Checks whether query ends with space.
	 *
	 * @param string QueryBase builder to check.
	 * @return QueryBase builder with space appended where necessary.
	 */
	protected StringBuilder ensureTrailingSpace(StringBuilder string) {
		if (string.lastIndexOf(" ") != string.length()) {
			// No trailing space, add it
			string.append(" ");
		}

		return string;
	}

	@Override
	public String getQuery() {
		// Rebuilt query if necessary (query changed and is not custom)
		if (this.queryChanged && !this.queryCustom) {
			this.buildQuery();
		}

		// Return string version of built query
		return this.query.toString();
	}

	@Override
	public void setQuery(String query) {
		// Set new custom query (won't be rebuilt)
		this.query = new StringBuilder(query);
		this.queryCustom = true;
	}

}
