package pk.database.api;

// Import used java utilities
import java.util.Collection;
import java.util.Map;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryUpdate;
import pk.database.api.queries.IQueryDelete;
import pk.database.api.queries.IQueryInsert;

/**
 * Interface for database query builders factory. This factory will control the
 * process of creating query builders that support basic database operations and
 * enforces that required values are always set.
 *
 */
public interface IQueries {

	/**
	 * Select query builder factory, requires base from table name. If query is
	 * not modified later all columns from base table are selected.
	 *
	 * @param baseFrom Main table from which to extract records.
	 * @return Select query builder for further manipulation, null if creation
	 * failed.
	 */
	public IQuerySelect getSelect(String baseFrom);

	/**
	 * Update query builder factory, requires names of tables on which update
	 * will be performed and new values. If query is not modified later all
	 * records in given tables will be updated.
	 *
	 * @param tables Names of the tables on which to perform this update.
	 * @param values Values mapped to corresponding column keys.
	 * @return Update query builder for further manipulation, null if creation
	 * failed.
	 */
	public IQueryUpdate getUpdate(Collection<String> tables,
		Map<String, Object> values);

	/**
	 * Delete query builder factory, requires names of tables on which delete
	 * will be performed. If query is not modified later all records in given
	 * tables will be deleted.
	 *
	 * @param tables Names of the tables on which to perform this delete.
	 * @return Delete query builder for further manipulation, null if creation
	 * failed.
	 */
	public IQueryDelete getDelete(Collection<String> tables);

	/**
	 * Insert query builder factory, requires name of table on which insert will
	 * be performed and new values.
	 *
	 * @param table Name of the table on which to perform this insert.
	 * @param values Values mapped to corresponding column keys.
	 * @return Insert query builder for further manipulation, null if creation
	 * failed.
	 */
	public IQueryInsert getInsert(String table, Map<String, Object> values);

}
