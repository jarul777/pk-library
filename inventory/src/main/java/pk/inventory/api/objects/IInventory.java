package pk.inventory.api.objects;

// Import used java libraries
import java.util.Date;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Inventory table representation. Fields:
 * <ul>
 * <li>Name</li>
 * <li>Publisher (optional)</li>
 * <li>Publish date (optional)</li>
 * <li>Description (optional)</li>
 * <li>Media type (read only)</li>
 * </ul>
 */
public interface IInventory extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Inventory item name, null if inventory identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New inventory item name.
	 */
	public void setName(String name);

	/**
	 * Publisher column value getter. This field is optional and can return null
	 * even if inventory identity has been set.
	 *
	 * @return Publisher, null if inventory identity has not been set or this
	 * inventory item doesn't have publisher field set.
	 */
	public String getPublisher();

	/**
	 * Publisher column value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param publisher New inventory item publisher.
	 */
	public void setPublisher(String publisher);

	/**
	 * Publish date column value getter. This field is optional and can return
	 * null even if inventory identity has been set.
	 *
	 * @return Publish date, null if inventory identity has not been set or this
	 * inventory item doesn't have publish date field set.
	 */
	public Date getPublishDate();

	/**
	 * Publish date value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param publishDate New inventory item publish date.
	 */
	public void setPublishDate(Date publishDate);

	/**
	 * Description column value getter. This field is optional and can return
	 * null even if inventory identity has been set.
	 *
	 * @return Description, null if inventory identity has not been set or this
	 * inventory item doesn't have description field set.
	 */
	public String getDescription();

	/**
	 * Description date value setter. This field is optional, this object can be
	 * saved without this being set.
	 *
	 * @param description New inventory item description.
	 */
	public void setDescription(String description);

	/**
	 * Media type column value getter. This relational object is read only and
	 * won't be saved in this context, but it's id can be changed to point to a
	 * different object.
	 *
	 * @return Media type, empty object if inventory identity has not been set.
	 */
	public IMediaType getMediaType();

}
