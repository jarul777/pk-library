package pk.inventory.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for LibraryInventory table representation. Fields:
 * <ul>
 * <li>Library (identity only, permanent)</li>
 * <li>Count</li>
 * <li>Inventory (permanent)</li>
 * </ul>
 */
public interface ILibraryInventory extends IObject {

	/**
	 * Library id column value getter. No representation is needed since library
	 * identity will only be used to select desired records, not to get actual
	 * information about library. This field is permanent, so it won't be
	 * updated.
	 *
	 * @return Library id, zero if library inventory identity has not been set.
	 */
	public int getIdLibrary();

	/**
	 * Library id column value setter. Since this field is permanent, setting
	 * this on an existing object will have no effect.
	 *
	 * @param idLibrary New library inventory library identity.
	 */
	public void setIdLibrary(int idLibrary);

	/**
	 * Count column value getter.
	 *
	 * @return Library inventory count, zero if library inventory identity has
	 * not been set.
	 */
	public int getCount();

	/**
	 * Count column value setter.
	 *
	 * @param count New library inventory count.
	 */
	public void setCount(int count);

	/**
	 * Inventory column value getter. This relational object is permanent and
	 * can't be changed to point to a different object, but the underlying
	 * object can be modified and will be saved.
	 *
	 * @return Inventory, empty object if inventory identity has not been set.
	 */
	public IInventory getInventory();

}
