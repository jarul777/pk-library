package pk.inventory.api.objects;

// Import extended object API
import pk.core.api.IObject;

/**
 * Interface for MediaType table representation. Fields:
 * <ul>
 * <li>Name</li>
 * </ul>
 */
public interface IMediaType extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Media type name, null if media type identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New media type name.
	 */
	public void setName(String name);

}
