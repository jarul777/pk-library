package pk.inventory.api.objects;

// Import used java time libraries
import java.time.Instant;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for borrowed inventory table representation. Fields:
 * <ul>
 * <li>Client (identity only, permanent)</li>
 * <li>Employee (identity only, permanent)</li>
 * <li>Borrow time(read only)</li>
 * <li>Due time</li>
 * <li>Return time (optional)</li>
 * <li>Library inventory (read only, permanent)</li>
 * </ul>
 */
public interface IBorrowedInventory extends IObject {

	/**
	 * Client id column value getter. No representation is needed since client
	 * identity will only be used to select desired records, not to get actual
	 * information about client. This field is permanent, so it won't be
	 * updated.
	 *
	 * @return Client id, zero if borrowed inventory identity has not been set.
	 */
	public int getIdClient();

	/**
	 * Client id column value setter. Since this field is permanent, setting
	 * this on an existing object will have no effect.
	 *
	 * @param idClient New borrowed inventory client identity.
	 */
	public void setIdClient(int idClient);

	/**
	 * Employee id column value getter. No representation is needed since client
	 * identity will only be used to select desired records, not to get actual
	 * information about client. This field is permanent, so it won't be
	 * updated.
	 *
	 * @return Employee id, zero if borrowed inventory identity has not been
	 * set.
	 */
	public int getIdEmployee();

	/**
	 * Employee id column value setter. Since this field is permanent, setting
	 * this on an existing object will have no effect.
	 *
	 * @param idEmployee New borrowed inventory employee identity.
	 */
	public void setIdEmployee(int idEmployee);

	/**
	 * Due time column value getter.
	 *
	 * @return Due time, null if borrowed inventory identity has not been set.
	 */
	public Instant getDueTime();

	/**
	 * Due time column value setter.
	 *
	 * @param dueTime New borrowed inventory due time.
	 */
	public void setDueTime(Instant dueTime);

	/**
	 * Return time column value getter. This field is optional and can return
	 * null even if borrowed inventory identity has been set.
	 *
	 * @return Return time, null if borrowed inventory identity has not been set
	 * or this borrowed inventory item doesn't have return time field set.
	 */
	public Instant getReturnTime();

	/**
	 * Return time column value setter. This field is optional, this object can
	 * be saved without this being set.
	 *
	 * @param returnTime New borrowed inventory return time.
	 */
	public void setReturnTime(Instant returnTime);

	/**
	 * Borrow time column value getter. This field is read only and has no
	 * setter.
	 *
	 * @return Borrow time, null if borrowed inventory identity has not been
	 * set.
	 */
	public Instant getBorrowTime();

	/**
	 * Library inventory column value getter. This relational object is
	 * permanent and read only, so it's identity won't be updated and underlying
	 * object won't be saved if modified.
	 *
	 * @return Library inventory object, empty if borrowed inventory identity
	 * has not been set.
	 */
	public ILibraryInventory getLibraryInventory();

}
