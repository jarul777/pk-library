package pk.inventory.api.objects;

// Import used java time libraries
import java.time.Instant;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for reserved inventory table representation. Fields:
 * <ul>
 * <li>Client (identity only, permanent)</li>
 * <li>Reservation time (read only)</li>
 * <li>Expiration time</li>
 * <li>Cancelled</li>
 * <li>Library inventory (read only, permanent)</li>
 * </ul>
 */
public interface IReservedInventory extends IObject {

	/**
	 * Client id column value getter. No representation is needed since client
	 * identity will only be used to select desired records, not to get actual
	 * information about client. This field is permanent, so it won't be
	 * updated.
	 *
	 * @return Client id, zero if reserved inventory identity has not been set.
	 */
	public int getIdClient();

	/**
	 * Client id column value setter. Since this field is permanent, setting
	 * this on an existing object will have no effect.
	 *
	 * @param idClient New reserved inventory client identity.
	 */
	public void setIdClient(int idClient);

	/**
	 * Expiration time column value getter.
	 *
	 * @return Expiration time, null if reserved inventory identity has not been
	 * set.
	 */
	public Instant getExpirationTime();

	/**
	 * Expiration time column value setter.
	 *
	 * @param expirationTime New reserved inventory expiration time.
	 */
	public void setExpirationTime(Instant expirationTime);

	/**
	 * Cancelled column value getter.
	 *
	 * @return If cancelled, false if reserved inventory identity has not been
	 * set.
	 */
	public boolean getCancelled();

	/**
	 * Cancelled column value setter.
	 *
	 * @param cancelled New reserved inventory cancelled value.
	 */
	public void setCancelled(boolean cancelled);

	/**
	 * Reservation time column value getter. This field is read only and has no
	 * setter.
	 *
	 * @return Reservation time, null if reserved inventory identity has not
	 * been set.
	 */
	public Instant getReservationTime();

	/**
	 * Library inventory column value getter. This relational object is
	 * permanent and read only, so it's identity won't be updated and underlying
	 * object won't be saved if modified.
	 *
	 * @return Library inventory object, empty if reserved inventory identity
	 * has not been set.
	 */
	public ILibraryInventory getLibraryInventory();

}
