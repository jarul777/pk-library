package pk.inventory.api;

// Import used java utilities
import java.util.Collection;

// Import used objects API
import pk.inventory.api.objects.IBorrowedInventory;
import pk.inventory.api.objects.IReservedInventory;

/**
 * Interface for reservations management. Provides methods realising borrowing
 * and reserving items as well as cancelling reservations and returning borrowed
 * items. It can list borrowed or reserved items as well.
 * <p>
 * Also serves as a factory for classes:
 * <ul>
 * <li>BorrowedInventory</li>
 * <li>ReservedInventory</li>
 * </ul>
 */
public interface IReservations {
	
	public String getCardNumber(int idClient);

	/**
	 * Get list of all borrowed inventory items registered in the system that
	 * are still active - have no return date. List is built for given client,
	 * employee or inventory item parameter or any combination of those.
	 * <p>
	 * For example building list with client and inventory identity greater than
	 * zero will constrain result set to records with given inventory and client
	 * identities that are still active.
	 *
	 * @param idClient Client identity to search for among borrowed library
	 * inventory objects. If zero this is omitted.
	 * @param idEmployee Employee identity to search for among borrowed library
	 * inventory objects. If zero this is omitted.
	 * @param idLibraryInventory Library inventory identity to search for among
	 * registered borrowed inventory objects. If zero this is omitted.
	 * @return All registered borrowed inventory items with given parameters.
	 */
	public Collection<IBorrowedInventory> listBorrowedInventory(int idClient,
		int idEmployee, int idLibraryInventory);

	/**
	 * Build borrowed inventory item with given identity. if identity is zero
	 * builds empty borrowed inventory object.
	 *
	 * @param idBorrowedInventory Identity of borrowed inventory to build.
	 * @return Borrowed inventory with given identity or empty borrowed
	 * inventory object if identity was zero.
	 */
	public IBorrowedInventory getBorrowedInventory(int idBorrowedInventory);

	/**
	 * Get list of all reserved inventory items registered in the system that
	 * are still active - are not cancelled or expired. List is built for given
	 * client or inventory item parameter or any combination of those.
	 * <p>
	 * For example building list only with client identity greater than zero
	 * will constrain result set to records with given client identity.
	 *
	 * @param idClient Client identity to search for among reserved library
	 * inventory objects. If zero this is omitted.
	 * @param idLibraryInventory Library inventory identity to search for among
	 * registered reserved inventory objects. If zero this is omitted.
	 * @return All registered reserved inventory items with given parameters
	 * that ares still active.
	 */
	public Collection<IReservedInventory> listReservedInventory(int idClient,
		int idLibraryInventory);

	/**
	 * Build reserved inventory item with given identity. if identity is zero
	 * builds empty reserved inventory object.
	 *
	 * @param idReservedInventory Identity of reserved inventory to build.
	 * @return Reserved inventory with given identity or empty reserved
	 * inventory object if identity was zero.
	 */
	public IReservedInventory getReservedInventory(int idReservedInventory);

	/**
	 * Add borrowed inventory item by client card number.
	 *
	 * @param cardNumber Unique card number of client that borrowed this item.
	 * @param idEmployee Identity of employee that borrowed this item to the
	 * client.
	 * @param idLibraryInventory Inventory item being borrowed.
	 * @return Newly borrowed inventory item, null if operation failed.
	 */
	public IBorrowedInventory borrow(String cardNumber, int idEmployee,
		int idLibraryInventory);

	/**
	 * Return borrowed inventory item.
	 *
	 * @param idBorrowedInventory Identity of borrowed inventory to return.
	 * @return True if return was successful, false otherwise.
	 */
	public boolean returnBorrowed(int idBorrowedInventory);

	/**
	 * Add reserved inventory item by client card number.
	 *
	 * @param cardNumber Unique card number of client that reserved this item.
	 * @param idLibraryInventory Inventory item being reserved.
	 * @return Newly reserved inventory item.
	 */
	public IReservedInventory reserve(String cardNumber,
		int idLibraryInventory);

	/**
	 * Cancel inventory item reservation.
	 *
	 * @param idReservedInventory Identity of reserved inventory to cancel.
	 * @return True if cancel was successful, false otherwise.
	 */
	public boolean cancelReserved(int idReservedInventory);

}
