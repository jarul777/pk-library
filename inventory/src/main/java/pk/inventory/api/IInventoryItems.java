package pk.inventory.api;

// Import used java utilities
import java.util.Collection;

// Import used objects API
import pk.inventory.api.objects.IInventory;
import pk.inventory.api.objects.ILibraryInventory;
import pk.inventory.api.objects.IMediaType;

/**
 * Interface for inventory management both global and library specific. Provides
 * methods for listing inventory both registered in the system globally and for
 * specific library.
 * <p>
 * Also serves as a factory for classes:
 * <ul>
 * <li>LibraryInventory</li>
 * <li>Inventory</li>
 * </ul>
 */
public interface IInventoryItems {

	/**
	 * Get list of all library inventory items registered in the system. List is
	 * built for given library, or inventory item parameter or any combination
	 * of those.
	 * <p>
	 * For example building list only with library identity greater than zero
	 * will return all inventory items in library with this identity.
	 *
	 * @param idLibrary Library identity to search for among registered library
	 * inventory objects. If zero this is omitted.
	 * @param idInventory Inventory identity to search for among registered
	 * library inventory objects. If zero this is omitted.
	 * @return All registered library inventory items with given parameters.
	 */
	public Collection<ILibraryInventory> listLibraryInventory(int idLibrary,
		int idInventory);

	/**
	 * Build library inventory item with given identity. if identity is zero
	 * builds empty library inventory object.
	 *
	 * @param idLibraryInventory Identity of library inventory to build.
	 * @return Library inventory with given identity or empty library inventory
	 * object if identity was zero.
	 */
	public ILibraryInventory getLibraryInventory(int idLibraryInventory);

	/**
	 * Get list of all inventory items registered in the system.
	 *
	 * @return All registered inventory items.
	 */
	public Collection<IInventory> listInventory();

	/**
	 * Build inventory item with given identity. if identity is zero builds
	 * empty inventory object.
	 *
	 * @param idInventory Identity of inventory to build.
	 * @return Inventory with given identity or empty inventory object if
	 * identity was zero.
	 */
	public IInventory getInventory(int idInventory);

	/**
	 * Get list of all media types registered in the system.
	 *
	 * @return All registered media types.
	 */
	public Collection<IMediaType> listMediaTypes();

}
