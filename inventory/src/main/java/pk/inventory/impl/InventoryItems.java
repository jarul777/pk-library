package pk.inventory.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;

// Import used external libraries
import com.google.common.collect.Sets;

// Import common libraries
import pk.core.api.base.ServiceBase;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import used objects API
import pk.inventory.api.objects.IInventory;
import pk.inventory.api.objects.ILibraryInventory;
import pk.inventory.api.objects.IMediaType;

// Import used objects API implementation
import pk.inventory.impl.objects.Inventory;
import pk.inventory.impl.objects.LibraryInventory;
import pk.inventory.impl.objects.MediaType;

// Import implemented API
import pk.inventory.api.IInventoryItems;

/**
 * Inventory items factory for managing single inventory items that are global
 * or library specific by identity or getting empty inventory objects to be
 * filled with data and saved. Can also list all inventory items registered in
 * the system.
 */
public class InventoryItems extends ServiceBase implements IInventoryItems {

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public InventoryItems(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	@Override
	public Collection<ILibraryInventory> listLibraryInventory(int idLibrary,
		int idInventory) {
		// Assume empty collection as default state
		Collection<ILibraryInventory> result = Sets.newHashSet();

		// Create library inventory list query
		IQuerySelect query = this.queries.
			getSelect("pklibrary.LibraryInventory");
		query.select(Sets.newHashSet("idLibraryInventory"));

		// Add optional where clauses for library
		Collection<String> where = Sets.newHashSet();
		if (idLibrary != 0) {
			where.add("idLibrary=" + idLibrary);
		}

		// Add optional where clauses for inventory
		if (idInventory != 0) {
			where.add("idInventory=" + idInventory);
		}

		// Add where part to query
		query.where(where);

		try {
			// Execute query to get all library inventory items
			ResultSet libraryInventoryItems = this.database.select(query);
			while (libraryInventoryItems.next()) {
				// Create library inventory with identity from database
				ILibraryInventory libraryInventory = new LibraryInventory(
					database, queries);
				libraryInventory.setId(libraryInventoryItems.getInt(
					"idLibraryInventory"));

				// Save it to the collection
				result.add(libraryInventory);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during library inventory " +
				"list creation!";
			Logger.getLogger(InventoryItems.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of library inventory items
		return result;
	}

	@Override
	public ILibraryInventory getLibraryInventory(int idLibraryInventory) {
		// Create empty object and fill its data if identity is not zero
		ILibraryInventory result = new LibraryInventory(this.database,
			this.queries);
		if (idLibraryInventory != 0) {
			result.setId(idLibraryInventory);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public Collection<IInventory> listInventory() {
		// Assume empty collection as default state
		Collection<IInventory> result = Sets.newHashSet();

		// Create inventory items list
		IQuerySelect query = this.queries.getSelect("pklibrary.Inventory");
		query.select(Sets.newHashSet("idInventory"));

		try {
			// Execute query to get all inventory items
			ResultSet inventoryItems = this.database.select(query);
			while (inventoryItems.next()) {
				// Create inventory with identity from database
				IInventory inventory = new Inventory(database, queries);
				inventory.setId(inventoryItems.getInt("idInventory"));

				// Save it to the collection
				result.add(inventory);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during inventory items list creation!";
			Logger.getLogger(InventoryItems.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of users
		return result;
	}

	@Override
	public IInventory getInventory(int idInventory) {
		// Create empty object and fill its data if identity is not zero
		IInventory result = new Inventory(this.database, this.queries);
		if (idInventory != 0) {
			result.setId(idInventory);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public Collection<IMediaType> listMediaTypes() {
		// Assume empty collection as default state
		Collection<IMediaType> result = Sets.newHashSet();

		// Create media types list
		IQuerySelect query = this.queries.getSelect("pklibrary.MediaType");
		query.select(Sets.newHashSet("idMediaType"));

		try {
			// Execute query to get all media types
			ResultSet mediaTypes = this.database.select(query);
			while (mediaTypes.next()) {
				// Create media type with identity from database
				IMediaType mediaType = new MediaType(database, queries);
				mediaType.setId(mediaTypes.getInt("idMediaType"));

				// Save it to the collection
				result.add(mediaType);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during media types list creation!";
			Logger.getLogger(InventoryItems.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of users
		return result;
	}

}
