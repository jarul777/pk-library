package pk.inventory.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java time libraries
import java.time.Instant;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;

// Import used external libraries
import com.google.common.collect.Sets;

// Import common libraries
import pk.core.api.base.ServiceBase;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import used objects API
import pk.inventory.api.objects.IBorrowedInventory;
import pk.inventory.api.objects.IReservedInventory;

// Import used objects API implementations
import pk.inventory.impl.objects.BorrowedInventory;
import pk.inventory.impl.objects.ReservedInventory;

// Import implemented API
import pk.inventory.api.IReservations;

/**
 * Factory for items related with reservations and borrowing - can list borrowed
 * or reserved items or get specific borrowings or reservations. Also can
 * perform reservations and borrowings, cancel reservations and return borrowed
 * items.
 */
public class Reservations extends ServiceBase implements IReservations {

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Reservations(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Returns identity of a client with given card number. Card numbers must be
	 * unique for this to work correctly.
	 *
	 * @param cardNumber Client card number.
	 * @return Identity of client with given card number.
	 */
	protected int getIdClient(String cardNumber) {
		int result = 0;

		// Get query for client id from card number
		IQuerySelect query = this.queries.
			getSelect("pklibrary.Client");
		query.select(Sets.newHashSet("idClient"));
		query.where(Sets.newHashSet("cardNumber=" + cardNumber));

		try {
			// Query database for client with given card number
			ResultSet client = this.database.select(query);

			// Retrieve client id
			client.first();
			result = client.getInt("idClient");
		} catch (SQLException exception) {
			String msg = "Error occured during checking client id " +
				"from card number!";
			Logger.getLogger(Reservations.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return client identity for this card number
		return result;
	}

	@Override
	public String getCardNumber(int idClient) {
		String result = null;

		// Get query for card number from client id
		IQuerySelect query = this.queries.
			getSelect("pklibrary.Client");
		query.select(Sets.newHashSet("cardNumber"));
		query.where(Sets.newHashSet("idClient=" + idClient));

		try {
			// Query database for card number for client with given id
			ResultSet client = this.database.select(query);

			// Retrieve card number
			client.first();
			result = client.getString("cardNumber");
		} catch (SQLException exception) {
			String msg = "Error occured during checking card number " +
				"from client id!";
			Logger.getLogger(Reservations.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return card number for this client identity
		return result;
	}

	@Override
	public Collection<IBorrowedInventory> listBorrowedInventory(int idClient,
		int idEmployee, int idInventory) {
		// Assume empty collection as default state
		Collection<IBorrowedInventory> result = Sets.newHashSet();

		// Create borrowed inventory list query
		IQuerySelect query = this.queries.
			getSelect("pklibrary.BorrowedInventory");
		query.select(Sets.newHashSet("idBorrowedInventory"));

		// Create where clause
		Collection<String> where = Sets.newHashSet();
		where.add("returnTime IS NULL");
		
		// Add optional where clauses for client
		if (idClient != 0) {
			where.add("idClient=" + idClient);
		}

		// Add optional where clauses for employee
		if (idEmployee != 0) {
			where.add("idEmployee=" + idEmployee);
		}

		// Add optional where clauses for inventory
		if (idInventory != 0) {
			where.add("idLibraryInventory=" + idInventory);
		}

		// Add where part to query
		query.where(where);

		try {
			// Execute query to get all borrowed inventory items
			ResultSet borrowedInventoryItems = this.database.select(query);
			while (borrowedInventoryItems.next()) {
				// Create borrowed inventory with identity from database
				IBorrowedInventory borrowedInventory = new BorrowedInventory(
					database, queries);
				borrowedInventory.setId(borrowedInventoryItems.getInt(
					"idBorrowedInventory"));

				// Save it to the collection
				result.add(borrowedInventory);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during borrowed inventory " +
				"list creation!";
			Logger.getLogger(Reservations.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of borrowed inventory items
		return result;
	}

	@Override
	public IBorrowedInventory getBorrowedInventory(int idBorrowedInventory) {
		// Create empty object and fill its data if identity is not zero
		IBorrowedInventory result = new BorrowedInventory(this.database,
			this.queries);
		if (idBorrowedInventory != 0) {
			result.setId(idBorrowedInventory);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public Collection<IReservedInventory> listReservedInventory(int idClient,
		int idInventory) {
		// Assume empty collection as default state
		Collection<IReservedInventory> result = Sets.newHashSet();

		// Create reserved inventory list query
		IQuerySelect query = this.queries.
			getSelect("pklibrary.ReservedInventory");
		query.select(Sets.newHashSet("idReservedInventory"));
		Collection<String> where = Sets.newHashSet();
		where.add("expirationTime>UNIX_TIMESTAMP()");
		where.add("cancelled=0");

		// Add optional where clauses for reserved inventory
		if (idClient != 0) {
			where.add("idClient=" + idClient);
		}

		// Add optional where clauses for inventory
		if (idInventory != 0) {
			where.add("idLibraryInventory=" + idInventory);
		}

		// Add where part to query
		query.where(where);

		try {
			// Execute query to get all reserved inventory items
			ResultSet reservedInventoryItems = this.database.select(query);
			while (reservedInventoryItems.next()) {
				// Create reserved inventory with identity from database
				IReservedInventory reservedInventory = new ReservedInventory(
					database, queries);
				reservedInventory.setId(reservedInventoryItems.getInt(
					"idReservedInventory"));

				// Save it to the collection
				result.add(reservedInventory);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during reserved inventory " +
				"list creation!";
			Logger.getLogger(Reservations.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of reserved inventory items
		return result;
	}

	@Override
	public IReservedInventory getReservedInventory(int idReservedInventory) {
		// Create empty object and fill its data if identity is not zero
		IReservedInventory result = new ReservedInventory(this.database,
			this.queries);
		if (idReservedInventory != 0) {
			result.setId(idReservedInventory);
		}

		// Return prepared user object
		return result;
	}

	@Override
	public IReservedInventory reserve(String cardNumber,
		int idLibraryInventory) {
		// Assume reservation failed - initialize with empty object
		IReservedInventory result = null;

		// Create new reserved inventory object
		int idClient = this.getIdClient(cardNumber);
		if (idClient != 0) {
			result = new ReservedInventory(this.database, this.queries);
			result.setIdClient(idClient);
			result.getLibraryInventory().setId(idLibraryInventory);
			result.setExpirationTime(Instant.now().plusSeconds(100000));

			// Try to save reservation
			if (!result.save()) {
				result = null;
			}
		}

		// Return list of reserved inventory items
		return result;
	}

	@Override
	public boolean cancelReserved(int idReservedInventory) {
		IReservedInventory reservedInventory =
			this.getReservedInventory(idReservedInventory);

		// Cancel reservation in database
		reservedInventory.setCancelled(true);
		return reservedInventory.save();
	}

	@Override
	public IBorrowedInventory borrow(String cardNumber, int idEmployee,
		int idLibraryInventory) {
		// Assume borrowing failed - initialize with empty object
		IBorrowedInventory result = null;

		// Create new borrowed inventory object
		int idClient = this.getIdClient(cardNumber);
		if (idClient != 0) {
			result = new BorrowedInventory(this.database, this.queries);
			result.setIdClient(idClient);
			result.setIdEmployee(idEmployee);
			result.setDueTime(Instant.now().plusSeconds(100000));
			result.getLibraryInventory().setId(idLibraryInventory);

			// Try to save borrowing
			if (!result.save()) {
				result = null;
			}
		}

		// Return list of borrowed inventory items
		return result;
	}

	@Override
	public boolean returnBorrowed(int idBorrowedInventory) {
		IBorrowedInventory borrowedInventory =
			this.getBorrowedInventory(idBorrowedInventory);

		// Return borrowed item in database
		borrowedInventory.setReturnTime(Instant.now());
		return borrowedInventory.save();
	}

}
