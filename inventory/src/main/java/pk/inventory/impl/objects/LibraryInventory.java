package pk.inventory.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.inventory.api.objects.IInventory;

// import implemented API
import pk.inventory.api.objects.ILibraryInventory;

/**
 * Class representing GroupPermission table with idLibraryInventory as identity
 * column. New instances of this class are distinguishable by inventory.
 */
public class LibraryInventory extends ObjectBase
	implements ILibraryInventory {

	/**
	 * Identity column (idLibraryInventory).
	 */
	protected int idLibraryInventory;

	/**
	 * Reference to a library table by identity column, no library
	 * representation is necessary for this class. This field is permanent and
	 * won't be updated.
	 */
	protected int idLibrary;

	/**
	 * Representation of count column.
	 */
	protected int count;

	/**
	 * Representation of idInventory column. This relational object is permanent
	 * and can't be changed to point to a different object, but the underlying
	 * object can be modified and will be saved.
	 */
	protected IInventory inventory;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public LibraryInventory(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.inventory = new Inventory(this.database, this.queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @param insert True if this list is built for insert query, required for
	 * excluding permanent fields during update.
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns(boolean insert) {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("count", this.count);

		// Handle permanent fields
		if (insert) {
			result.put("idLibrary", this.idLibrary);
			result.put("idInventory", this.inventory.getId());
		}

		return result;
	}

	/**
	 * Get query builder to update this library inventory.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getLibraryInventoryUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.LibraryInventory");
		Map<String, Object> updateColumns = this.getColumns(false);
		Collection<String> updateWhere = Sets.newHashSet(
			"idLibraryInventory=" + this.idLibraryInventory,
			"idLibrary=" + this.idLibrary,
			"idInventory=" + this.inventory.getId());

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this library inventory.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idLibraryInventory == 0) {
			// This is a new library inventory, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.LibraryInventory", this.getColumns(true));
			this.idLibraryInventory = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idLibraryInventory > 0;
		} else {
			// This group permission already exists, do an update
			IQueryUpdate query = this.getLibraryInventoryUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idLibraryInventory=" + this.idLibraryInventory);
		IQuerySelect query = this.queries.getSelect(
			"pklibrary.LibraryInventory");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet libraryInventory = this.database.select(query);
			libraryInventory.first();

			// Fill fields with row data
			this.idLibrary = libraryInventory.getInt("idLibrary");
			this.count = libraryInventory.getInt("count");

			// Fill relational objects
			this.inventory.setId(libraryInventory.getInt("idInventory"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during library inventory " +
				"object creation!";
			Logger.getLogger(LibraryInventory.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idLibraryInventory;
	}

	@Override
	public void setId(int id) {
		this.idLibraryInventory = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public int getIdLibrary() {
		return this.idLibrary;
	}

	@Override
	public void setIdLibrary(int idLibrary) {
		this.idLibrary = idLibrary;
	}

	@Override
	public int getCount() {
		return this.count;
	}

	@Override
	public void setCount(int count) {
		this.count = count;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public IInventory getInventory() {
		return this.inventory;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save relational objects that are not read only
		this.inventory.save();

		// Save self if changed
		if (this.isChanged()) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idLibraryInventory != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.LibraryInventory"));
			Collection<String> where = Sets.newHashSet(
				"idLibraryInventory=" + this.idLibraryInventory,
				"idLibrary=" + this.idLibrary,
				"idInventory=" + this.inventory.getId());
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		// Use underlying inventory item name
		return "Library item: " + this.inventory.getName();
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 3;
		hash = 67 * hash + this.idLibraryInventory;

		// Add permission hash to distinguish newly created objects
		hash = 67 * hash + Objects.hashCode(this.inventory);

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof LibraryInventory) {
			result = this.idLibraryInventory ==
				((LibraryInventory) toCompare).getId();

			// Add inventory id to distinguish newly created objects
			result &= this.inventory.equals(
				((LibraryInventory) toCompare).getInventory());
		}

		return result;
	}

}
