package pk.inventory.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.inventory.api.objects.IMediaType;

/**
 * Class representing MediaType table with idMediaType as identity column. New
 * instances of this class are not distinguishable.
 */
public class MediaType extends ObjectBase implements IMediaType {

	/**
	 * Identity column (idMediaType).
	 */
	protected int idMediaType;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public MediaType(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Get query builder to insert this media type.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getMediaTypeInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("name", this.name);

		// Return query builder
		return this.queries.getInsert("pklibrary.MediaType", insertMap);
	}

	/**
	 * Get query builder to update this media type.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getMediaTypeUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.MediaType");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("name", this.name);
		Collection<String> updateWhere = Sets.newHashSet(
			"idMediaType=" + this.idMediaType);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this media type.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idMediaType == 0) {
			// This is a new group media type, do an insert
			IQueryInsert query = this.getMediaTypeInsert();
			this.idMediaType = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idMediaType > 0;
		} else {
			// This group media type already exists, do an update
			IQueryUpdate query = this.getMediaTypeUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idMediaType=" + this.idMediaType);
		IQuerySelect query = this.queries.getSelect("pklibrary.MediaType");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet mediaType = this.database.select(query);
			mediaType.first();

			// Fill fields with row data
			this.name = mediaType.getString("name");
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during media type object creation!";
			Logger.getLogger(MediaType.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idMediaType;
	}

	@Override
	public void setId(int id) {
		this.idMediaType = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed
		if (this.isChanged()) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idMediaType != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.MediaType"));
			Collection<String> where = Sets.newHashSet(
				"idMediaType=" + this.idMediaType);
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 5;
		hash = 17 * hash + this.idMediaType;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof MediaType) {
			result = this.idMediaType ==
				((MediaType) toCompare).getId();
		}

		return result;
	}

}
