package pk.inventory.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used java time libraries
import java.time.Instant;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.inventory.api.objects.ILibraryInventory;

// Import implemented API
import pk.inventory.api.objects.IReservedInventory;

/**
 * Class representing ReservedInventory table, id field represents
 * idReservedInventory identity column. New instances of this class are not
 * distinguishable.
 */
public class ReservedInventory extends ObjectBase implements
	IReservedInventory {

	/**
	 * Identity column (idReservedInventory).
	 */
	protected int idReservedInventory;

	/**
	 * Reference to the client table by identity column, no client
	 * representation is necessary for this class. This field is permanent and
	 * won't be updated.
	 */
	protected int idClient;

	/**
	 * Representation of reservationTime column, this field is read only and has
	 * no setter.
	 */
	protected Timestamp reservationTime;

	/**
	 * Representation of expirationTime column.
	 */
	protected Timestamp expirationTime;

	/**
	 * Representation of cancelled column
	 */
	protected boolean cancelled;

	/**
	 * Representation of idLibraryInventory column. This field is read only and
	 * permanent, so it's id won't be updated and underlying object won't be
	 * saved if modified.
	 */
	protected ILibraryInventory libraryInventory;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public ReservedInventory(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.libraryInventory = new LibraryInventory(
			this.database, this.queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @param insert True if this list is built for insert query, required for
	 * excluding permanent fields during update.
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns(boolean insert) {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("expirationTime", this.expirationTime.toString());
		result.put("cancelled", this.cancelled ? 1 : 0);

		// Handle permanent fields
		if (insert) {
			result.put("idClient", this.idClient);
			result.put("idLibraryInventory", this.libraryInventory.getId());
		}

		return result;
	}

	/**
	 * Get query builder to update this borrowed inventory.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getReservedInventoryUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.ReservedInventory");
		Map<String, Object> updateColumns = this.getColumns(false);
		Collection<String> updateWhere = Sets.newHashSet(
			"idReservedInventory=" + this.idReservedInventory,
			"idClient=" + this.idClient,
			"idLibraryInventory=" + this.libraryInventory.getId());

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this borrowed inventory.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idReservedInventory == 0) {
			// This is a new borrowedReservedInventory, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.ReservedInventory", this.getColumns(true));
			this.idReservedInventory = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idReservedInventory > 0;
		} else {
			// This borrowedReservedInventory already exists, do an update
			IQueryUpdate query = this.getReservedInventoryUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idReservedInventory=" + this.idReservedInventory);
		IQuerySelect query = this.queries.getSelect(
			"pklibrary.ReservedInventory");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet borrowedInventory = this.database.select(query);
			borrowedInventory.first();

			// Fill fields with row data
			this.idClient = borrowedInventory.getInt("idClient");
			this.reservationTime = borrowedInventory.getTimestamp(
				"reservationTime");
			this.expirationTime = borrowedInventory.getTimestamp(
				"expirationTime");
			this.cancelled = borrowedInventory.getBoolean("cancelled");

			// Fill relational objects
			this.libraryInventory.setId(borrowedInventory.getInt(
				"idLibraryInventory"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg =
				"Error occured during reserved inventory object creation!";
			Logger.getLogger(ReservedInventory.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idReservedInventory;
	}

	@Override
	public void setId(int id) {
		this.idReservedInventory = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public int getIdClient() {
		return this.idClient;
	}

	@Override
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	@Override
	public Instant getExpirationTime() {
		Instant result = null;
		if (this.expirationTime != null) {
			// Convert expiration time to instant (represents point in time)
			result = this.expirationTime.toInstant();
		}

		return result;
	}

	@Override
	public boolean getCancelled() {
		return this.cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
		
		// Object base value changed
		this.changed = true;
	}

	@Override
	public void setExpirationTime(Instant expirationTime) {
		this.expirationTime = new Timestamp(expirationTime.toEpochMilli());

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Instant getReservationTime() {
		Instant result = null;
		if (this.reservationTime != null) {
			// Convert reservation time to instant (represents point in time)
			result = this.reservationTime.toInstant();
		}

		return result;
	}

	@Override
	public ILibraryInventory getLibraryInventory() {
		return this.libraryInventory;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idReservedInventory == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idReservedInventory != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.ReservedInventory"));
			Collection<String> where = Sets.newHashSet(
				"idReservedInventory=" + this.idReservedInventory,
				"idClient=" + this.idClient,
				"idLibraryInventory=" + this.libraryInventory.getId());
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		// Use underlying inventory item name
		return "Reservation no. " + String.valueOf(this.idReservedInventory) + 
			": " +	this.libraryInventory.getInventory().getName();
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 5;
		hash = 23 * hash + this.idReservedInventory;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof ReservedInventory) {
			result = this.idReservedInventory ==
				((ReservedInventory) toCompare).getId();
		}

		return result;
	}

}
