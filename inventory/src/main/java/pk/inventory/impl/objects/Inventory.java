package pk.inventory.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java text libraries
import java.text.SimpleDateFormat;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;
import java.util.Date;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.inventory.api.objects.IMediaType;

// Import implemented API
import pk.inventory.api.objects.IInventory;

/**
 * Class representing employee table, id field represents idEmployee identity
 * column. New instances of this class are not distinguishable.
 */
public class Inventory extends ObjectBase implements IInventory {

	/**
	 * Identity column (idInventory).
	 */
	protected int idInventory;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Representation of publisher column. This is an optional field.
	 */
	protected String publisher;

	/**
	 * Representation of publishDate column. This is an optional field.
	 */
	protected Date publishDate;

	/**
	 * Representation of description column. This is an optional field.
	 */
	protected String description;

	/**
	 * Representation of idMediaType column. This field is read only in this
	 * context and won't be saved. The id itself can be changed to represent
	 * different object.
	 */
	protected IMediaType mediaType;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Inventory(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.mediaType = new MediaType(this.database, this.queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns() {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("idMediaType", this.mediaType.getId());
		result.put("name", this.name);

		// Save optional publisher if it was set
		if (this.publisher != null) {
			result.put("publisher", this.publisher);
		}

		// Save optional publish date if it was set
		if (this.publishDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
			result.put("publishDate", formatter.format(this.publishDate));
		}

		// Save optional description if it was set
		if (this.description != null) {
			result.put("description", this.description);
		}

		return result;
	}

	/**
	 * Get query builder to update this inventory.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getInventoryUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.Inventory");
		Map<String, Object> updateColumns = this.getColumns();
		Collection<String> updateWhere = Sets.newHashSet(
			"idInventory=" + this.idInventory);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this inventory item.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idInventory == 0) {
			// This is a new inventory, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.Inventory", this.getColumns());
			this.idInventory = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idInventory > 0;
		} else {
			// This inventory already exists, do an update
			IQueryUpdate query = this.getInventoryUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idInventory=" + this.idInventory);
		IQuerySelect query = this.queries.getSelect("pklibrary.Inventory");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet inventory = this.database.select(query);
			inventory.first();

			// Fill fields with row data
			this.name = inventory.getString("name");
			this.publisher = inventory.getString("publisher");
			this.publishDate = inventory.getDate("publishDate");
			this.description = inventory.getString("description");

			// Fill relational objects
			this.mediaType.setId(inventory.getInt("idMediaType"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during inventory object creation!";
			Logger.getLogger(Inventory.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idInventory;
	}

	@Override
	public void setId(int id) {
		this.idInventory = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getPublisher() {
		return this.publisher;
	}

	@Override
	public void setPublisher(String publisher) {
		this.publisher = publisher;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Date getPublishDate() {
		return this.publishDate;
	}

	@Override
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public IMediaType getMediaType() {
		return this.mediaType;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idInventory == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idInventory != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.Inventory"));
			Collection<String> where = Sets.newHashSet(
				"idInventory=" + this.idInventory);
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 7;
		hash = 89 * hash + this.idInventory;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Inventory) {
			result = this.idInventory ==
				((Inventory) toCompare).getId();
		}

		return result;
	}

}
