package pk.inventory.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used java time libraries
import java.time.Instant;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.*;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used objects API
import pk.inventory.api.objects.ILibraryInventory;

// Import implemented API
import pk.inventory.api.objects.IBorrowedInventory;

/**
 * Class representing BorrowedInventory table, id field represents
 * idBorrowedInventory identity column. New instances of this class are not
 * distinguishable.
 */
public class BorrowedInventory extends ObjectBase implements
	IBorrowedInventory {

	/**
	 * Identity column (idBorrowedInventory).
	 */
	protected int idBorrowedInventory;

	/**
	 * Reference to the client table by identity column, no client
	 * representation is necessary for this class. This field is permanent and
	 * won't be updated.
	 */
	protected int idClient;

	/**
	 * Reference to the employee table by identity column, no group
	 * representation is necessary for this class. This field is permanent and
	 * won't be updated.
	 */
	protected int idEmployee;

	/**
	 * Representation of borrowTime column, this field is read only and has no
	 * setter.
	 */
	protected Timestamp borrowTime;

	/**
	 * Representation of dueTime column.
	 */
	protected Timestamp dueTime;

	/**
	 * Representation of return time column. This is an optional field.
	 */
	protected Timestamp returnTime;

	/**
	 * Representation of idLibraryInventory column. This field is read only and
	 * permanent, so it's id won't be updated and underlying object won't be
	 * saved if modified.
	 */
	protected ILibraryInventory libraryInventory;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public BorrowedInventory(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.libraryInventory = new LibraryInventory(
			this.database, this.queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @param insert True if this list is built for insert query, required for
	 * excluding permanent fields during update.
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns(boolean insert) {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("dueTime", this.dueTime.toString());

		// Save optional return time if it was set
		if (this.returnTime != null) {
			result.put("returnTime", this.returnTime.toString());
		}

		// Handle permanent fields
		if (insert) {
			result.put("idClient", this.idClient);
			result.put("idEmployee", this.idEmployee);
			result.put("idLibraryInventory", this.libraryInventory.getId());
		}

		return result;
	}

	/**
	 * Get query builder to update this borrowed inventory.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getBorrowedInventoryUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet(
			"pklibrary.BorrowedInventory");
		Map<String, Object> updateColumns = this.getColumns(false);
		Collection<String> updateWhere = Sets.newHashSet(
			"idBorrowedInventory=" + this.idBorrowedInventory,
			"idClient=" + this.idClient,
			"idEmployee=" + this.idEmployee,
			"idLibraryInventory=" + this.libraryInventory.getId());

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this borrowed inventory.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idBorrowedInventory == 0) {
			// This is a new borrowedBorrowedInventory, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.BorrowedInventory", this.getColumns(true));
			this.idBorrowedInventory = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idBorrowedInventory > 0;
		} else {
			// This borrowedBorrowedInventory already exists, do an update
			IQueryUpdate query = this.getBorrowedInventoryUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idBorrowedInventory=" + this.idBorrowedInventory);
		IQuerySelect query = this.queries.getSelect(
			"pklibrary.BorrowedInventory");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet borrowedInventory = this.database.select(query);
			borrowedInventory.first();

			// Fill fields with row data
			this.idClient = borrowedInventory.getInt("idClient");
			this.idEmployee = borrowedInventory.getInt("idEmployee");
			this.borrowTime = borrowedInventory.getTimestamp("borrowTime");
			this.dueTime = borrowedInventory.getTimestamp("dueTime");
			this.returnTime = borrowedInventory.getTimestamp("returnTime");

			// Fill relational objects
			this.libraryInventory.setId(borrowedInventory.getInt(
				"idLibraryInventory"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg =
				"Error occured during borrowed inventory object creation!";
			Logger.getLogger(BorrowedInventory.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idBorrowedInventory;
	}

	@Override
	public void setId(int id) {
		this.idBorrowedInventory = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public int getIdClient() {
		return this.idClient;
	}

	@Override
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	@Override
	public int getIdEmployee() {
		return this.idEmployee;
	}

	@Override
	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	@Override
	public Instant getDueTime() {
		Instant result = null;
		if (this.dueTime != null) {
			// Convert due time to instant (represents point in time)
			result = this.dueTime.toInstant();
		}

		return result;
	}

	@Override
	public void setDueTime(Instant dueTime) {
		this.dueTime = new Timestamp(dueTime.toEpochMilli());

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Instant getReturnTime() {
		Instant result = null;
		if (this.returnTime != null) {
			// Convert return time to instant (represents point in time)
			result = this.returnTime.toInstant();
		}

		return result;
	}

	@Override
	public void setReturnTime(Instant returnTime) {
		this.returnTime = new Timestamp(returnTime.toEpochMilli());

		// Object base value changed
		this.changed = true;
	}

	@Override
	public Instant getBorrowTime() {
		Instant result = null;
		if (this.borrowTime != null) {
			// Convert borrow time to instant (represents point in time)
			result = this.borrowTime.toInstant();
		}

		return result;
	}

	@Override
	public ILibraryInventory getLibraryInventory() {
		return this.libraryInventory;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idBorrowedInventory == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		boolean result = false;

		if (this.idBorrowedInventory != 0) {
			// Get delete query builder object
			IQueryDelete query = this.queries.getDelete(Sets.newHashSet(
				"pklibrary.BorrowedInventory"));
			Collection<String> where = Sets.newHashSet(
				"idBorrowedInventory=" + this.idBorrowedInventory,
				"idClient=" + this.idClient,
				"idEmployee=" + this.idEmployee,
				"idLibraryInventory=" + this.libraryInventory.getId());
			query.where(where);

			// Perform a delete
			int deleted = this.database.delete(query);

			// Operation succeeded if more than one row was deleted
			result = deleted > 0;
		}

		return result;
	}

	@Override
	public String toString() {
		// Use underlying inventory item name
		return "Borrow no. " + String.valueOf(this.idBorrowedInventory) + 
			": " +	this.libraryInventory.getInventory().getName();
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 5;
		hash = 11 * hash + this.idBorrowedInventory;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof BorrowedInventory) {
			result = this.idBorrowedInventory ==
				((BorrowedInventory) toCompare).getId();
		}

		return result;
	}

}
