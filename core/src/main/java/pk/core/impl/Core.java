package pk.core.impl;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

// Import used external libraries
import com.google.common.collect.Sets;

// Import common libraries
import pk.core.api.base.ServiceBase;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;

// Import used objects API
import pk.core.api.objects.ILibrary;
import pk.core.api.objects.IAddress;

// Import used objects implementation
import pk.core.impl.objects.Library;
import pk.core.impl.objects.Address;

// Import implemented API
import pk.core.api.ICore;

/**
 * Core items factory. Handles library objects and address objects.
 */
public class Core extends ServiceBase implements ICore {

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Core(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	@Override
	public Collection<ILibrary> listLibraries() {
		// Assume empty collection as default state
		Collection<ILibrary> result = Sets.newHashSet();

		// Create libraries list
		IQuerySelect query = this.queries.getSelect("pklibrary.Library");
		query.select(Sets.newHashSet("idLibrary"));

		try {
			// Execute query to get all libraries
			ResultSet libraries = this.database.select(query);
			while (libraries.next()) {
				// Create library with identity from database
				ILibrary library = new Library(database, queries);
				library.setId(libraries.getInt("idLibrary"));

				// Save it to the collection
				result.add(library);
			}
		} catch (SQLException exception) {
			String msg = "Error occured during libraries list creation!";
			Logger.getLogger(Core.class.getName()).
				log(Level.SEVERE, msg, exception);
		}

		// Return list of libraies
		return result;
	}

	@Override
	public ILibrary getLibrary(int idLibrary) {
		// Create empty library object and fill its data if identity is not zero
		ILibrary result = new Library(this.database, this.queries);
		if (idLibrary != 0) {
			result.setId(idLibrary);
		}

		// Return prepared library object
		return result;
	}

	@Override
	public IAddress getAddress(int idAddress) {
		// Create empty address object and fill its data if identity is not zero
		IAddress result = new Address(this.database, this.queries);
		if (idAddress != 0) {
			result.setId(idAddress);
		}

		// Return prepared address object
		return result;
	}

}
