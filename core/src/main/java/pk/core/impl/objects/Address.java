package pk.core.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryInsert;
import pk.database.api.queries.IQueryUpdate;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.core.api.objects.IAddress;

/**
 * Class representing Address table, id field represents idAddress identity
 * column. New instances of this class are not distinguishable. This object
 * shouldn't be deleted and invoking delete method will have no effect.
 */
public class Address extends ObjectBase implements IAddress {

	/**
	 * Identity column (idAddress).
	 */
	protected int idAddress;

	/**
	 * Representation of street column.
	 */
	protected String street;

	/**
	 * Representation of house column.
	 */
	protected int house;

	/**
	 * Representation of flat column. This is an optional field.
	 */
	protected int flat;

	/**
	 * Representation of postalCode column.
	 */
	protected String postalCode;

	/**
	 * Representation of city column.
	 */
	protected String city;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Address(IDatabase database, IQueries queries) {
		super(database, queries);
	}

	/**
	 * Get map of columns and corresponding values.
	 *
	 * @return Mapped columns for update or insert.
	 */
	protected Map<String, Object> getColumns() {
		Map<String, Object> result = Maps.newHashMap();

		// Save required columns
		result.put("street", this.street);
		result.put("house", this.house);
		result.put("postalCode", this.postalCode);
		result.put("city", this.city);

		// Save optional flat number if it was set
		if (this.flat != 0) {
			result.put("flat", this.flat);
		}

		return result;
	}

	/**
	 * Get query builder to insert this address to database.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getAddressInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("street", this.street);
		insertMap.put("house", this.house);
		insertMap.put("postalCode", this.postalCode);
		insertMap.put("city", this.city);

		// Save optional flat number if it was set
		if (this.flat != 0) {
			insertMap.put("flat", this.flat);
		}

		// Return query builder
		return this.queries.getInsert("pklibrary.Address", insertMap);
	}

	/**
	 * Get query builder to update this address in database. Relational objects
	 * won't be updated with this query, only the base data.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getAddressUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Address");
		Map<String, Object> updateColumns = this.getColumns();

		Collection<String> updateWhere = Sets.newHashSet(
			"idAddress=" + this.idAddress);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this address. This won't save the state of relational
	 * objects, these should be handled in save method.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idAddress == 0) {
			// This is a new address, do an insert
			IQueryInsert query = this.queries.getInsert(
				"pklibrary.Address", this.getColumns());
			this.idAddress = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idAddress > 0;
		} else {
			// This address already exists, do an update
			IQueryUpdate query = this.getAddressUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idAddress=" + this.idAddress);
		IQuerySelect query = this.queries.getSelect("pklibrary.Address");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet address = this.database.select(query);
			address.first();

			// Fill fields with row data
			this.street = address.getString("street");
			this.house = address.getInt("house");
			this.flat = address.getInt("flat");
			this.postalCode = address.getString("postalCode");
			this.city = address.getString("city");
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during address object creation!";
			Logger.getLogger(Address.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idAddress;
	}

	@Override
	public void setId(int id) {
		this.idAddress = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getStreet() {
		return this.street;
	}

	@Override
	public void setStreet(String street) {
		this.street = street;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public int getHouse() {
		return this.house;
	}

	@Override
	public void setHouse(int house) {
		this.house = house;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public int getFlat() {
		return this.flat;
	}

	@Override
	public void setFlat(int flat) {
		this.flat = flat;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getPostalCode() {
		return this.postalCode;
	}

	@Override
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public String getCity() {
		return this.city;
	}

	@Override
	public void setCity(String city) {
		this.city = city;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save self if changed or new
		if (this.isChanged() || this.idAddress == 0) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Address records should never be deleted
		String msg = "Address should not be deleted!";
		Logger.getLogger(Address.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return street + " street, " + house + "/" + flat + ", " +
			postalCode + ", " + city;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 7;
		hash = 89 * hash + this.idAddress;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Address) {
			result = this.idAddress ==
				((Address) toCompare).getId();
		}

		return result;
	}

}
