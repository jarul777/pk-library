package pk.core.impl.objects;

// Import used java SQL libraries
import java.sql.SQLException;
import java.sql.ResultSet;

// Import used java utilities
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collection;
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

// Import used common libraries
import pk.core.api.base.ObjectBase;

// Import common ocject API
import pk.core.api.objects.IAddress;

// Import used query objects API
import pk.database.api.queries.IQuerySelect;
import pk.database.api.queries.IQueryInsert;
import pk.database.api.queries.IQueryUpdate;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import implemented API
import pk.core.api.objects.ILibrary;

/**
 * Class representing Library table, id field represents idLibrary identity
 * column. New instances of this class are not distinguishable. This object
 * shouldn't be deleted and invoking delete method will have no effect.
 */
public class Library extends ObjectBase implements ILibrary {

	/**
	 * Identity column (idLibrary).
	 */
	protected int idLibrary;

	/**
	 * Representation of name column.
	 */
	protected String name;

	/**
	 * Representation of idAddress column. This id is permanent and will never
	 * be updated (underlying address object can be updated).
	 */
	protected IAddress address;

	/**
	 * Constructor, calls parent to set database fields.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public Library(IDatabase database, IQueries queries) {
		super(database, queries);

		// Initialize relational fields with empty classes
		this.address = new Address(database, queries);
	}

	/**
	 * Get query builder to insert this library to database.
	 *
	 * @return Insert query builder.
	 */
	protected IQueryInsert getLibraryInsert() {
		// Prepare INSERT query collections
		Map<String, Object> insertMap = Maps.newHashMap();
		insertMap.put("name", this.name);
		insertMap.put("idAddress", this.address.getId());

		// Return query builder
		return this.queries.getInsert("pklibrary.Library", insertMap);
	}

	/**
	 * Get query builder to update this library in database. Relational objects
	 * won't be updated with this query, only the base data.
	 *
	 * @return Update query builder.
	 */
	protected IQueryUpdate getLibraryUpdate() {
		// Prepare UPDATE query collections
		Collection<String> updateTables = Sets.newHashSet("pklibrary.Library");
		Map<String, Object> updateColumns = Maps.newHashMap();
		updateColumns.put("name", this.name);

		Collection<String> updateWhere = Sets.newHashSet(
			"idLibrary=" + this.idLibrary);

		// Create query builder
		IQueryUpdate result = this.queries.
			getUpdate(updateTables, updateColumns);
		result.where(updateWhere);

		// Return query builder
		return result;
	}

	/**
	 * Save state of this library. This won't save the state of relational
	 * objects, these should be handled in save method.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	protected boolean doSave() {
		boolean result;

		// Determine what operation to execute
		if (this.idLibrary == 0) {
			// This is a new library, do an insert
			IQueryInsert query = this.getLibraryInsert();
			this.idLibrary = this.database.insert(query);

			// Operation succeeded if generated key is not zero
			result = this.idLibrary > 0;
		} else {
			// This library already exists, do an update
			IQueryUpdate query = this.getLibraryUpdate();
			int updated = this.database.update(query);

			// Operation succeeded if updated rows count is not zero
			result = updated > 0;
		}

		return result;
	}

	/**
	 * Automatically fill object properties using previously set unique id
	 * number.
	 */
	protected void fill() {
		// Prepare SELECT query
		Collection<String> whereSet = Sets.newHashSet(
			"idLibrary=" + this.idLibrary);
		IQuerySelect query = this.queries.getSelect("pklibrary.Library");
		query.where(whereSet);

		try {
			// Execute query and set result to first row
			ResultSet library = this.database.select(query);
			library.first();

			// Fill fields with row data
			this.name = library.getString("name");

			// Fill relational objects
			this.address.setId(library.getInt("idAddress"));
		} catch (SQLException exception) {
			// Unable to set some of the fields, log error
			String msg = "Error occured during library object creation!";
			Logger.getLogger(Library.class.getName()).
				log(Level.SEVERE, msg, exception);
		}
	}

	@Override
	public int getId() {
		return this.idLibrary;
	}

	@Override
	public void setId(int id) {
		this.idLibrary = id;

		// Fill the object to represent updated identity
		this.fill();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

		// Object base value changed
		this.changed = true;
	}

	@Override
	public IAddress getAddress() {
		return this.address;
	}

	@Override
	public boolean save() {
		// Assume successful save
		boolean result = true;

		// Save relational objects that are not read only
		result &= this.address.save();

		// Save self if changed
		if (this.isChanged()) {
			result &= this.doSave();
		}

		return result;
	}

	@Override
	public boolean delete() {
		// Library records should never be deleted
		String msg = "Library should not be deleted!";
		Logger.getLogger(Library.class.getName()).
			log(Level.WARNING, msg);

		return false;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		// Build hash code using fields
		int hash = 7;
		hash = 83 * hash + this.idLibrary;

		return hash;
	}

	@Override
	public boolean equals(Object toCompare) {
		boolean result = false;
		if (toCompare instanceof Library) {
			result = this.idLibrary ==
				((Library) toCompare).getId();
		}

		return result;
	}

}
