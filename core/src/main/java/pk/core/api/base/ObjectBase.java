package pk.core.api.base;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

// Import extended object API
import pk.core.api.IObject;

/**
 * Represents an object that will have to access database. Object should possess
 * unique id and be able to morph itself into another object of the same class.
 * All changes made to the object should be reflected in the database.
 */
public abstract class ObjectBase implements IObject {

	/**
	 * Whether this object was changed or not, used to determine whether update
	 * is necessary or not.
	 */
	protected boolean changed = false;

	/**
	 * Database object supplied for this connector.
	 */
	protected IDatabase database;

	/**
	 * Queries factory object supplied for this connector.
	 */
	protected IQueries queries;

	/**
	 * Constructor, acts as a setter for this connector properties.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public ObjectBase(IDatabase database, IQueries queries) {
		this.database = database;
		this.queries = queries;
	}

	/**
	 * Database object getter.
	 *
	 * @return Configured database module.
	 */
	public IDatabase getDatabase() {
		return database;
	}

	/**
	 * Database object setter.
	 *
	 * @param database Configured database module.
	 */
	public void setDatabase(IDatabase database) {
		this.database = database;
	}

	/**
	 * Queries object getter.
	 *
	 * @return Configured queries module.
	 */
	public IQueries getQueries() {
		return queries;
	}

	/**
	 * Queries object setter.
	 *
	 * @param queries Configured queries module.
	 */
	public void setQueries(IQueries queries) {
		this.queries = queries;
	}

	@Override
	public boolean isChanged() {
		return this.changed;
	}

	@Override
	public abstract int getId();

	@Override
	public abstract void setId(int id);

	@Override
	public abstract boolean save();

	@Override
	public abstract boolean delete();

}
