package pk.core.api.base;

// Import database API
import pk.database.api.IDatabase;
import pk.database.api.IQueries;

/**
 * Represents a service that will have to access database.
 */
public abstract class ServiceBase {

	/**
	 * Database object supplied for this connector.
	 */
	protected IDatabase database;

	/**
	 * Queries factory object supplied for this connector.
	 */
	protected IQueries queries;

	/**
	 * Constructor, acts as a setter for this connector properties.
	 *
	 * @param database Database service implementation.
	 * @param queries Queries factory service implementation.
	 */
	public ServiceBase(IDatabase database, IQueries queries) {
		this.database = database;
		this.queries = queries;
	}

	/**
	 * Database object getter.
	 *
	 * @return Configured database module.
	 */
	public IDatabase getDatabase() {
		return database;
	}

	/**
	 * Database object setter.
	 *
	 * @param database Configured database module.
	 */
	public void setDatabase(IDatabase database) {
		this.database = database;
	}

	/**
	 * Queries object getter.
	 *
	 * @return Configured queries module.
	 */
	public IQueries getQueries() {
		return queries;
	}

	/**
	 * Queries object setter.
	 *
	 * @param queries Configured queries module.
	 */
	public void setQueries(IQueries queries) {
		this.queries = queries;
	}

}
