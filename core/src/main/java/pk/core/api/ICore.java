package pk.core.api;

// Import used java utilities
import java.util.Collection;

// Import used objects API
import pk.core.api.objects.ILibrary;
import pk.core.api.objects.IAddress;

/**
 * Interface for libraries management.
 */
public interface ICore {

	/**
	 * Get list of all libraries registered in the system.
	 *
	 * @return All registered libraries.
	 */
	public Collection<ILibrary> listLibraries();

	/**
	 * Build library with given identity. if identity is zero builds empty
	 * library object.
	 *
	 * @param idLibrary Identity of library to build.
	 * @return Library with given identity or empty library object if identity
	 * was zero.
	 */
	public ILibrary getLibrary(int idLibrary);

	/**
	 * Build address with given identity. if identity is zero builds empty
	 * address object.
	 *
	 * @param idAddress Identity of address to build.
	 * @return Address with given identity or empty address object if identity
	 * was zero.
	 */
	public IAddress getAddress(int idAddress);

}
