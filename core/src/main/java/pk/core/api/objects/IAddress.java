package pk.core.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Address table representation. Fields:
 * <ul>
 * <li>Street</li>
 * <li>House</li>
 * <li>Flat (optional)</li>
 * <li>Postal code</li>
 * <li>City</li>
 * </ul>
 */
public interface IAddress extends IObject {

	/**
	 * Street column value getter.
	 *
	 * @return Street, null if address identity has not been set.
	 */
	public String getStreet();

	/**
	 * Street column value setter.
	 *
	 * @param street New street.
	 */
	public void setStreet(String street);

	/**
	 * House number column value getter.
	 *
	 * @return House number, zero if address identity has not been set.
	 */
	public int getHouse();

	/**
	 * House number column value setter.
	 *
	 * @param house New house number.
	 */
	public void setHouse(int house);

	/**
	 * Flat number column value getter. This field is optional and can return
	 * empty object even if user identity has been set.
	 *
	 * @return Flat number, zero if address identity has not been set or this
	 * address doesn't have flat field set.
	 */
	public int getFlat();

	/**
	 * Flat number column value setter. This field is optional, this object can
	 * be saved without this being set.
	 *
	 * @param flat New flat number.
	 */
	public void setFlat(int flat);

	/**
	 * Postal code column value getter.
	 *
	 * @return Postal code, null if address identity has not been set.
	 */
	public String getPostalCode();

	/**
	 * Postal code column value setter.
	 *
	 * @param postalCode New postal code.
	 */
	public void setPostalCode(String postalCode);

	/**
	 * City column value getter.
	 *
	 * @return City, null if address identity has not been set.
	 */
	public String getCity();

	/**
	 * City column value setter.
	 *
	 * @param city New city.
	 */
	public void setCity(String city);

}
