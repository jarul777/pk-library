package pk.core.api.objects;

// Import extended object interface
import pk.core.api.IObject;

/**
 * Interface for Library table representation. Fields:
 * <ul>
 * <li>Name</li>
 * <li>Address (permanent)</li>
 * </ul>
 */
public interface ILibrary extends IObject {

	/**
	 * Name column value getter.
	 *
	 * @return Library name, null if library identity has not been set.
	 */
	public String getName();

	/**
	 * Name column value setter.
	 *
	 * @param name New library name.
	 */
	public void setName(String name);

	/**
	 * Library address column value getter. This relational object is permanent,
	 * so it won't be updated.
	 *
	 * @return Library address object, empty if library identity has not been
	 * set.
	 */
	public IAddress getAddress();

}
