package pk.core.api;

/**
 * This is an interface of any object that can have saved state and can be
 * identified by unique object id number. Most obvious application for this is
 * database object representation.
 */
public interface IObject {

	/**
	 * Set object identifier. When this method is invoked the object should
	 * change itself to represent the state of new object corresponding to the
	 * newly set identifier.
	 *
	 * @param id Object identifier.
	 */
	public void setId(int id);

	/**
	 * Get object identifier.
	 *
	 * @return Object identifier.
	 */
	public int getId();

	/**
	 * Indicates whether this object was modified or not.
	 *
	 * @return True of this object was modified, false otherwise.
	 */
	public boolean isChanged();

	/**
	 * Save all elements of this object.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	public boolean save();

	/**
	 * Delete this object and all elements that reference it.
	 *
	 * @return True if save succeeded, false otherwise.
	 */
	public boolean delete();

}
