package pk.gui.panels;

// Import used java utilities
import java.util.Collection;

// Import used external libraries
import com.google.common.collect.Sets;

// Import used AWT libraries
import java.awt.Dimension;
import java.awt.Font;

// Import used AWT events
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// Import used javax controls and models
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

// Import used objects API
import pk.users.api.objects.*;

// Import used services API
import pk.users.api.IGroups;

// Import implemented API
import pk.gui.api.IRefreshable;

/**
 * Panel for managing groups and permissions assigned to them.
 */
public class GroupsPanel extends JPanel implements IRefreshable {

	/**
	 * User currently logged in.
	 */
	private IUser loggedUser;

	/**
	 * Service for managing groups and their permissions. Can be set to null,
	 * but then all interaction will yield error messages.
	 */
	private IGroups groupsService;

	/**
	 * Currently selected group object.
	 */
	private IGroup selectedGroup;

	/**
	 * Newly created group, generated by successful addGroup method.
	 */
	private IGroup newGroup;

	/**
	 * Creates new form GroupsPanel with groups service already set.
	 *
	 * @param groupsService Service to be used for groups management.
	 */
	public GroupsPanel(IUser loggedUser, IGroups groupsService) {
		// Set logged user
		this.loggedUser = loggedUser;

		// Set used services
		this.groupsService = groupsService;

		// Initialize display
		this.initComponents();
		this.initPermissionsEdit();
	}

	/**
	 * Creates new form GroupsPanel.
	 */
	public GroupsPanel() {
		// Initialize display
		this.initComponents();
	}

	/**
	 * Enable buttons appropriate for logged user's permissions. Only edit
	 * permissions are checked here, since see permissions were checked in main
	 * form.
	 */
	private void initPermissionsEdit() {
		// Get logged user permissions
		Collection<IGroupPermission> permissions = this.loggedUser.getGroup().
			getGroupPermissions();

		// Enable allowed elements
		for (IGroupPermission permission : permissions) {
			String name = permission.getPermission().getName();

			// Condition to see whether buttons should be enabled or not
			boolean enable = (name.equals("All") || name.equals("Groups")) &&
				permission.getEdit();

			// No further checking needed if enabled
			if (enable) {
				this.enableButtons(enable);
				break;
			}
		}
	}

	/**
	 * Enable or disable every button on this panel.
	 *
	 * @param enable New button enabled state.
	 */
	private void enableButtons(boolean enable) {
		this.removeGroup.setEnabled(enable);
		this.addPermission.setEnabled(enable);
		this.removePermission.setEnabled(enable);
		this.saveGroups.setEnabled(enable);
	}

	/**
	 * Creates a model for groups list.
	 *
	 * @return Default model for groups drop down list, empty if groups service
	 * is null.
	 */
	private ComboBoxModel getGroupsFieldModel() {
		// Get groups collection
		Collection<IGroup> groups = this.groupsService == null ?
			Sets.newHashSet() : this.groupsService.listGroups();

		// Fill groups field with groups using default model
		DefaultComboBoxModel groupsFieldModel = new DefaultComboBoxModel();
		for (IGroup group : groups) {
			groupsFieldModel.addElement(group);
		}

		// Return filled model
		this.selectedGroup = (IGroup) groupsFieldModel.getSelectedItem();
		return groupsFieldModel;
	}

	/**
	 * Creates a model for permissions list.
	 *
	 * @return Default model for permissions drop down list, empty if groups
	 * service is null.
	 */
	private ComboBoxModel getPermissionsFieldModel() {
		// Get permissions collection
		Collection<IPermission> permissions = this.groupsService == null ?
			Sets.newHashSet() : this.groupsService.listPermissions();

		// Fill groups field with permissions using default model
		DefaultComboBoxModel permissionsFieldModel = new DefaultComboBoxModel();
		for (IPermission permission : permissions) {
			permissionsFieldModel.addElement(permission);
		}

		// Return filled model
		return permissionsFieldModel;
	}

	/**
	 * Creates a model for permissions table.
	 *
	 * @return Default model for permissions table, contains single empty row if
	 * groups service is null.
	 */
	private TableModel getPermissionsTableModel() {
		// Get group permissions collection for selected user
		Collection<IGroupPermission> groupPermissions =
			this.selectedGroup == null ? Sets.newHashSet() :
				this.selectedGroup.getGroupPermissions();

		int i = 0;
		String[] columnNames = {"Permission", "See", "Edit"};
		Object[][] data = new Object[groupPermissions.size()][3];
		for (IGroupPermission groupPermission : groupPermissions) {
			// Fill data row with group permission data
			data[i][0] = groupPermission.getPermission();
			data[i][1] = groupPermission.getSee();
			data[i][2] = groupPermission.getEdit();
			i++;
		}

		// Create table default model
		DefaultTableModel permissionsTableModel =
			new DefaultTableModel(data, columnNames) {
				Class[] types = new Class[] {
					IPermission.class,
					Boolean.class,
					Boolean.class
				};

				@Override
				public Class getColumnClass(int columnIndex) {
					return types[columnIndex];
				}

			};

		// Return filled model
		return permissionsTableModel;
	}

	/**
	 * Sets drop down list of permissions as editor for permissions column.
	 */
	private void setPermissionsTableEditors() {
		// Get column with permission name
		TableColumn permissionColumn = this.permissionsTable.
			getColumnModel().getColumn(0);

		// Create drop down with permissions list
		JComboBox permissionsField =
			new JComboBox(this.getPermissionsFieldModel());

		// Set created drop down as permission editor
		permissionColumn.setCellEditor(new DefaultCellEditor(permissionsField));
	}

	/**
	 * Sets collection of group permissions selected in permissions table to
	 * given group object.
	 *
	 * @param group Group to assign selected permissions to.
	 */
	private void setNewGroupPermissions(IGroup group) {
		TableModel tab = this.permissionsTable.getModel();
		Collection<IGroupPermission> groupPermissions =
			group.getGroupPermissions();
		for (int i = 0; i < tab.getRowCount(); i++) {
			// Don't save empty rows
			if (tab.getValueAt(i, 0) == null) {
				continue;
			}

			// Create group permission with selected permission
			IGroupPermission tempGroupPermission =
				this.groupsService.getGroupPermission(0);

			// Set permission object
			tempGroupPermission.getPermission().setId(
				((IPermission) tab.getValueAt(i, 0)).getId());

			// Set see and edit permissions
			tempGroupPermission.setSee((boolean) tab.getValueAt(i, 1));
			tempGroupPermission.setEdit((boolean) tab.getValueAt(i, 2));

			// Add created group permission to group
			groupPermissions.add(tempGroupPermission);
		}
	}

	/**
	 * Add new group with data set in this form. If group is created
	 * successfully, save it to newGroup field for further use.
	 *
	 * @return True if group was added successfully, false otherwise.
	 */
	private boolean addGroup() {
		// Create temporary group
		IGroup tempGroup = this.groupsService.getGroup(0);
		tempGroup.setName(this.createGroupField.getText());

		// Set permissions set in this form to temporary group
		this.setNewGroupPermissions(tempGroup);

		// Save created group
		boolean result = tempGroup.save();
		if (result) {
			this.newGroup = tempGroup;
		} else {
			this.newGroup = null;
		}

		return result;
	}

	/**
	 * Update selected group with new permissions list.
	 *
	 * @return True if group was updated successfully, false otherwise.
	 */
	private boolean updateGroup() {
		// Assume positive result (if nothing is deleted)
		boolean result = true;

		// Delete existing group permissions from database and selected group
		Collection<IGroupPermission> groupPermissions =
			this.selectedGroup.getGroupPermissions();
		for (IGroupPermission groupPermission : groupPermissions) {
			result &= groupPermission.delete();
		}

		// Clear permissions from group, new list will be created
		groupPermissions.clear();

		// Set permissions set in this form to selected group
		this.setNewGroupPermissions(this.selectedGroup);

		// Save modified permissions
		result &= this.selectedGroup.save();

		return result;
	}

	@Override
	public void refreshState() {
		// Update models and editors
		this.selectGroupField.setModel(this.getGroupsFieldModel());
		this.permissionsTable.setModel(this.getPermissionsTableModel());
		this.setPermissionsTableEditors();
	}

	/**
	 * This method is called from within the constructor to initialise the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMessage = new JDialog();
        popupMessageField = new JLabel();
        selectGroupLabel = new JLabel();
        selectGroupField = new JComboBox();
        createGroupLabel = new JLabel();
        createGroupField = new JTextField();
        permissionsLabel = new JLabel();
        permissionsSeparator = new JSeparator();
        saveGroups = new JButton();
        permissionsScrollPane = new JScrollPane();
        permissionsTable = new JTable();
        addPermission = new JButton();
        removePermission = new JButton();
        saveSeparator = new JSeparator();
        removeGroup = new JButton();

        popupMessage.setTitle("Groups Tab Info");
        popupMessage.setAlwaysOnTop(true);
        popupMessage.setMinimumSize(new Dimension(300, 100));
        popupMessage.setName("popupMessage"); // NOI18N
        popupMessage.setResizable(false);

        popupMessageField.setText("No message");
        popupMessageField.setName("popupMessageField"); // NOI18N

        GroupLayout popupMessageLayout = new GroupLayout(popupMessage.getContentPane());
        popupMessage.getContentPane().setLayout(popupMessageLayout);
        popupMessageLayout.setHorizontalGroup(popupMessageLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(popupMessageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(popupMessageField, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );
        popupMessageLayout.setVerticalGroup(popupMessageLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(popupMessageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(popupMessageField, GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                .addContainerGap())
        );

        setMinimumSize(new Dimension(792, 542));
        setName("Form"); // NOI18N
        setPreferredSize(new Dimension(792, 542));

        selectGroupLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        selectGroupLabel.setText("Select group");
        selectGroupLabel.setName("selectGroupLabel"); // NOI18N

        selectGroupField.setModel(getGroupsFieldModel());
        selectGroupField.setName("selectGroupField"); // NOI18N
        selectGroupField.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                selectGroupFieldItemStateChanged(evt);
            }
        });

        createGroupLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        createGroupLabel.setText("Create new group");
        createGroupLabel.setName("createGroupLabel"); // NOI18N

        createGroupField.setName("createGroupField"); // NOI18N

        permissionsLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        permissionsLabel.setText("Select permissions");
        permissionsLabel.setName("permissionsLabel"); // NOI18N

        permissionsSeparator.setName("permissionsSeparator"); // NOI18N

        saveGroups.setText("Save");
        saveGroups.setToolTipText("Click here to save changes in groups tab.");
        saveGroups.setEnabled(false);
        saveGroups.setName("saveGroups"); // NOI18N
        saveGroups.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                saveGroupsMouseClicked(evt);
            }
        });

        permissionsScrollPane.setName("permissionsScrollPane"); // NOI18N

        permissionsTable.setModel(getPermissionsTableModel());
        permissionsTable.setName("permissionsTable"); // NOI18N
        permissionsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        permissionsTable.setShowVerticalLines(false);
        this.setPermissionsTableEditors();
        permissionsScrollPane.setViewportView(permissionsTable);

        addPermission.setText("Add Permission");
        addPermission.setToolTipText("Click here to add empty permission row.");
        addPermission.setEnabled(false);
        addPermission.setName("addPermission"); // NOI18N
        addPermission.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                addPermissionMouseClicked(evt);
            }
        });

        removePermission.setText("Remove Permission");
        removePermission.setToolTipText("Click here to remove selected permission row.");
        removePermission.setEnabled(false);
        removePermission.setName("removePermission"); // NOI18N
        removePermission.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                removePermissionMouseClicked(evt);
            }
        });

        saveSeparator.setName("saveSeparator"); // NOI18N

        removeGroup.setText("Remove Group");
        removeGroup.setToolTipText("Click here to remove group selected in 'Select Group\" dialog.");
        removeGroup.setEnabled(false);
        removeGroup.setName("removeGroup"); // NOI18N
        removeGroup.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                removeGroupMouseClicked(evt);
            }
        });

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(removeGroup, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(permissionsSeparator)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(createGroupLabel)
                            .addComponent(selectGroupLabel))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(selectGroupField, 0, 626, Short.MAX_VALUE)
                            .addComponent(createGroupField)))
                    .addComponent(saveGroups, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(permissionsLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(permissionsScrollPane)
                    .addComponent(addPermission, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removePermission, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saveSeparator))
                .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(selectGroupLabel)
                    .addComponent(selectGroupField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(createGroupLabel)
                    .addComponent(createGroupField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(removeGroup)
                .addGap(18, 18, 18)
                .addComponent(permissionsSeparator, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(permissionsLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(permissionsScrollPane, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addPermission)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(removePermission)
                .addGap(18, 18, 18)
                .addComponent(saveSeparator, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(saveGroups)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void saveGroupsMouseClicked(MouseEvent evt) {//GEN-FIRST:event_saveGroupsMouseClicked
		if (!this.createGroupField.getText().isEmpty()) {
			if (this.addGroup()) {
				// Update groups model and clear new group text
				this.selectGroupField.addItem(this.newGroup);
				this.createGroupField.setText(null);

				// Set success info
				this.popupMessageField.setText("New group created!");
			} else {
				// Set fail info
				this.popupMessageField.setText("Failed to create new group!");
			}

			// Set selected group to newly creaed (or null, if creation failed)
			this.selectGroupField.setSelectedItem(this.newGroup);
		} else {
			if (this.updateGroup()) {
				// Set success info
				this.popupMessageField.setText("Group updated!");
			} else {
				// Set fail info
				this.popupMessageField.setText("Failed to update group!");
			}
		}

		// Show message with result information
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_saveGroupsMouseClicked

    private void selectGroupFieldItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_selectGroupFieldItemStateChanged
		// Update group selection and relevant models and editors
		this.selectedGroup = (IGroup) this.selectGroupField.getSelectedItem();
		this.permissionsTable.setModel(this.getPermissionsTableModel());
		this.setPermissionsTableEditors();
    }//GEN-LAST:event_selectGroupFieldItemStateChanged

    private void addPermissionMouseClicked(MouseEvent evt) {//GEN-FIRST:event_addPermissionMouseClicked
		if (this.permissionsTable.getModel() instanceof DefaultTableModel) {
			// Create empty row
			Object[] data = {null, false, false};

			// Add empty row to permissions table
			((DefaultTableModel) this.permissionsTable.getModel()).addRow(data);
		}
    }//GEN-LAST:event_addPermissionMouseClicked

    private void removePermissionMouseClicked(MouseEvent evt) {//GEN-FIRST:event_removePermissionMouseClicked
		if (this.permissionsTable.getModel() instanceof DefaultTableModel) {
			// Get selected row
			int row = this.permissionsTable.getSelectedRow();

			// Remove selected row from the table
			((DefaultTableModel) this.permissionsTable.getModel()).
				removeRow(row);
		}
    }//GEN-LAST:event_removePermissionMouseClicked

    private void removeGroupMouseClicked(MouseEvent evt) {//GEN-FIRST:event_removeGroupMouseClicked
		if (this.selectedGroup != null) {
			// Delete selected group
			if (this.selectedGroup.delete()) {
				// Set success info
				this.popupMessageField.setText("Group deleted!");
				this.refreshState();
			} else {
				// Set failure info
				this.popupMessageField.setText("Group failed to delete!");
			}
		} else {
			// Set no group selected info
			this.popupMessageField.setText("No group selected!");
		}

		// Show message with result information
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_removeGroupMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton addPermission;
    private JTextField createGroupField;
    private JLabel createGroupLabel;
    private JLabel permissionsLabel;
    private JScrollPane permissionsScrollPane;
    private JSeparator permissionsSeparator;
    private JTable permissionsTable;
    private JDialog popupMessage;
    private JLabel popupMessageField;
    private JButton removeGroup;
    private JButton removePermission;
    private JButton saveGroups;
    private JSeparator saveSeparator;
    private JComboBox selectGroupField;
    private JLabel selectGroupLabel;
    // End of variables declaration//GEN-END:variables

}
