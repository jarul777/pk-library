package pk.gui.panels;

// Import used java utilities
import java.util.Collection;

// Import used external libraries
import com.google.common.collect.Sets;

// Import used AWT libraries
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;

// Import used swing controls and models
import javax.swing.*;

// Import used swing events
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// Import used objects API
import pk.inventory.api.objects.*;
import pk.users.api.objects.IGroupPermission;
import pk.users.api.objects.IUser;
import pk.core.api.objects.ILibrary;

// Import used services API
import pk.inventory.api.IInventoryItems;
import pk.inventory.api.IReservations;

// Import implemented API
import pk.gui.api.IRefreshable;

public class LibraryInventoryPanel extends JPanel implements IRefreshable {

	private IUser loggedUser;

	/**
	 * Selected library, defaults to the library current user is registered in.
	 */
	private ILibrary selectedLibrary;

	private IInventoryItems inventoryItemsService;

	private IReservations reservationsService;

	private ILibraryInventory selectedLibraryInventory;

	/**
	 * Creates new form LibraryInventoryPanel with inventories service already
	 * set.
	 *
	 * @param inventoriesService Service to be used for inventory management.
	 * @param reservationsService Service to be used for inventory borrow and
	 * reservation.
	 */
	public LibraryInventoryPanel(IUser loggedUser,
		IInventoryItems inventoryItemsService,
		IReservations reservationsService) {
		// Set logged user and his library
		this.loggedUser = loggedUser;
		this.selectedLibrary = loggedUser.getPerson().getLibrary();

		// Set used services
		this.inventoryItemsService = inventoryItemsService;
		this.reservationsService = reservationsService;

		// Initialize component
		this.initComponents();
		this.initPermissionsSee();
		this.initPermissionsEdit();
	}

	/**
	 * Creates new form LibraryInventoryPanel.
	 */
	public LibraryInventoryPanel() {
		this.initComponents();
	}

	/**
	 * Displays tabs appropriate for logged user's permissions. Only see
	 * privileges are checked on this stage.
	 */
	private void initPermissionsSee() {
		// Get logged user permissions
		Collection<IGroupPermission> permissions = this.loggedUser.getGroup().
			getGroupPermissions();

		// Display allowed elements
		for (IGroupPermission permission : permissions) {
			String name = permission.getPermission().getName();
			if (name.equals("All") && permission.getSee()) {
				this.showAll();
				break;
			} else if (permission.getSee()) {
				this.showPermitted(name);
			}
		}
	}

	/**
	 * Show tab corresponding to given permission name.
	 *
	 * @param permissionName Name of permission for displaying some portion of
	 * the interface.
	 */
	private void showPermitted(String permissionName) {
		switch (permissionName) {
			case "Borrow":
				this.libraryInventoryTabbedPane.addTab("Borrow", this.borrowTab);
				break;
			case "Reserve":
				this.libraryInventoryTabbedPane.addTab("Reserve",
					this.reservationsTab);
				break;
			default:
				break;
		}
	}

	/**
	 * Shows all tabs.
	 */
	private void showAll() {
		// Remove all already added to prevent doubling
		this.libraryInventoryTabbedPane.removeAll();

		// Add all available tabs
		this.libraryInventoryTabbedPane.addTab("Details", this.detailsTab);
		this.libraryInventoryTabbedPane.addTab("Borrow", this.borrowTab);
		this.libraryInventoryTabbedPane.addTab("Reserve", this.reservationsTab);
	}

	/**
	 * Enable buttons appropriate for logged user's permissions. Only edit
	 * permissions are checked here, since see permissions were checked in main
	 * form and other see permissions in this panel are handled by separate
	 * method.
	 */
	private void initPermissionsEdit() {
		// Get logged user permissions
		Collection<IGroupPermission> permissions = this.loggedUser.getGroup().
			getGroupPermissions();

		// Enable allowed elements
		for (IGroupPermission permission : permissions) {
			String name = permission.getPermission().getName();

			// Condition to see whether all buttons should be enabled or not
			boolean enable = (name.equals("All") ||
				name.equals("Library Inventory")) && permission.getEdit();
			if (enable) {
				this.enableButtons(enable);
				break;
			}

			// Condition to see whether borrow buttons should be enabled or not
			boolean enableBorrow = name.equals("Borrow") && permission.getEdit();
			if (enableBorrow) {
				this.borrowButton.setEnabled(enableBorrow);
				this.borrowToReserved.setEnabled(enableBorrow);
				this.returnButton.setEnabled(enableBorrow);
			}

			// Condition to see whether reserve buttons should be enabled or not
			boolean enableReserve = name.equals("Reserve") && permission.
				getEdit();
			if (enableReserve) {
				this.reserveButton.setEnabled(enableReserve);
				this.cancelResButton.setEnabled(enableReserve);
			}
		}
	}

	/**
	 * Enable or disable every button on this panel.
	 *
	 * @param enable New button enabled state.
	 */
	private void enableButtons(boolean enable) {
		this.borrowButton.setEnabled(enable);
		this.borrowToReserved.setEnabled(enable);
		this.returnButton.setEnabled(enable);
		this.reserveButton.setEnabled(enable);
		this.cancelResButton.setEnabled(enable);
	}

	/**
	 * Creates a model for library inventory list.
	 *
	 * @return Default model for library inventory list, empty if inventory
	 * items service is null.
	 */
	private ListModel getLibraryInventoryFieldModel() {
		// Check requirements for this model and initiallize set accordingly
		Collection<ILibraryInventory> libraryInventoryItems = Sets.newHashSet();
		if (this.inventoryItemsService != null &&
			this.loggedUser != null) {
			libraryInventoryItems = this.inventoryItemsService.
				listLibraryInventory(this.selectedLibrary.getId(), 0);
		}

		// Fill library inventory field using default model
		DefaultListModel libraryInventoryFieldModel = new DefaultListModel();
		for (ILibraryInventory libraryInventory : libraryInventoryItems) {
			libraryInventoryFieldModel.addElement(libraryInventory);
		}

		// Return filled model
		return libraryInventoryFieldModel;
	}

	/**
	 * Creates a model for borrowed inventory list.
	 *
	 * @return Default model for borrowed inventory list, empty if inventory
	 * items service is null.
	 */
	private ListModel getBorrowedByFieldModel() {
		// Check requirements for this model and initiallize set accordingly
		Collection<IBorrowedInventory> borrowedInventoryItems = Sets.
			newHashSet();
		if (this.reservationsService != null &&
			this.selectedLibraryInventory != null) {
			borrowedInventoryItems = this.reservationsService.
				listBorrowedInventory(0, 0,
					this.selectedLibraryInventory.getId());
		}

		// Fill borrowed inventory field using default model
		DefaultListModel borrowedByFieldModel = new DefaultListModel();
		for (IBorrowedInventory borrowedInventory : borrowedInventoryItems) {
			borrowedByFieldModel.addElement(borrowedInventory);
		}

		// Return filled model
		return borrowedByFieldModel;
	}

	/**
	 * Creates a model for reserved inventory list.
	 *
	 * @return Default model for reserved inventory list, empty if inventory
	 * items service is null.
	 */
	private ListModel getReservedByFieldModel() {
		// Check requirements for this model and initialise set accordingly
		Collection<IReservedInventory> reservedInventoryItems = Sets.
			newHashSet();
		if (this.reservationsService != null &&
			this.selectedLibraryInventory != null) {
			reservedInventoryItems = this.reservationsService.
				listReservedInventory(0, this.selectedLibraryInventory.getId());
		}

		// Fill reserved inventory field using default model
		DefaultListModel reservedByFieldModel = new DefaultListModel();
		for (IReservedInventory reservedInventory : reservedInventoryItems) {
			reservedByFieldModel.addElement(reservedInventory);
		}

		// Return filled model
		return reservedByFieldModel;
	}

	/**
	 * Creates a model for reserved by logged user inventory list. This model is
	 * independent of selected library inventory position.
	 *
	 * @return Default model for reserved inventory list, empty if inventory
	 * items service is null.
	 */
	private ListModel getReservedByMeFieldModel() {
		// Check requirements for this model and initiallize set accordingly
		Collection<IReservedInventory> reservedInventoryItems = Sets.
			newHashSet();
		if (this.reservationsService != null &&
			this.loggedUser != null) {
			reservedInventoryItems = this.reservationsService.
				listReservedInventory(this.loggedUser.getClient().getId(), 0);
		}

		// Fill reserved inventory field using default model
		DefaultListModel reservedByFieldModel = new DefaultListModel();
		for (IReservedInventory reservedInventory : reservedInventoryItems) {
			reservedByFieldModel.addElement(reservedInventory);
		}

		// Return filled model
		return reservedByFieldModel;
	}

	/**
	 * Determines how many positions of given library inventory is available in
	 * total and currently. If no more positions are available blocks
	 * reservations and borrowing options.
	 */
	private void setAmounts() {
		// Handle null selected inventory item
		if (this.selectedLibraryInventory == null) {
			this.totalAmountValueLabel.setText("No information");
			this.availableAmountValueLabel.setText("No information");
			this.borrowClientAmountField.setText("No information");
			this.reservationAmountField.setText("No information");
			return;
		}

		// Get total amount and not available amounts
		int total = this.selectedLibraryInventory.getCount();
		int borrowed = this.reservationsService.listBorrowedInventory(
			0, 0, this.selectedLibraryInventory.getId()).size();
		int reserved = this.reservationsService.listReservedInventory(
			0, this.selectedLibraryInventory.getId()).size();
		int available = total - borrowed - reserved;

		// Set amount fields
		this.totalAmountValueLabel.setText(String.valueOf(total));
		this.availableAmountValueLabel.setText(String.valueOf(available));
		this.borrowClientAmountField.setText(String.valueOf(available));
		this.reservationAmountField.setText(String.valueOf(available));

		// If no more items are available block buttons
		if (available == 0) {
			this.borrowButton.setEnabled(false);
			this.reserveButton.setEnabled(false);
		} else {
			this.borrowButton.setEnabled(true);
			this.reserveButton.setEnabled(true);
		}
	}

	/**
	 * Set other library selection than library used by logged user.
	 *
	 * @param library New library selection.
	 */
	public void setLibrary(ILibrary library) {
		// Set new field value and refresh models
		this.selectedLibrary = library;
		this.refreshState();
	}

	@Override
	public void refreshState() {
		this.libraryInventoryField.setModel(
			this.getLibraryInventoryFieldModel());
		this.borrowedByField.setModel(this.getBorrowedByFieldModel());
		this.reservedByField.setModel(this.getReservedByFieldModel());
		this.reservedByMeField.setModel(this.getReservedByMeFieldModel());
	}

	/**
	 * This method is called from within the constructor to initialise the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        borrowTab = new JPanel();
        borrowedByScrollPane = new JScrollPane();
        borrowedByField = new JList();
        borrowedByLabel = new JLabel();
        reservedByLabel = new JLabel();
        reservedByScrollPane = new JScrollPane();
        reservedByField = new JList();
        borrowToReserved = new JButton();
        returnButton = new JButton();
        cardNumberLabel = new JLabel();
        borrowButton = new JButton();
        cardNumberField = new JTextField();
        borrowClientAmountLabel = new JLabel();
        borrowClientAmountField = new JLabel();
        reservationsTab = new JPanel();
        reservationAmountLabel = new JLabel();
        reserveButton = new JButton();
        cancelResButton = new JButton();
        reservationAmountField = new JLabel();
        reservedByMeScrollPane = new JScrollPane();
        reservedByMeField = new JList();
        reservationTimeLabel = new JLabel();
        reservationTimeField = new JLabel();
        reservationExpiresLabel = new JLabel();
        reservationExpiresField = new JLabel();
        reservedByMeLabel = new JLabel();
        popupMessage = new JDialog();
        popupMessageField = new JLabel();
        libraryInventorySplitPane = new JSplitPane();
        libraryInventoryScrollPane = new JScrollPane();
        libraryInventoryField = new JList();
        libraryInventoryTabbedPane = new JTabbedPane();
        detailsTab = new JPanel();
        nameLabel = new JLabel();
        publisherLabel = new JLabel();
        publishDateLabel = new JLabel();
        typeLabel = new JLabel();
        descriptionLabel = new JLabel();
        availableAmountLabel = new JLabel();
        nameValueLabel = new JLabel();
        publisherValueLabel = new JLabel();
        publishDateValueLabel = new JLabel();
        typeValueLabel = new JLabel();
        descriptionValueLabel = new JLabel();
        availableAmountValueLabel = new JLabel();
        totalAmountLabel = new JLabel();
        totalAmountValueLabel = new JLabel();

        borrowTab.setName("borrowTab"); // NOI18N
        borrowTab.setPreferredSize(new Dimension(275, 510));

        borrowedByScrollPane.setName("borrowedByScrollPane"); // NOI18N

        borrowedByField.setModel(getBorrowedByFieldModel());
        borrowedByField.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        borrowedByField.setName("borrowedByField"); // NOI18N
        borrowedByScrollPane.setViewportView(borrowedByField);

        borrowedByLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        borrowedByLabel.setText("Borrowed by");
        borrowedByLabel.setName("borrowedByLabel"); // NOI18N

        reservedByLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        reservedByLabel.setText("Reserved by");
        reservedByLabel.setToolTipText("");
        reservedByLabel.setName("reservedByLabel"); // NOI18N

        reservedByScrollPane.setName("reservedByScrollPane"); // NOI18N

        reservedByField.setModel(getReservedByFieldModel());
        reservedByField.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        reservedByField.setName("reservedByField"); // NOI18N
        reservedByScrollPane.setViewportView(reservedByField);

        borrowToReserved.setText("Borrow");
        borrowToReserved.setName("borrowToReserved"); // NOI18N
        borrowToReserved.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                borrowToReservedMouseClicked(evt);
            }
        });

        returnButton.setText("Return");
        returnButton.setName("returnButton"); // NOI18N
        returnButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                returnButtonMouseClicked(evt);
            }
        });

        cardNumberLabel.setText("Card Number");
        cardNumberLabel.setName("cardNumberLabel"); // NOI18N

        borrowButton.setText("Borrow");
        borrowButton.setName("borrowButton"); // NOI18N
        borrowButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                borrowButtonMouseClicked(evt);
            }
        });

        cardNumberField.setName("cardNumberField"); // NOI18N

        borrowClientAmountLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        borrowClientAmountLabel.setText("Available amount:");
        borrowClientAmountLabel.setName("borrowClientAmountLabel"); // NOI18N

        borrowClientAmountField.setText("No information");
        borrowClientAmountField.setName("borrowClientAmountField"); // NOI18N

        GroupLayout borrowTabLayout = new GroupLayout(borrowTab);
        borrowTab.setLayout(borrowTabLayout);
        borrowTabLayout.setHorizontalGroup(borrowTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(borrowTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(borrowTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(borrowedByLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(borrowedByScrollPane, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                    .addComponent(reservedByLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservedByScrollPane)
                    .addComponent(borrowToReserved, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(returnButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(borrowButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(borrowTabLayout.createSequentialGroup()
                        .addComponent(cardNumberLabel, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cardNumberField))
                    .addGroup(borrowTabLayout.createSequentialGroup()
                        .addComponent(borrowClientAmountLabel)
                        .addGap(18, 18, 18)
                        .addComponent(borrowClientAmountField, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        borrowTabLayout.setVerticalGroup(borrowTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(borrowTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(borrowedByLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(borrowedByScrollPane, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(borrowTabLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(cardNumberLabel)
                    .addComponent(cardNumberField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(borrowButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(returnButton)
                .addGap(18, 18, 18)
                .addComponent(reservedByLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reservedByScrollPane, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(borrowToReserved)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(borrowTabLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(borrowClientAmountLabel)
                    .addComponent(borrowClientAmountField))
                .addContainerGap())
        );

        reservationsTab.setName("reservationsTab"); // NOI18N
        reservationsTab.setPreferredSize(new Dimension(275, 510));

        reservationAmountLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        reservationAmountLabel.setText("Available amount:");
        reservationAmountLabel.setName("reservationAmountLabel"); // NOI18N

        reserveButton.setText("Reserve");
        reserveButton.setName("reserveButton"); // NOI18N
        reserveButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                reserveButtonMouseClicked(evt);
            }
        });

        cancelResButton.setText("Cancel Reservation");
        cancelResButton.setName("cancelResButton"); // NOI18N
        cancelResButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                cancelResButtonMouseClicked(evt);
            }
        });

        reservationAmountField.setText("No information");
        reservationAmountField.setName("reservationAmountField"); // NOI18N

        reservedByMeScrollPane.setName("reservedByMeScrollPane"); // NOI18N

        reservedByMeField.setModel(getReservedByMeFieldModel());
        reservedByMeField.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        reservedByMeField.setName("reservedByMeField"); // NOI18N
        reservedByMeScrollPane.setViewportView(reservedByMeField);

        reservationTimeLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        reservationTimeLabel.setText("Reservation Time");
        reservationTimeLabel.setName("reservationTimeLabel"); // NOI18N

        reservationTimeField.setText("No information");
        reservationTimeField.setName("reservationTimeField"); // NOI18N

        reservationExpiresLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        reservationExpiresLabel.setText("Expires Time");
        reservationExpiresLabel.setName("reservationExpiresLabel"); // NOI18N

        reservationExpiresField.setText("No information");
        reservationExpiresField.setName("reservationExpiresField"); // NOI18N

        reservedByMeLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        reservedByMeLabel.setText("Reserved by me");
        reservedByMeLabel.setName("reservedByMeLabel"); // NOI18N

        GroupLayout reservationsTabLayout = new GroupLayout(reservationsTab);
        reservationsTab.setLayout(reservationsTabLayout);
        reservationsTabLayout.setHorizontalGroup(reservationsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, reservationsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reservationsTabLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(reserveButton, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservedByMeScrollPane, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                    .addComponent(reservationTimeLabel, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservationTimeField, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservationExpiresLabel, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservationExpiresField, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reservedByMeLabel, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelResButton, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(GroupLayout.Alignment.LEADING, reservationsTabLayout.createSequentialGroup()
                        .addComponent(reservationAmountLabel)
                        .addGap(18, 18, 18)
                        .addComponent(reservationAmountField, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        reservationsTabLayout.setVerticalGroup(reservationsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(reservationsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(reservedByMeLabel)
                .addGap(18, 18, 18)
                .addComponent(reservedByMeScrollPane, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reserveButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cancelResButton)
                .addGap(18, 18, 18)
                .addComponent(reservationTimeLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reservationTimeField)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reservationExpiresLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reservationExpiresField)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addGroup(reservationsTabLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(reservationAmountLabel)
                    .addComponent(reservationAmountField))
                .addContainerGap())
        );

        popupMessage.setTitle("Inventory Tab Info");
        popupMessage.setAlwaysOnTop(true);
        popupMessage.setMinimumSize(new Dimension(300, 100));
        popupMessage.setName("popupMessage"); // NOI18N
        popupMessage.setResizable(false);

        popupMessageField.setText("No message");
        popupMessageField.setName("popupMessageField"); // NOI18N

        GroupLayout popupMessageLayout = new GroupLayout(popupMessage.getContentPane());
        popupMessage.getContentPane().setLayout(popupMessageLayout);
        popupMessageLayout.setHorizontalGroup(popupMessageLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(popupMessageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(popupMessageField, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );
        popupMessageLayout.setVerticalGroup(popupMessageLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(popupMessageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(popupMessageField, GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                .addContainerGap())
        );

        libraryInventorySplitPane.setDividerLocation(300);
        libraryInventorySplitPane.setDividerSize(10);
        libraryInventorySplitPane.setName("libraryInventorySplitPane"); // NOI18N

        libraryInventoryScrollPane.setName("libraryInventoryScrollPane"); // NOI18N

        libraryInventoryField.setModel(getLibraryInventoryFieldModel());
        libraryInventoryField.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        libraryInventoryField.setName("libraryInventoryField"); // NOI18N
        libraryInventoryField.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                libraryInventoryFieldValueChanged(evt);
            }
        });
        libraryInventoryScrollPane.setViewportView(libraryInventoryField);

        libraryInventorySplitPane.setLeftComponent(libraryInventoryScrollPane);

        libraryInventoryTabbedPane.setTabPlacement(JTabbedPane.RIGHT);
        libraryInventoryTabbedPane.setName("libraryInventoryTabbedPane"); // NOI18N

        detailsTab.setName("detailsTab"); // NOI18N

        nameLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        nameLabel.setText("Name:");
        nameLabel.setName("nameLabel"); // NOI18N

        publisherLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        publisherLabel.setText("Publisher:");
        publisherLabel.setName("publisherLabel"); // NOI18N

        publishDateLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        publishDateLabel.setText("Publish Date:");
        publishDateLabel.setName("publishDateLabel"); // NOI18N

        typeLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        typeLabel.setText("Type:");
        typeLabel.setName("typeLabel"); // NOI18N

        descriptionLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        descriptionLabel.setText("Description:");
        descriptionLabel.setName("descriptionLabel"); // NOI18N

        availableAmountLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        availableAmountLabel.setText("Available amount:");
        availableAmountLabel.setName("availableAmountLabel"); // NOI18N

        nameValueLabel.setText("No information");
        nameValueLabel.setName("nameValueLabel"); // NOI18N

        publisherValueLabel.setText("No information");
        publisherValueLabel.setName("publisherValueLabel"); // NOI18N

        publishDateValueLabel.setText("No information");
        publishDateValueLabel.setName("publishDateValueLabel"); // NOI18N

        typeValueLabel.setText("No information");
        typeValueLabel.setName("typeValueLabel"); // NOI18N

        descriptionValueLabel.setText("No information");
        descriptionValueLabel.setVerticalAlignment(SwingConstants.TOP);
        descriptionValueLabel.setName("descriptionValueLabel"); // NOI18N

        availableAmountValueLabel.setText("No information");
        availableAmountValueLabel.setName("availableAmountValueLabel"); // NOI18N

        totalAmountLabel.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        totalAmountLabel.setText("Total amount:");
        totalAmountLabel.setName("totalAmountLabel"); // NOI18N

        totalAmountValueLabel.setText("No information");
        totalAmountValueLabel.setName("totalAmountValueLabel"); // NOI18N

        GroupLayout detailsTabLayout = new GroupLayout(detailsTab);
        detailsTab.setLayout(detailsTabLayout);
        detailsTabLayout.setHorizontalGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(detailsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(detailsTabLayout.createSequentialGroup()
                        .addGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(totalAmountLabel, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(availableAmountLabel, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(availableAmountValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totalAmountValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(nameValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(descriptionValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(descriptionLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(publisherValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(publishDateValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(typeValueLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nameLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(publisherLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(publishDateLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(typeLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        detailsTabLayout.setVerticalGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(detailsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nameLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nameValueLabel)
                .addGap(18, 18, 18)
                .addComponent(publisherLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(publisherValueLabel)
                .addGap(18, 18, 18)
                .addComponent(publishDateLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(publishDateValueLabel)
                .addGap(18, 18, 18)
                .addComponent(typeLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(typeValueLabel)
                .addGap(18, 18, 18)
                .addComponent(descriptionLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(descriptionValueLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 131, Short.MAX_VALUE)
                .addGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(totalAmountLabel)
                    .addComponent(totalAmountValueLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(detailsTabLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(availableAmountLabel)
                    .addComponent(availableAmountValueLabel))
                .addContainerGap())
        );

        libraryInventoryTabbedPane.addTab("Details", detailsTab);

        libraryInventorySplitPane.setRightComponent(libraryInventoryTabbedPane);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(libraryInventorySplitPane, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(libraryInventorySplitPane)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void libraryInventoryFieldValueChanged(ListSelectionEvent evt) {//GEN-FIRST:event_libraryInventoryFieldValueChanged
		// Update library inventory selection and get its data
		Object selectedVal = this.libraryInventoryField.getSelectedValue();
		ILibraryInventory libInv = this.inventoryItemsService.
			getLibraryInventory(0);
		if (selectedVal != null) {
			this.selectedLibraryInventory = (ILibraryInventory) selectedVal;
			libInv = this.selectedLibraryInventory;
		} else {
			this.selectedLibraryInventory = null;
		}

		// Populate library inventory data
		IInventory inv = libInv.getInventory();
		this.nameValueLabel.setText(inv.getName());
		this.publisherValueLabel.setText(inv.getPublisher());
		this.typeValueLabel.setText(inv.getMediaType().getName());
		this.descriptionValueLabel.setText(inv.getDescription());

		// Set new amounts
		this.setAmounts();

		// Handle publish date (can be null)
		if (inv.getPublishDate() != null) {
			this.publishDateValueLabel.setText(inv.getPublishDate().toString());
		} else {
			this.publishDateValueLabel.setText(null);
		}

		// Refresh only relevant models for this list
		this.borrowedByField.setModel(this.getBorrowedByFieldModel());
		this.reservedByField.setModel(this.getReservedByFieldModel());
    }//GEN-LAST:event_libraryInventoryFieldValueChanged

    private void borrowButtonMouseClicked(MouseEvent evt) {//GEN-FIRST:event_borrowButtonMouseClicked
		String message = "Borrowing failed!";

		// Card number must be set and logged user must be employee
		if (!this.cardNumberField.getText().isEmpty() &&
			this.loggedUser.getEmployee().getId() != 0) {
			IBorrowedInventory borrowed =
				this.reservationsService.borrow(
					this.cardNumberField.getText(),
					this.loggedUser.getEmployee().getId(),
					this.selectedLibraryInventory.getId());

			if (borrowed.getId() != 0) {
				message = "Successfully borrowed, borrowing no. " +
					borrowed.getId();
			}

			// Refresh borrowed list model
			this.borrowedByField.setModel(this.getBorrowedByFieldModel());

			// Set new amounts
			this.setAmounts();
		}

		// Display information
		this.popupMessageField.setText(message);
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_borrowButtonMouseClicked

    private void reserveButtonMouseClicked(MouseEvent evt) {//GEN-FIRST:event_reserveButtonMouseClicked
		String message = "Reservation failed!";

		// Logged user must be client
		if (this.loggedUser.getClient().getId() != 0) {
			IReservedInventory reserved =
				this.reservationsService.reserve(
					this.loggedUser.getClient().getCardNumber(),
					this.selectedLibraryInventory.getId());

			if (reserved.getId() != 0) {
				message = "Successfully reserved, reservation no. " +
					reserved.getId();
			}

			// Refresh reserved list model
			this.reservedByField.setModel(this.getReservedByFieldModel());
			this.reservedByMeField.setModel(this.getReservedByMeFieldModel());

			// Set new amounts
			this.setAmounts();
		}

		// Display information
		this.popupMessageField.setText(message);
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_reserveButtonMouseClicked

    private void borrowToReservedMouseClicked(MouseEvent evt) {//GEN-FIRST:event_borrowToReservedMouseClicked
		String message = "Borrowing failed!";

		// Logged user must be employee
		if (this.reservedByField.getSelectedValue() != null &&
			this.loggedUser.getEmployee().getId() != 0) {
			IReservedInventory reservation =
				(IReservedInventory) this.reservedByField.getSelectedValue();

			// Borrow reserved item
			IBorrowedInventory borrowed =
				this.reservationsService.borrow(this.reservationsService.
					getCardNumber(reservation.getIdClient()),
					this.loggedUser.getEmployee().getId(),
					this.selectedLibraryInventory.getId());

			// Delete reservation if borrowing was successful
			if (borrowed.getId() != 0) {
				message = "Successfully borrowed, borrowing no. " +
					borrowed.getId();
				reservation.delete();
			}

			// Refresh relevant models
			this.borrowedByField.setModel(this.getBorrowedByFieldModel());
			this.reservedByField.setModel(this.getReservedByFieldModel());
			this.reservedByMeField.setModel(this.getReservedByMeFieldModel());
		}

		// Display information
		this.popupMessageField.setText(message);
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_borrowToReservedMouseClicked

    private void cancelResButtonMouseClicked(MouseEvent evt) {//GEN-FIRST:event_cancelResButtonMouseClicked
		String message = "Cancellation failed!";
		IReservedInventory selected =
			(IReservedInventory) this.reservedByMeField.
			getSelectedValue();
		if (selected != null && this.loggedUser.getClient().getId() != 0) {
			if (this.reservationsService.cancelReserved(selected.getId())) {
				message = "Cancelled reservation no. " + selected.getId();
			}

			// Refresh borrowed list model
			this.reservedByField.setModel(this.getReservedByFieldModel());
			this.reservedByMeField.setModel(this.getReservedByMeFieldModel());

			// Set new amounts
			this.setAmounts();
		}

		// Display information
		this.popupMessageField.setText(message);
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_cancelResButtonMouseClicked

    private void returnButtonMouseClicked(MouseEvent evt) {//GEN-FIRST:event_returnButtonMouseClicked
		String message = "Return failed!";
		IBorrowedInventory selected = (IBorrowedInventory) this.borrowedByField.
			getSelectedValue();
		if (selected != null && this.loggedUser.getEmployee().getId() != 0) {
			if (this.reservationsService.returnBorrowed(selected.getId())) {
				message = "Returned borrowing no. " + selected.getId();
			}

			// Refresh borrowed list model
			this.borrowedByField.setModel(this.getBorrowedByFieldModel());

			// Set new amounts
			this.setAmounts();
		}

		// Display information
		this.popupMessageField.setText(message);
		this.popupMessage.setVisible(true);
    }//GEN-LAST:event_returnButtonMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JLabel availableAmountLabel;
    private JLabel availableAmountValueLabel;
    private JButton borrowButton;
    private JLabel borrowClientAmountField;
    private JLabel borrowClientAmountLabel;
    private JPanel borrowTab;
    private JButton borrowToReserved;
    private JList borrowedByField;
    private JLabel borrowedByLabel;
    private JScrollPane borrowedByScrollPane;
    private JButton cancelResButton;
    private JTextField cardNumberField;
    private JLabel cardNumberLabel;
    private JLabel descriptionLabel;
    private JLabel descriptionValueLabel;
    private JPanel detailsTab;
    private JList libraryInventoryField;
    private JScrollPane libraryInventoryScrollPane;
    private JSplitPane libraryInventorySplitPane;
    private JTabbedPane libraryInventoryTabbedPane;
    private JLabel nameLabel;
    private JLabel nameValueLabel;
    private JDialog popupMessage;
    private JLabel popupMessageField;
    private JLabel publishDateLabel;
    private JLabel publishDateValueLabel;
    private JLabel publisherLabel;
    private JLabel publisherValueLabel;
    private JLabel reservationAmountField;
    private JLabel reservationAmountLabel;
    private JLabel reservationExpiresField;
    private JLabel reservationExpiresLabel;
    private JLabel reservationTimeField;
    private JLabel reservationTimeLabel;
    private JPanel reservationsTab;
    private JButton reserveButton;
    private JList reservedByField;
    private JLabel reservedByLabel;
    private JList reservedByMeField;
    private JLabel reservedByMeLabel;
    private JScrollPane reservedByMeScrollPane;
    private JScrollPane reservedByScrollPane;
    private JButton returnButton;
    private JLabel totalAmountLabel;
    private JLabel totalAmountValueLabel;
    private JLabel typeLabel;
    private JLabel typeValueLabel;
    // End of variables declaration//GEN-END:variables
}
