package pk.gui;

// Import OSGi framework libraries
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

// Import used GUI libraries
import pk.gui.display.LibraryDisplay;

// Import used services API
import pk.core.api.ICore;
import pk.users.api.IAuthentication;
import pk.users.api.IGroups;
import pk.users.api.IUsers;
import pk.inventory.api.IInventoryItems;
import pk.inventory.api.IReservations;

/**
 * Activator for GUI bundle, uses services provided by other bundles, doesn't
 * register any own services.
 */
public class Activator implements BundleActivator, ServiceListener {

	/**
	 * Reference to context in which this bundle was started.
	 */
	private BundleContext context;

	/**
	 * Class in control of displaying library GUI.
	 */
	private LibraryDisplay libraryDisplay;

	private ServiceReference coreService;

	private ServiceReference authenticationService;

	private ServiceReference usersService;

	private ServiceReference groupsService;

	private ServiceReference inventoryItemsService;

	private ServiceReference reservationsService;

	protected void updateServices() {
		// Save libraries service in registry if exists
		if (this.coreService != null) {
			ICore libraries = (ICore) this.context.
				getService(this.coreService);
			this.libraryDisplay.setService(ICore.class.getName(),
				libraries);
		}

		// Save authentication service in registry if exists
		if (this.authenticationService != null) {
			IAuthentication authentication = (IAuthentication) this.context.
				getService(this.authenticationService);
			this.libraryDisplay.setService(IAuthentication.class.getName(),
				authentication);
		}

		// Save users service in registry if exists
		if (this.usersService != null) {
			IUsers users = (IUsers) this.context.getService(
				this.usersService);
			this.libraryDisplay.setService(IUsers.class.getName(), users);
		}

		// Save groups service in registry if exists
		if (this.groupsService != null) {
			IGroups groups = (IGroups) this.context.getService(
				this.groupsService);
			this.libraryDisplay.setService(IGroups.class.getName(), groups);
		}

		// Save inventory items service in registry if exists
		if (this.inventoryItemsService != null) {
			IInventoryItems inventoryItems = (IInventoryItems) this.context.
				getService(this.inventoryItemsService);
			this.libraryDisplay.setService(IInventoryItems.class.getName(),
				inventoryItems);
		}

		// Save reservation service in registry if exists
		if (this.reservationsService != null) {
			IReservations reservations = (IReservations) this.context.
				getService(this.reservationsService);
			this.libraryDisplay.setService(IReservations.class.getName(),
				reservations);
		}
	}

	@Override
	public void start(BundleContext context) throws Exception {
		// Save context of this bundle and add it as service listener
		this.context = context;
		this.context.addServiceListener((ServiceListener) this);

		// Initialize library display
		this.libraryDisplay = new LibraryDisplay();

		// Get service references
		this.coreService = this.context.getServiceReference(
			ICore.class.getName());
		this.authenticationService = this.context.getServiceReference(
			IAuthentication.class.getName());
		this.usersService = this.context.getServiceReference(
			IUsers.class.getName());
		this.groupsService = this.context.getServiceReference(
			IGroups.class.getName());
		this.inventoryItemsService = this.context.getServiceReference(
			IInventoryItems.class.getName());
		this.reservationsService = this.context.getServiceReference(
			IReservations.class.getName());

		// Try to get required services and save them in library registry
		this.updateServices();

		// Display library login screen
		this.libraryDisplay.displayLogin();
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {

	}

	@Override
	public void serviceChanged(ServiceEvent se) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
