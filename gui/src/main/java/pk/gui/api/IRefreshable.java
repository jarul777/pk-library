package pk.gui.api;

public interface IRefreshable {
	public void refreshState();
}
