package pk.gui.display;

// Import used java utilities
import java.util.Map;

// Import used external libraries
import com.google.common.collect.Maps;
import pk.core.api.ICore;
import pk.core.api.objects.ILibrary;

// Import all displayable forms
import pk.gui.login.LoginForm;
import pk.gui.main.CreateLibraryForm;
import pk.gui.main.MainForm;
import pk.gui.main.SwitchLibraryForm;

// Import used objects API
import pk.users.api.objects.IUser;

// Import used services API
import pk.users.api.IUsers;

/**
 * Provides functions for displaying separate forms that are included in PK
 * Library GUI. Also acts as a service provider for this forms.
 */
public class LibraryDisplay {

	public Map<String, Object> services;

	private LoginForm login;

	private MainForm main;

	private SwitchLibraryForm switchLibrary;

	private CreateLibraryForm createLibrary;

	public LibraryDisplay() {
		// Intialize services collection
		this.services = Maps.newHashMap();
	}

	public Object getService(String key) {
		return this.services.getOrDefault(key, null);
	}

	public void setService(String key, Object value) {
		this.services.put(key, value);
	}

	public void switchLibrary(ILibrary library) {
		this.main.setSelectedLibrary(library);
	}

	public void displayLogin() {
		// Initialise and display login form
		this.login = new LoginForm(this);
		this.login.setVisible(true);
	}

	public void displayMain(int idUser) {
		IUsers usersManager = (IUsers) this.services.getOrDefault(
			IUsers.class.getName(), null);
		if (usersManager != null) {
			IUser logged = usersManager.getUser(idUser);

			// Hide login form
			this.login.setVisible(false);

			// Initialise and display main form
			this.main = new MainForm(logged, this);
			this.main.setVisible(true);
		}
	}

	public void displaySwitchLibrary(ILibrary currentSelection) {
		// Initialise and display switch library form
		this.switchLibrary = new SwitchLibraryForm(currentSelection, this);
		this.switchLibrary.setVisible(true);
	}

}
