package pk.gui.main;

// Import used java utilities
import com.google.common.collect.Sets;
import java.util.Collection;

// Import used AWT libraries
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.*;

// Import used javax controls and models
import javax.swing.*;

// Import used swing events
import javax.swing.event.*;

// Import used custom GUI items
import pk.gui.display.LibraryDisplay;
import pk.gui.panels.*;

// Import used objects API
import pk.users.api.objects.IGroupPermission;
import pk.users.api.objects.IUser;
import pk.core.api.objects.ILibrary;

// Import used services API
import pk.inventory.api.IInventoryItems;
import pk.inventory.api.IReservations;
import pk.users.api.IGroups;
import pk.users.api.IUsers;
import pk.core.api.ICore;

// Import implemented API
import pk.gui.api.IRefreshable;
import pk.inventory.api.objects.IBorrowedInventory;
import pk.inventory.api.objects.IReservedInventory;

/**
 * Interface main frame, holds all other elements and distributes services among
 * them with help of library display object, which acts as service registry.
 */
public class MainForm extends JFrame implements IRefreshable {

	/**
	 * Display controller and service registry.
	 */
	private LibraryDisplay display;

	/**
	 * User currently logged in.
	 */
	private IUser loggedUser;

	/**
	 * Selected library, defaults to the library current user is registered in.
	 */
	private ILibrary selectedLibrary;

	/**
	 * Creates new form MainForm with given display object.
	 *
	 * @param display Library display object, contains all services.
	 */
	public MainForm(IUser loggedUser, LibraryDisplay display) {
		// Set library display and user, must be done before initialization
		this.display = display;
		this.loggedUser = loggedUser;
		this.selectedLibrary = loggedUser.getPerson().getLibrary();

		// Initialize display and permissions
		this.initComponents();
		this.initPermissionsSee();
	}

	/**
	 * Creates new form MainForm.
	 */
	public MainForm() {
		// Initialize display
		this.initComponents();
	}

	/**
	 * Displays tabs appropriate for logged user's permissions. Only see
	 * privileges are checked on this stage.
	 */
	private void initPermissionsSee() {
		// Get logged user permissions
		Collection<IGroupPermission> permissions = this.loggedUser.getGroup().
			getGroupPermissions();

		// Display allowed elements
		for (IGroupPermission permission : permissions) {
			String name = permission.getPermission().getName();
			if (name.equals("All")) {
				this.showAll();
				break;
			} else if (permission.getSee()) {
				this.showPermitted(name);
			}
		}
	}

	/**
	 * Show tab or menu item corresponding to given permission name.
	 *
	 * @param permissionName Name of permission for displaying some portion of
	 * the interface.
	 */
	private void showPermitted(String permissionName) {
		switch (permissionName) {
			case "Library Inventory":
				this.mainTabbedPane.addTab("Library Inventory",
					this.libraryInventoryTab);
				break;
			case "Global Inventory":
				this.mainTabbedPane.addTab("Global Inventory",
					this.inventoryTab);
				break;
			case "Users":
				this.mainTabbedPane.addTab("Users", this.usersTab);
				break;
			case "Groups":
				this.mainTabbedPane.addTab("Groups", this.groupsTab);
				break;
			case "Libraries":
				this.mainMenuBar.add(this.libraryMenuPosition);
				break;
			default:
				break;
		}
	}

	/**
	 * Shows all tabs and menu items.
	 */
	private void showAll() {
		// Remove all already added to prevent doubling
		this.mainTabbedPane.removeAll();

		// Add all available tabs
		this.mainTabbedPane.addTab("Notifications", this.notificationsTab);
		this.mainTabbedPane.addTab("Library Inventory",
			this.libraryInventoryTab);
		this.mainTabbedPane.addTab("Global Inventory",
			this.inventoryTab);
		this.mainTabbedPane.addTab("Users", this.usersTab);
		this.mainTabbedPane.addTab("Groups", this.groupsTab);

		// Add menu items
		this.mainMenuBar.add(this.libraryMenuPosition);
	}

	/**
	 * Creates a model for notifications list.
	 *
	 * @return Default model for notifications list, empty if reservations
	 * service is null.
	 */
	private ListModel getNotificationsFieldModel() {
		Collection<String> notifications = Sets.newHashSet();
		IReservations reservationsService = (IReservations) this.display.
			getService(IReservations.class.getName());
		if (reservationsService != null &&
			this.loggedUser != null) {
			// Add client specific notifications
			if (this.loggedUser.getClient().getId() != 0) {
				Collection<IBorrowedInventory> borrowedByClient =
					reservationsService.listBorrowedInventory(this.loggedUser.
						getClient().getId(), 0, 0);
				Collection<IReservedInventory> reservedByClient =
					reservationsService.listReservedInventory(this.loggedUser.
						getClient().getId(), 0);

				// Add borrowing notifications
				for (IBorrowedInventory borrowed : borrowedByClient) {
					notifications.add(this.parseClientBorrowedNotification(
						borrowed));
				}

				// Add reservation notifications
				for (IReservedInventory reserved : reservedByClient) {
					notifications.add(this.parseClientReservedNotification(
						reserved));
				}
			}

			// Add employee specific notifications
			if (this.loggedUser.getEmployee().getId() != 0) {
				Collection<IBorrowedInventory> borrowedByEmployee =
					reservationsService.listBorrowedInventory(0,
						this.loggedUser.getEmployee().getId(), 0);

				// Add borrowing notifications
				for (IBorrowedInventory borrowed : borrowedByEmployee) {
					notifications.add(this.parseEmployeeBorrowedNotification(
						borrowed));
				}
			}
		}

		// Fill notifications field using default model
		DefaultListModel notificationsFieldModel = new DefaultListModel();
		for (String notification : notifications) {
			notificationsFieldModel.addElement(notification);
		}

		// Return filled model
		return notificationsFieldModel;
	}

	private String parseClientBorrowedNotification(IBorrowedInventory inv) {
		return "Borrowed from library, no. " + inv.getId() + ", item: " +
			inv.getLibraryInventory().getInventory().getName() + ", borrowed " +
			inv.getBorrowTime() + ", due " + inv.getDueTime();
	}

	private String parseClientReservedNotification(IReservedInventory inv) {
		return "Reserved from library, no. " + inv.getId() + ", item: " +
			inv.getLibraryInventory().getInventory().getName() + ", reserved " +
			inv.getReservationTime() + ", expires " + inv.getExpirationTime();
	}

	private String parseEmployeeBorrowedNotification(IBorrowedInventory inv) {
		return "Borrowed to client, no. " + inv.getId() + ", item: " +
			inv.getLibraryInventory().getInventory().getName() + ", borrowed " +
			inv.getBorrowTime() + ", due " + inv.getDueTime();
	}

	/**
	 * Set other library selection than library used by logged user.
	 *
	 * @param library New library selection.
	 */
	public void setSelectedLibrary(ILibrary library) {
		// Update field and all panels using library selection
		this.selectedLibrary = library;
		this.libraryInventoryTab.setLibrary(library);
	}

	@Override
	public void refreshState() {
		// Refresh model for this component elemetns
		this.notificationsField.setModel(this.getNotificationsFieldModel());

		// Refresh other components
		this.libraryInventoryTab.refreshState();
		this.inventoryTab.refreshState();
		this.usersTab.refreshState();
		this.groupsTab.refreshState();
	}

	/**
	 * This method is called from within the constructor to initialise the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        libraryInventoryTab = new LibraryInventoryPanel(this.loggedUser, (IInventoryItems) this.display.getService(IInventoryItems.class.getName()), (IReservations) this.display.getService(IReservations.class.getName()));
        ;
        inventoryTab = new InventoryPanel(this.loggedUser, (IInventoryItems) this.display.getService(IInventoryItems.class.getName()), (ICore) this.display.getService(ICore.class.getName()));
        ;
        usersTab = new UsersPanel(this.loggedUser, (IUsers) this.display.getService(IUsers.class.getName()), (IGroups) this.display.getService(IGroups.class.getName()), (ICore) this.display.getService(ICore.class.getName()));
        ;
        groupsTab = new GroupsPanel(this.loggedUser, (IGroups) this.display.getService(IGroups.class.getName()));
        ;
        libraryMenuPosition = new JMenu();
        switchLibrary = new JMenuItem();
        newLibrary = new JMenuItem();
        mainTabbedPane = new JTabbedPane();
        notificationsTab = new Panel();
        notificationsScrollPane = new JScrollPane();
        notificationsField = new JList();
        mainMenuBar = new JMenuBar();
        userMenuPosition = new JMenu();
        switchUser = new JMenuItem();
        editUser = new JMenuItem();

        libraryInventoryTab.setMinimumSize(new Dimension(0, 0));
        libraryInventoryTab.setName("libraryInventoryTab"); // NOI18N

        inventoryTab.setMinimumSize(new Dimension(0, 0));
        inventoryTab.setName("inventoryTab"); // NOI18N

        usersTab.setMinimumSize(new Dimension(0, 0));
        usersTab.setName("usersTab"); // NOI18N

        groupsTab.setMinimumSize(new Dimension(0, 0));
        groupsTab.setName("groupsTab"); // NOI18N

        libraryMenuPosition.setText("Library");
        libraryMenuPosition.setName("libraryMenuPosition"); // NOI18N

        switchLibrary.setText("Switch");
        switchLibrary.setName("switchLibrary"); // NOI18N
        switchLibrary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                switchLibraryActionPerformed(evt);
            }
        });
        libraryMenuPosition.add(switchLibrary);

        newLibrary.setText("Create new");
        newLibrary.setName("newLibrary"); // NOI18N
        libraryMenuPosition.add(newLibrary);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("PK Library");
        setMinimumSize(new Dimension(800, 600));
        setName("Library"); // NOI18N
        setPreferredSize(new Dimension(800, 600));

        mainTabbedPane.setName("mainTabbedPane"); // NOI18N
        mainTabbedPane.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                mainTabbedPaneStateChanged(evt);
            }
        });

        notificationsTab.setName("notificationsTab"); // NOI18N

        notificationsScrollPane.setName("notificationsScrollPane"); // NOI18N

        notificationsField.setModel(getNotificationsFieldModel());
        notificationsField.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        notificationsField.setName("notificationsField"); // NOI18N
        notificationsScrollPane.setViewportView(notificationsField);

        GroupLayout notificationsTabLayout = new GroupLayout(notificationsTab);
        notificationsTab.setLayout(notificationsTabLayout);
        notificationsTabLayout.setHorizontalGroup(notificationsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(notificationsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(notificationsScrollPane, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addContainerGap())
        );
        notificationsTabLayout.setVerticalGroup(notificationsTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(notificationsTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(notificationsScrollPane, GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainTabbedPane.addTab("Notifications", notificationsTab);

        mainMenuBar.setName("mainMenuBar"); // NOI18N

        userMenuPosition.setText("User");
        userMenuPosition.setName("userMenuPosition"); // NOI18N

        switchUser.setText("Switch");
        switchUser.setName("switchUser"); // NOI18N
        switchUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                switchUserActionPerformed(evt);
            }
        });
        userMenuPosition.add(switchUser);

        editUser.setText("Edit data");
        editUser.setName("editUser"); // NOI18N
        userMenuPosition.add(editUser);

        mainMenuBar.add(userMenuPosition);

        setJMenuBar(mainMenuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(mainTabbedPane)
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(mainTabbedPane)
        );

        mainTabbedPane.getAccessibleContext().setAccessibleName("Inventory");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mainTabbedPaneStateChanged(ChangeEvent evt) {//GEN-FIRST:event_mainTabbedPaneStateChanged
		// Refresh selected tab if it can be refreshed
		Component selected = this.mainTabbedPane.getSelectedComponent();
		if (selected instanceof IRefreshable) {
			((IRefreshable) selected).refreshState();
		}
    }//GEN-LAST:event_mainTabbedPaneStateChanged

    private void switchUserActionPerformed(ActionEvent evt) {//GEN-FIRST:event_switchUserActionPerformed
		this.display.displayLogin();
		this.dispose();
    }//GEN-LAST:event_switchUserActionPerformed

    private void switchLibraryActionPerformed(ActionEvent evt) {//GEN-FIRST:event_switchLibraryActionPerformed
		this.display.displaySwitchLibrary(this.selectedLibrary);
    }//GEN-LAST:event_switchLibraryActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JMenuItem editUser;
    private GroupsPanel groupsTab;
    private InventoryPanel inventoryTab;
    private LibraryInventoryPanel libraryInventoryTab;
    private JMenu libraryMenuPosition;
    private JMenuBar mainMenuBar;
    private JTabbedPane mainTabbedPane;
    private JMenuItem newLibrary;
    private JList notificationsField;
    private JScrollPane notificationsScrollPane;
    private Panel notificationsTab;
    private JMenuItem switchLibrary;
    private JMenuItem switchUser;
    private JMenu userMenuPosition;
    private UsersPanel usersTab;
    // End of variables declaration//GEN-END:variables

}
